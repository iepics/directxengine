#include "stdafx.h"
#include "FBXLoader.h"

using std::shared_ptr;

FBXLoader::FBXLoader(void)
{
	mFBXManager = nullptr;
	mFBXScene = nullptr;
	mHasBone = false;
}

FBXLoader::~FBXLoader(void)
{
}

bool FBXLoader::Initialize()
{
	mFBXManager = FbxManager::Create();
	if(!mFBXManager)
	{
		return false;
	}

	FbxIOSettings *fbxIOSettings = FbxIOSettings::Create(mFBXManager, IOSROOT);
	mFBXManager->SetIOSettings(fbxIOSettings);

	mFBXScene = FbxScene::Create(mFBXManager, "myScene");

	return true;
}

bool FBXLoader::LoadScene(std::wstring inFileName, AxisSystem axis)
{
	std::chrono::system_clock::time_point lastTime = std::chrono::system_clock::now();
	std::chrono::system_clock::time_point currTime;
	std::chrono::duration<float> sec;

	mFileName = inFileName;

	// FBX 파일을 읽기 위한 Importer 생성
	FbxImporter* fbxImporter = FbxImporter::Create(mFBXManager, "myImporter");
	if(!fbxImporter)
	{
		return false;
	}
	
	// 파일 이름을 인자로 Importer 초기화(파일 열기)
	char* utf;
	FbxWCToUTF8(mFileName.c_str(), utf);
	if(!fbxImporter->Initialize(utf, -1, mFBXManager->GetIOSettings()))
	{
		return false;
	}
	currTime = std::chrono::system_clock::now();
	sec = currTime - lastTime;
	lastTime = currTime;
	printf("%f\n", sec.count());

	// 초기화된 Importer를 이용하여 현재 열린 FBX 파일을 Scene으로 불러온다
	if(!fbxImporter->Import(mFBXScene))
	{
		return false;
	}
	fbxImporter->Destroy();
	printf("FBX 파일 로딩 완료\n");
	currTime = std::chrono::system_clock::now();
	sec = currTime - lastTime;
	lastTime = currTime;
	printf("%f\n", sec.count());

		
	FbxAxisSystem axisSystem = FbxAxisSystem::OpenGL;
	axisSystem.ConvertScene(mFBXScene);
	mAxisSystem = axis;


	// 폴리곤 처리의 편의성을 위해 폴리곤을 삼각화
	FbxGeometryConverter geometryConverter(mFBXManager);
	geometryConverter.Triangulate(mFBXScene, true);
	printf("삼각화 완료\n");
	currTime = std::chrono::system_clock::now();
	sec = currTime - lastTime;
	lastTime = currTime;
	printf("%f\n", sec.count());

	mFBXScene->GetGlobalSettings().GetSystemUnit().GetConversionFactorTo(FbxSystemUnit::m); 

//	ProcessSkeletonHierarchy(mFBXScene->GetRootNode());

	// Scene에서 애니메이션, 머터리얼, 지오메트리 정보를 로드
	LoadAnimationClip();
	printf("애니메이션 클립 처리 완료\n");
	currTime = std::chrono::system_clock::now();
	sec = currTime - lastTime;
	lastTime = currTime;
	printf("%f\n", sec.count());

	LoadMaterials(mFBXScene);
	printf("머터리얼 로딩 완료\n");
	currTime = std::chrono::system_clock::now();
	sec = currTime - lastTime;
	lastTime = currTime;
	printf("%f\n", sec.count());

	mRootNode = std::make_shared<ModelNode>(L"RootNode", L"null");
	LoadGeometry(mFBXScene->GetRootNode(), _T("null"), mRootNode);
	printf("지오메트리 로딩 완료\n");
	currTime = std::chrono::system_clock::now();
	sec = currTime - lastTime;
	lastTime = currTime;
	printf("%f\n", sec.count());

	mModelData = std::make_shared<ModelData>();
	CreateModel();
	printf("모델 로딩 완료\n");
	currTime = std::chrono::system_clock::now();
	sec = currTime - lastTime;
	lastTime = currTime;
	printf("%f\n", sec.count());

	return true;
}

shared_ptr<ModelData> FBXLoader::CreateModel()
{
	shared_ptr<ModelData> model = std::make_shared<ModelData>();
	model->name = mRootNode->name;
	for(auto value : mRootNode->childs)
	{
		if(value->type == ModelNode::Bone)
		{
			model->boneRootNodes[value->nodeID] = value;
		}
		else
		{
			model->meshRootNodes.push_back(value);
		}
	}
	model->animationClips.swap(mAnimationClips);
	CreateGameObject(model);

	ResourceManager::GetInstance()->AddModel(model->name, model);

	return model;
}

shared_ptr<GameObject> FBXLoader::CreateGameObject(shared_ptr<ModelData>& modelData)
{
	unsigned int meshNodeSize = (unsigned int)modelData->meshRootNodes.size();
	unsigned int boneNodeSize = (unsigned int)modelData->boneRootNodes.size();
	unsigned int clipSize = (unsigned int)modelData->animationClips.size();

	shared_ptr<GameObject> rootObject;
	if(meshNodeSize + boneNodeSize > 1)
	{
		rootObject = EditorUtility::CreateEmpty();
		rootObject->SetName(modelData->name);
		modelData->gameObject = rootObject;
		//modelData->gameObject.push_back(rootObject);

		for(unsigned int i = 0; i < meshNodeSize; ++i)
		{
			CreateGameObjectRecursively(modelData, modelData->meshRootNodes[i]);
		}

		if (boneNodeSize > 0)
		{
			///Animation component
			auto& animation = rootObject->InitComponent<Animation>();
			for (auto& clip : modelData->animationClips)
			{
				animation->AddClip(clip);
			}
		}
	}
	else if(meshNodeSize == 1)
	{
		mRootNode = modelData->meshRootNodes[0];
		rootObject = CreateGameObjectRecursively(modelData, modelData->meshRootNodes[0]);
		modelData->gameObject = rootObject;
		//modelData->gameObject.push_back(rootObject);
	}

//	modelData->cached.clear();

	return rootObject;
}

shared_ptr<GameObject> FBXLoader::CreateGameObjectRecursively(shared_ptr<ModelData>& modelData, shared_ptr<ModelNode>& modelNode)
{
	shared_ptr<GameObject> retObj;
	retObj = EditorUtility::CreateEmpty();
	retObj->SetName(modelNode->name);

//	modelData->cached.push_back(retObj);

	if(modelNode->type == ModelNode::Mesh)
	{
		modelData->meshes[modelNode->name] = CreateMesh(modelNode);
		shared_ptr<MeshRenderer> renderer;

		renderer = retObj->InitComponent<MeshRenderer>();
		renderer->SetMesh(modelData->meshes[modelNode->name]);		

		ResourceManager* manager = ResourceManager::GetInstance();
		for(unsigned int i = 0; i < modelNode->materialNames.size(); i++)
		{
			shared_ptr<Material> material = manager->GetMaterial(modelNode->materialNames[i]);
			renderer->AddMaterial(material);
		}

		if(modelNode->parentName == mRootNode->name)
		{
			/*modelData->gameObject->GetTransform()->SetChild(retObj->GetTransform());
			retObj->GetTransform()->SetParent(modelData->gameObject->GetTransform());	*/		
			modelData->gameObject->SetChild(retObj);
			retObj->SetParent(modelData->gameObject);
			/*modelData->gameObject.front()->GetTransform()->SetChild(retObj->GetTransform());
			retObj->GetTransform()->SetParent(modelData->gameObject.front()->GetTransform());*/
		}
	}
	if(modelNode->type == ModelNode::SkinnedMesh)
	{
		int64_t boneID = modelNode->rootBoneID;
		shared_ptr<ModelNode> boneNode = modelData->boneRootNodes[boneID];
		shared_ptr<GameObject> boneObject = CreateGameObjectRecursively(modelData, boneNode);

		modelData->meshes[modelNode->name] = CreateMesh(modelNode);		

		shared_ptr<SkinnedMeshRenderer> renderer;
		renderer = retObj->InitComponent<SkinnedMeshRenderer>();
		renderer->SetMesh(modelData->meshes[modelNode->name]);		

		ResourceManager* manager = ResourceManager::GetInstance();
		for(unsigned int i = 0; i < modelNode->materialNames.size(); i++)
		{
			shared_ptr<Material> material = manager->GetMaterial(modelNode->materialNames[i]);
			renderer->AddMaterial(material);
		}
		renderer->SetRootBoneByName(boneNode->name);		

		if(modelNode->parentName == mRootNode->name)
		{
			/*modelData->gameObject->GetTransform()->SetChild(retObj->GetTransform());
			retObj->GetTransform()->SetParent(modelData->gameObject->GetTransform());

			modelData->gameObject->GetTransform()->SetChild(boneObject->GetTransform());
			boneObject->GetTransform()->SetParent(modelData->gameObject->GetTransform());*/
			modelData->gameObject->SetChild(retObj);
			retObj->SetParent(modelData->gameObject);

			modelData->gameObject->SetChild(boneObject);
			boneObject->SetParent(modelData->gameObject);

			/*modelData->gameObject.front()->GetTransform()->SetChild(retObj->GetTransform());
			retObj->GetTransform()->SetParent(modelData->gameObject.front()->GetTransform());

			modelData->gameObject.front()->GetTransform()->SetChild(boneObject->GetTransform());
			boneObject->GetTransform()->SetParent(modelData->gameObject.front()->GetTransform());*/
		}
	}

	XMVECTOR pos, rot, scale;
	XMFLOAT3 p, s;
	XMFLOAT4 r;
	XMMatrixDecompose(&scale, &rot, &pos, modelNode->matrix);
	XMStoreFloat3(&p, pos);
	XMStoreFloat4(&r, rot);
	XMStoreFloat3(&s, scale);
	retObj->GetTransform()->SetLocalPosition(p);
	retObj->GetTransform()->SetLocalRotation(r);
	retObj->GetTransform()->SetLocalScale(s);

	for(unsigned int i = 0; i < modelNode->childs.size(); i++)
	{
		shared_ptr<GameObject> child = CreateGameObjectRecursively(modelData, modelNode->childs[i]);
		retObj->SetChild(child);
		child->SetParent(retObj);
		/*retObj->GetTransform()->SetChild(child->GetTransform());
		child->GetTransform()->SetParent(retObj->GetTransform());*/
	}

	return retObj;
}

shared_ptr<Mesh> FBXLoader::CreateMesh(shared_ptr<ModelNode>& meshNode)
{
	shared_ptr<Mesh> mesh = std::make_shared<Mesh>();

	for(unsigned int i = 0; i < meshNode->vertices.size(); i++)
	{
		ModelVertex vertex;
		vertex.position = meshNode->vertices[i].mPosition;
		vertex.normal = meshNode->vertices[i].mNormal;
		vertex.uv = meshNode->vertices[i].mUVs[0];

		if(mHasBone)
		{
			vertex.weights.x = (float)meshNode->vertices[i].mVertexBlendingInfos[0].mBlendingWeight;
			vertex.weights.y = (float)meshNode->vertices[i].mVertexBlendingInfos[1].mBlendingWeight;
			vertex.weights.z = (float)meshNode->vertices[i].mVertexBlendingInfos[2].mBlendingWeight;
			vertex.boneIndices[0] = (BYTE)meshNode->vertices[i].mVertexBlendingInfos[0].mBlendingIndex;
			vertex.boneIndices[1] = (BYTE)meshNode->vertices[i].mVertexBlendingInfos[1].mBlendingIndex;
			vertex.boneIndices[2] = (BYTE)meshNode->vertices[i].mVertexBlendingInfos[2].mBlendingIndex;
			vertex.boneIndices[3] = (BYTE)meshNode->vertices[i].mVertexBlendingInfos[3].mBlendingIndex;
		}

		mesh->AddVertex(vertex);
	}

	std::vector<XMFLOAT4X4> bindPoses;
	for(unsigned int i = 0; i < meshNode->bindPoses.size(); ++i)
	{
		XMFLOAT4X4 mat;
		XMStoreFloat4x4(&mat, meshNode->bindPoses[i]);
		bindPoses.push_back(mat);
	}
	mesh->SetBindPoses(bindPoses);

	unsigned int subMeshSize = (unsigned int)meshNode->submeshIndices.size();
	UINT matIndex = 0;
	UINT triangleStart = 0;
	UINT triangleCount = 0;
	std::vector<SubMesh> subMeshes;
	subMeshes.resize(subMeshSize);
	for(unsigned int i = 0; i < subMeshSize; ++i)
	{
		std::vector<unsigned int> indices = meshNode->submeshIndices[i];
		for(unsigned int j = 0; j < indices.size(); ++j)
		{
			subMeshes[i].indices.push_back(indices[j]);
		}
	}
	mesh->SetSubMeshes(subMeshes);

	mesh->SetName(meshNode->name);

	mesh->CreateVertexBuffer();
	mesh->CreateIndexBuffer();

	return mesh;
}

shared_ptr<Material> FBXLoader::CreateMaterial(const ModelMaterial& inMaterial)
{
	ResourceManager* resourceManager = ResourceManager::GetInstance();

	ModelMaterial::MaterialType type = inMaterial.type;

	shared_ptr<Material> material = std::make_shared<Material>();

	std::wstring::size_type index = mFileName.find_last_of(L"/");
	std::wstring upPath = mFileName.substr(0, index);
	upPath.append(L"/");

	auto diffuseTexture = std::make_shared<Texture2D>();
	if(inMaterial.diffuseMapName != L"" &&diffuseTexture->LoadFile((upPath + inMaterial.diffuseMapName).c_str()))
	{
		resourceManager->AddTexture(inMaterial.diffuseMapName, std::static_pointer_cast<Texture, Texture2D>(diffuseTexture));
		material->SetTexture("_diffuse", diffuseTexture);
	}

	auto normalTexture = std::make_shared<Texture2D>();
	if(inMaterial.normalMapName != L"" && normalTexture->LoadFile((upPath + inMaterial.normalMapName).c_str()))
	{
		resourceManager->AddTexture(inMaterial.diffuseMapName, std::static_pointer_cast<Texture, Texture2D>(normalTexture));
		material->SetTexture("_normal", normalTexture);
	}

	//
	material->SetName(inMaterial.name);

	//
	//
	//material->SetColor("_diffuse", inMaterial.diffuse);
	//material->SetColor("_emissive", inMaterial.emissive);

	material->SetEffect(resourceManager->GetEffect(L"Resource/FX/Standard.fx"));

	//리소스 매니저에 추가
	resourceManager->AddMaterial(material->GetName(), material);

	return material;
}

void FBXLoader::LoadAnimationClip()
{
	int animCount =  mFBXScene->GetSrcObjectCount<FbxAnimStack>();
	mAnimationClips.resize(animCount);
	FbxTime::EMode timeMode = mFBXScene->GetGlobalSettings().GetTimeMode();
	for(int i = 0; i < animCount; ++i)
	{
		FbxAnimStack* animStack = mFBXScene->GetSrcObject<FbxAnimStack>(i);
		FbxString name = animStack->GetName();
		FbxTakeInfo* takeInfo = mFBXScene->GetTakeInfo(name);
		FbxTime start = takeInfo->mLocalTimeSpan.GetStart();
		FbxTime end = takeInfo->mLocalTimeSpan.GetStop();
		FbxLongLong	frameCount = end.GetFrameCount(timeMode) - start.GetFrameCount(timeMode) + 1;

		std::string temp(name.Buffer());
		std::wstring wname(temp.begin(), temp.end());
		float frameRate = (float)start.GetFrameRate(timeMode);
		float length = takeInfo->mLocalTimeSpan.GetDuration().GetMilliSeconds() / 1000.0f;

		shared_ptr<AnimationClip> clip = std::make_shared<AnimationClip>();
		clip->SetName(wname);
		clip->SetFrameRate(frameRate);
		clip->SetLength(length);
		
		mAnimationClips[i] = clip;
	}
}

void FBXLoader::LoadGeometry(FbxNode* inNode, std::wstring parentName, shared_ptr<ModelNode>& modelNode)
{
	if(!inNode)
		return;

	modelNode->nodeID = inNode->GetUniqueID();

	std::string str(inNode->GetName());
	std::wstring name(str.begin(), str.end());
	modelNode->name = name;
	modelNode->parentName = parentName;

	if(parentName == L"null")
	{
		std::wstring::size_type beginIndex = mFileName.find_last_of(L"/");
		std::wstring::size_type endIndex = mFileName.find_last_of(L".");
		std::wstring modelName = mFileName.substr(beginIndex + 1, endIndex - beginIndex - 1);
		modelNode->name = modelName;
	}	
	
	if(inNode->GetNodeAttribute())
	{
		// 얻어온 노드의 타입에 따라 처리를 한다
		auto type = inNode->GetNodeAttribute()->GetAttributeType();
		
		if(type == FbxNodeAttribute::eMesh)
		{
			modelNode->type = ModelNode::NodeType::Mesh;

			LoadControlPoints(inNode);
			LoadBoneNode(inNode, modelNode);
			LoadMesh(inNode, modelNode);
			AssociateMaterialToMesh(inNode, modelNode);
		}
		else if(type == FbxNodeAttribute::eSkeleton)
		{
			modelNode->type = ModelNode::NodeType::Bone;
		}
		else
		{
			modelNode->type = ModelNode::NodeType::Empty;
		}
	}

	ComputeNodeMatrix(inNode, modelNode);

	// 노드의 자식의 수만큼 재귀적으로 처리한다
	const int count = inNode->GetChildCount();
	modelNode->childs.resize(count);
	for(int i = 0; i < count; i++)
	{
		shared_ptr<ModelNode> meshNode = std::make_shared<ModelNode>(L"", L"");
		modelNode->childs[i] = meshNode;
		LoadGeometry(inNode->GetChild(i), modelNode->name, modelNode->childs[i]);
	}
}

void FBXLoader::LoadBoneNode(FbxNode* inNode, shared_ptr<ModelNode>& modelNode)
{
	FbxMesh* currMesh = inNode->GetMesh();
	unsigned int numOfDeformers = currMesh->GetDeformerCount();

	FbxAMatrix geometryTransform = FBXUtil::GetGeometryTransformation(inNode);

	int animCount = mFBXScene->GetSrcObjectCount<FbxAnimStack>();
	for(int animIndex = 0; animIndex < animCount; ++animIndex)
	{
		FbxAnimStack* animStack = mFBXScene->GetSrcObject<FbxAnimStack>(animIndex);
		auto clip = mAnimationClips[animIndex];

		FbxAnimLayer* layer = animStack->GetMember<FbxAnimLayer>();

		for(unsigned int deformerIndex = 0; deformerIndex < numOfDeformers; deformerIndex++)
		{
			FbxSkin* currSkin = reinterpret_cast<FbxSkin*>(currMesh->GetDeformer(deformerIndex, FbxDeformer::eSkin));
			if(!currSkin)
			{
				continue;
			}

			unsigned int numOfClusters = currSkin->GetClusterCount();
			if(numOfClusters > 0)
			{
				FbxCluster* currCluster = currSkin->GetCluster(0);
				modelNode->rootBoneID = currCluster->GetLink()->GetUniqueID();
				modelNode->type = ModelNode::SkinnedMesh;
				mHasBone = true;
			}

			for(unsigned int clusterIndex = 0; clusterIndex < numOfClusters; clusterIndex++)
			{
				FbxCluster* currCluster = currSkin->GetCluster(clusterIndex);

				unsigned int numOfindices = currCluster->GetControlPointIndicesCount();
				for(unsigned int i = 0; i < numOfindices; i++)
				{
					BlendingIndexWeightPair currBlendingIndexWeightPair;
					currBlendingIndexWeightPair.mBlendingIndex = clusterIndex;
					currBlendingIndexWeightPair.mBlendingWeight = currCluster->GetControlPointWeights()[i];
					mControlPoints[currCluster->GetControlPointIndices()[i]]->mBlendingInfo.push_back(currBlendingIndexWeightPair);
				}
				
				std::string temp = currCluster->GetLink()->GetName();
				std::wstring currBoneName;
				currBoneName.assign(temp.begin(), temp.end());
				std::wcout<<currBoneName<<std::endl;

				FbxAMatrix transformMatrix;
				FbxAMatrix transformLinkMatrix;
				FbxAMatrix globalBindposeInverseMatrix;

				currCluster->GetTransformMatrix(transformMatrix);
				currCluster->GetTransformLinkMatrix(transformLinkMatrix);
				globalBindposeInverseMatrix = transformLinkMatrix.Inverse() * transformMatrix * geometryTransform;

				//bind pose
				if(mAxisSystem == AxisSystem::Direct)
				{
					FbxVector4 t = globalBindposeInverseMatrix.GetT();
					FbxQuaternion q = globalBindposeInverseMatrix.GetQ();
					FbxVector4 r = globalBindposeInverseMatrix.GetR();
					FbxVector4 s = globalBindposeInverseMatrix.GetS();
					t[0] = -t[0];
					q[1] = -q[1];
					q[2] = -q[2];
					r[1] = -r[1];
					r[2] = -r[2];
					globalBindposeInverseMatrix = FbxAMatrix(t, r, s);
				}
				XMMATRIX matrix;
				FBXMatrixToXMMATRIX(globalBindposeInverseMatrix, matrix);
				modelNode->bindPoses.push_back(matrix);

				/////
				FbxVector4 preRotation = currCluster->GetLink()->GetPreRotation(FbxNode::eSourcePivot);
				FbxAMatrix preRotMatrix;
				preRotMatrix.SetIdentity();
				preRotMatrix.SetR(preRotation);
				FbxAnimCurveNode* tCurveNode = currCluster->GetLink()->LclTranslation.GetCurveNode(layer);
				ProcessAnimCurveNode(tCurveNode, clusterIndex, currBoneName, clip, preRotMatrix);
				FbxAnimCurveNode* rCurveNode = currCluster->GetLink()->LclRotation.GetCurveNode(layer);
				ProcessAnimCurveNode(rCurveNode, clusterIndex, currBoneName, clip, preRotMatrix);
				FbxAnimCurveNode* sCurveNode = currCluster->GetLink()->LclScaling.GetCurveNode(layer);
				ProcessAnimCurveNode(sCurveNode, clusterIndex, currBoneName, clip, preRotMatrix);				
			}
		}
	}

	
	//adding dummy joints (max == 4)
	BlendingIndexWeightPair currBlendingIndexWeightPair;
	currBlendingIndexWeightPair.mBlendingIndex = 0;
	currBlendingIndexWeightPair.mBlendingWeight = 0;
	for(auto itr = mControlPoints.begin(); itr != mControlPoints.end(); itr++)
	{
		for(unsigned int i = (unsigned int)itr->second->mBlendingInfo.size(); i < 4; i++)
		{
			itr->second->mBlendingInfo.push_back(currBlendingIndexWeightPair);
		}
	}

	printf("애니메이션 불러오기 완료\n");
}

void FBXLoader::ProcessAnimCurveNode(FbxAnimCurveNode* curveNode, int boneIndex, std::wstring boneName, shared_ptr<AnimationClip>& animClip, FbxAMatrix& preRotMatrix)
{
	int count = curveNode->GetChannelsCount();
	int curveCount = 0;
	for(int chIndex = 0; chIndex < count; ++chIndex)
	{
		curveCount += curveNode->GetCurveCount(chIndex);
	}
	if(curveCount == 0)
	{
		return;
	}

	std::vector<FBXAnimCurveInfo> curveVector;
	FbxAnimCurve** curves;
	curves = new FbxAnimCurve*[curveCount];
	for(int chIndex = 0; chIndex < count; ++chIndex)
	{
		std::string type = curveNode->GetDstProperty().GetLabel().Buffer();
		std::string prop = curveNode->GetChannelName(chIndex).Buffer();
		int curveCnt = curveNode->GetCurveCount(chIndex);

		for(int curveIndex = 0; curveIndex < curveCnt; ++curveIndex)
		{
			FbxAnimCurve* curve = curveNode->GetCurve(chIndex, curveIndex);
			curves[chIndex*curveCnt + curveIndex] = curve;
			FBXAnimCurveInfo info;
			info.curve = curve;
			info.type = type;
			size_t strIndex = type.find_last_of(" ");
			std::string parseStr = type.substr(strIndex + 1, std::string::npos);
			info.prop = parseStr + prop;
			info.boneIndex = boneIndex;
			info.boneName = boneName;
			curveVector.push_back(info);
		}
	}
	FbxAnimCurveFilterKeyReducer reducer;
	reducer.SetKeySync(true);
	reducer.Apply(curves, curveCount);
	FbxAnimCurveFilterTSS tss;
	FbxTime shift;
	shift.SetSecondDouble(-curves[0]->KeyGetTime(0).GetSecondDouble());
	tss.SetShift(shift);
	tss.Apply(curves, curveCount);

	int keyCount = curves[0]->KeyGetCount();
	float second = (float)curves[0]->KeyGetTime(keyCount - 1).GetSecondDouble();
	animClip->SetLength(second);
	delete[] curves;

	///				
	ProcessAnimCurve(curveVector, animClip, preRotMatrix);
	///
}

void FBXLoader::ProcessAnimCurve(std::vector<FBXAnimCurveInfo>& in, shared_ptr<AnimationClip>& animClip, FbxAMatrix& preRotMatrix)
{
	if(in.size() == 0)
	{
		return;
	}

	
	if(in[0].type == "Lcl Translation" || in[0].type == "Lcl Scaling")
	{
		for(FBXAnimCurveInfo& fbxcurveInfo : in)
		{
			shared_ptr<AnimCurveInfo> curveInfo = std::make_shared<AnimCurveInfo>();
			shared_ptr<AnimationCurve> curve = std::make_shared<AnimationCurve>();
			for(int keyIndex = 0; keyIndex < fbxcurveInfo.curve->KeyGetCount(); ++keyIndex)
			{
				Keyframetemp keyframe;
				keyframe.time = (float)fbxcurveInfo.curve->KeyGetTime(keyIndex).GetSecondDouble();
				keyframe.value = fbxcurveInfo.curve->KeyGetValue(keyIndex);

				if(mAxisSystem == AxisSystem::Direct)
				{
					if(fbxcurveInfo.prop == "TranslationX")
					{
						keyframe.value = -keyframe.value;
					}
				}
				
				curve->AddKey(keyframe);
			}
			curveInfo->SetCurve(curve);
			curveInfo->SetBoneIndex(fbxcurveInfo.boneIndex);
			curveInfo->SetBoneName(fbxcurveInfo.boneName);
			curveInfo->SetType(AnimCurveInfo::Type::Transform);
			curveInfo->SetProperty(fbxcurveInfo.prop);			

			animClip->SetCurve(curveInfo);
		}
	}
	else if(in[0].type == "Lcl Rotation")
	{
		if(in.size() == 3)
		{
			shared_ptr<AnimCurveInfo> xCurveInfo = std::make_shared<AnimCurveInfo>();
			shared_ptr<AnimationCurve> xCurve = std::make_shared<AnimationCurve>();
			shared_ptr<AnimCurveInfo> yCurveInfo = std::make_shared<AnimCurveInfo>();
			shared_ptr<AnimationCurve> yCurve = std::make_shared<AnimationCurve>();
			shared_ptr<AnimCurveInfo> zCurveInfo = std::make_shared<AnimCurveInfo>();
			shared_ptr<AnimationCurve> zCurve = std::make_shared<AnimationCurve>();
			for(int keyIndex = 0; keyIndex < in[0].curve->KeyGetCount(); ++keyIndex)
			{
				float x = in[0].curve->KeyGetValue(keyIndex);
				float y = in[1].curve->KeyGetValue(keyIndex);
				float z = in[2].curve->KeyGetValue(keyIndex);

				FbxVector4 rot(x, y, z, 1);
				FbxAMatrix rotMat;
				rotMat.SetR(rot);

				rotMat = preRotMatrix * rotMat;
				rot = rotMat.GetR();

				if(mAxisSystem == AxisSystem::Direct)
				{
					x = (float)rot[0];
					y = (float)-rot[1];
					z = (float)-rot[2];
				}
				else
				{
					x = (float)rot[0];
					y = (float)rot[1];
					z = (float)rot[2];
				}

				float xtime = (float)in[0].curve->KeyGetTime(keyIndex).GetSecondDouble();
				float ytime = (float)in[1].curve->KeyGetTime(keyIndex).GetSecondDouble();
				float ztime = (float)in[2].curve->KeyGetTime(keyIndex).GetSecondDouble();

				Keyframetemp xKey, yKey, zKey;
				xKey.value = x;	yKey.value = y;	zKey.value = z;
				xKey.time = xtime; yKey.time = ytime; zKey.time = ztime;

				xCurve->AddKey(xKey); yCurve->AddKey(yKey); zCurve->AddKey(zKey);
			}
			xCurveInfo->SetCurve(xCurve);
			xCurveInfo->SetBoneIndex(in[0].boneIndex);
			xCurveInfo->SetBoneName(in[0].boneName);
			xCurveInfo->SetType(AnimCurveInfo::Type::Transform);
			xCurveInfo->SetProperty(in[0].prop);

			yCurveInfo->SetCurve(yCurve);
			yCurveInfo->SetBoneIndex(in[1].boneIndex);
			yCurveInfo->SetBoneName(in[1].boneName);
			yCurveInfo->SetType(AnimCurveInfo::Type::Transform);
			yCurveInfo->SetProperty(in[1].prop);

			zCurveInfo->SetCurve(zCurve);
			zCurveInfo->SetBoneIndex(in[2].boneIndex);
			zCurveInfo->SetBoneName(in[2].boneName);
			zCurveInfo->SetType(AnimCurveInfo::Type::Transform);
			zCurveInfo->SetProperty(in[2].prop);		

			animClip->SetCurve(xCurveInfo);
			animClip->SetCurve(yCurveInfo);
			animClip->SetCurve(zCurveInfo);
		}
	}
}

void FBXLoader::LoadControlPoints(FbxNode* inNode)
{
	FbxMesh* currMesh = inNode->GetMesh();
	UINT ctrlPointCount = currMesh->GetControlPointsCount();
	for(UINT i = 0; i < ctrlPointCount; i++)
	{
		shared_ptr<CtrlPoint> currCtrlPoint = std::make_shared<CtrlPoint>();
		XMFLOAT3 currPosition;
		currPosition.x = (float)(currMesh->GetControlPointAt(i).mData[0]);
		currPosition.y = (float)(currMesh->GetControlPointAt(i).mData[1]);
		currPosition.z = (float)(currMesh->GetControlPointAt(i).mData[2]);
		currCtrlPoint->mPosition = currPosition;
		mControlPoints[i] = currCtrlPoint;
	}
}

//unsigned int FBXLoader::FindJointIndexUsingName(const std::wstring& inJointName)
//{
//	for(unsigned int i = 0; i < mSkeleton.mJoints.size(); i++)
//	{
//		if(mSkeleton.mJoints[i].mName == inJointName)
//		{
//			return i;
//		}
//	}
//
//	throw std::exception("skeleton information in FBX corrupted");
//}

void FBXLoader::LoadMesh(FbxNode* inNode, shared_ptr<ModelNode>& meshNode)
{
	FbxMesh* currMesh = inNode->GetMesh();

	FbxLayerElementArrayTemplate<int>* materialIndices;
	currMesh->GetMaterialIndices(&materialIndices);
	unsigned int materialIndex = 0;

	FbxStringList uvsetName;
	currMesh->GetUVSetNames(uvsetName);
	int numUVSet = uvsetName.GetCount();
	bool unmapped = false;


	// 폴리곤의 수만큼 정보를 얻어온다
	int polygonCount = currMesh->GetPolygonCount();
	UINT vertexCounter = 0;
	for(int i = 0; i < polygonCount; i++)
	{
		materialIndex = materialIndices->GetAt(i);
		XMFLOAT3 normal[3];
		XMFLOAT3 tangent[3];
		XMFLOAT3 binormal[3];

		int polygonSize = currMesh->GetPolygonSize(i);
		for(int j = polygonSize-1; j >= 0; j--)
		{
			// 폴리곤의 특정 정점의 인덱스를 통해 사전에 저장해둔 정점 정보를 얻어온다
			int ctrlPointIndex = currMesh->GetPolygonVertex(i, j);
			shared_ptr<CtrlPoint> currCtrlPoint = mControlPoints[ctrlPointIndex];

			// 폴리곤의 특정 정점의 normal 정보를 얻어온다
			FbxVector4 nor;
			currMesh->GetPolygonVertexNormal(i, j, nor);

			PNTIWVertex temp;
			temp.mPosition.x = currCtrlPoint->mPosition.x;
			temp.mPosition.y = currCtrlPoint->mPosition.y;
			temp.mPosition.z = currCtrlPoint->mPosition.z;
			temp.mNormal.x = (float)nor[0];
			temp.mNormal.y = (float)nor[1];
			temp.mNormal.z = (float)nor[2];

			if(mAxisSystem == AxisSystem::Direct)
			{
				temp.mPosition.x = -currCtrlPoint->mPosition.x;
				temp.mNormal.x = (float)-nor[0];
			}

			//uv setting
			for(int uvCount = 0; uvCount < numUVSet; ++uvCount)
			{
				FbxString name = uvsetName.GetStringAt(uvCount);
				FbxVector2 texCoord;
				currMesh->GetPolygonVertexUV(i, j, name, texCoord, unmapped);
				XMFLOAT2 uv;
				uv.x = (float)texCoord.mData[0];
				uv.y = (float)texCoord.mData[1];

				if(mAxisSystem == AxisSystem::Direct)
				{
					uv.y = 1 - (float)texCoord.mData[1];
				}

				temp.mUVs.push_back(uv);
			}
			//blending info
			for(UINT i = 0; i < currCtrlPoint->mBlendingInfo.size(); i++)
			{
				VertexBlendingInfo currBlendingInfo;
				currBlendingInfo.mBlendingIndex = currCtrlPoint->mBlendingInfo[i].mBlendingIndex;
				currBlendingInfo.mBlendingWeight = currCtrlPoint->mBlendingInfo[i].mBlendingWeight;
				temp.mVertexBlendingInfos.push_back(currBlendingInfo);
			}
			//sort the blending info
			temp.SortBlendingInfoByWeight();

			meshNode->vertices.push_back(temp);
			meshNode->submeshIndices[materialIndex].push_back(vertexCounter);

			++vertexCounter;
		}
	}

	mControlPoints.clear();
}


void FBXLoader::ReadUV(FbxMesh* inMesh, int inCtrlPointIndex, int inVertexCounter, PNTIWVertex& vertex)
{

}

void FBXLoader::ReadNormal(FbxMesh* inMesh, int inCtrlPointIndex, int inVertexCounter, XMFLOAT3& outNormal)
{
	if(inMesh->GetElementNormalCount() < 1)
	{
		throw std::exception("Invalid Normal Number");
	}

	FbxGeometryElementNormal* vertexNormal = inMesh->GetElementNormal(0);
	switch(vertexNormal->GetMappingMode())
	{
	case FbxGeometryElement::eByControlPoint:
		switch(vertexNormal->GetReferenceMode())
		{
		case FbxGeometryElement::eDirect:
		{
			outNormal.x = static_cast<float>(vertexNormal->GetDirectArray().GetAt(inCtrlPointIndex).mData[0]);
			outNormal.y = static_cast<float>(vertexNormal->GetDirectArray().GetAt(inCtrlPointIndex).mData[1]);
			outNormal.z = static_cast<float>(vertexNormal->GetDirectArray().GetAt(inCtrlPointIndex).mData[2]);
		}
		break;

		case FbxGeometryElement::eIndexToDirect:
		{
			int index = vertexNormal->GetIndexArray().GetAt(inCtrlPointIndex);
			outNormal.x = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[0]);
			outNormal.y = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[1]);
			outNormal.z = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[2]);
		}
		break;

		default:
			throw std::exception("Invalid Reference");
		}
		break;

	case FbxGeometryElement::eByPolygonVertex:
		switch(vertexNormal->GetReferenceMode())
		{
		case FbxGeometryElement::eDirect:
		{
			outNormal.x = static_cast<float>(vertexNormal->GetDirectArray().GetAt(inVertexCounter).mData[0]);
			outNormal.y = static_cast<float>(vertexNormal->GetDirectArray().GetAt(inVertexCounter).mData[1]);
			outNormal.z = static_cast<float>(vertexNormal->GetDirectArray().GetAt(inVertexCounter).mData[2]);
		}
		break;

		case FbxGeometryElement::eIndexToDirect:
		{
			int index = vertexNormal->GetIndexArray().GetAt(inVertexCounter);
			outNormal.x = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[0]);
			outNormal.y = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[1]);
			outNormal.z = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[2]);
		}
		break;

		default:
			throw std::exception("Invalid Reference");
		}
		break;
	}
}

void FBXLoader::ReadBinormal(FbxMesh* inMesh, int inCtrlPointIndex, int inVertexCounter, XMFLOAT3& outBinormal)
{
	if(inMesh->GetElementBinormalCount() < 1)
	{
		throw std::exception("Invalid Binormal Number");
	}

	FbxGeometryElementBinormal* vertexBinormal = inMesh->GetElementBinormal(0);
	switch(vertexBinormal->GetMappingMode())
	{
	case FbxGeometryElement::eByControlPoint:
		switch(vertexBinormal->GetReferenceMode())
		{
		case FbxGeometryElement::eDirect:
		{
			outBinormal.x = static_cast<float>(vertexBinormal->GetDirectArray().GetAt(inCtrlPointIndex).mData[0]);
			outBinormal.y = static_cast<float>(vertexBinormal->GetDirectArray().GetAt(inCtrlPointIndex).mData[1]);
			outBinormal.z = static_cast<float>(vertexBinormal->GetDirectArray().GetAt(inCtrlPointIndex).mData[2]);
		}
		break;

		case FbxGeometryElement::eIndexToDirect:
		{
			int index = vertexBinormal->GetIndexArray().GetAt(inCtrlPointIndex);
			outBinormal.x = static_cast<float>(vertexBinormal->GetDirectArray().GetAt(index).mData[0]);
			outBinormal.y = static_cast<float>(vertexBinormal->GetDirectArray().GetAt(index).mData[1]);
			outBinormal.z = static_cast<float>(vertexBinormal->GetDirectArray().GetAt(index).mData[2]);
		}
		break;

		default:
			throw std::exception("Invalid Reference");
		}
		break;

	case FbxGeometryElement::eByPolygonVertex:
		switch(vertexBinormal->GetReferenceMode())
		{
		case FbxGeometryElement::eDirect:
		{
			outBinormal.x = static_cast<float>(vertexBinormal->GetDirectArray().GetAt(inVertexCounter).mData[0]);
			outBinormal.y = static_cast<float>(vertexBinormal->GetDirectArray().GetAt(inVertexCounter).mData[1]);
			outBinormal.z = static_cast<float>(vertexBinormal->GetDirectArray().GetAt(inVertexCounter).mData[2]);
		}
		break;

		case FbxGeometryElement::eIndexToDirect:
		{
			int index = vertexBinormal->GetIndexArray().GetAt(inVertexCounter);
			outBinormal.x = static_cast<float>(vertexBinormal->GetDirectArray().GetAt(index).mData[0]);
			outBinormal.y = static_cast<float>(vertexBinormal->GetDirectArray().GetAt(index).mData[1]);
			outBinormal.z = static_cast<float>(vertexBinormal->GetDirectArray().GetAt(index).mData[2]);
		}
		break;

		default:
			throw std::exception("Invalid Reference");
		}
		break;
	}
}

void FBXLoader::ReadTangent(FbxMesh* inMesh, int inCtrlPointIndex, int inVertexCounter, XMFLOAT3& outTangent)
{
	if(inMesh->GetElementTangentCount() < 1)
	{
		throw std::exception("Invalid Tangent Number");
	}

	FbxGeometryElementTangent* vertexTangent = inMesh->GetElementTangent(0);
	switch(vertexTangent->GetMappingMode())
	{
	case FbxGeometryElement::eByControlPoint:
		switch(vertexTangent->GetReferenceMode())
		{
		case FbxGeometryElement::eDirect:
		{
			outTangent.x = static_cast<float>(vertexTangent->GetDirectArray().GetAt(inCtrlPointIndex).mData[0]);
			outTangent.y = static_cast<float>(vertexTangent->GetDirectArray().GetAt(inCtrlPointIndex).mData[1]);
			outTangent.z = static_cast<float>(vertexTangent->GetDirectArray().GetAt(inCtrlPointIndex).mData[2]);
		}
		break;

		case FbxGeometryElement::eIndexToDirect:
		{
			int index = vertexTangent->GetIndexArray().GetAt(inCtrlPointIndex);
			outTangent.x = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index).mData[0]);
			outTangent.y = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index).mData[1]);
			outTangent.z = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index).mData[2]);
		}
		break;

		default:
			throw std::exception("Invalid Reference");
		}
		break;

	case FbxGeometryElement::eByPolygonVertex:
		switch(vertexTangent->GetReferenceMode())
		{
		case FbxGeometryElement::eDirect:
		{
			outTangent.x = static_cast<float>(vertexTangent->GetDirectArray().GetAt(inVertexCounter).mData[0]);
			outTangent.y = static_cast<float>(vertexTangent->GetDirectArray().GetAt(inVertexCounter).mData[1]);
			outTangent.z = static_cast<float>(vertexTangent->GetDirectArray().GetAt(inVertexCounter).mData[2]);
		}
		break;

		case FbxGeometryElement::eIndexToDirect:
		{
			int index = vertexTangent->GetIndexArray().GetAt(inVertexCounter);
			outTangent.x = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index).mData[0]);
			outTangent.y = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index).mData[1]);
			outTangent.z = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index).mData[2]);
		}
		break;

		default:
			throw std::exception("Invalid Reference");
		}
		break;
	}
}

void FBXLoader::AssociateMaterialToMesh(FbxNode* inNode, shared_ptr<ModelNode>& meshNode)
{
	FbxGeometryElement::EMappingMode materialMappingMode = FbxGeometryElement::eNone;
	FbxMesh* currMesh = inNode->GetMesh();

	int materialCount = inNode->GetMaterialCount();
	for(int i = 0; i < materialCount; ++i)
	{
		FbxSurfaceMaterial* material = inNode->GetMaterial(i);
		std::string str = material->GetName();
		std::wstring name(str.begin(), str.end());
		meshNode->materialNames.push_back(name);
	}
}

void FBXLoader::LoadMaterials(FbxScene* scene)
{
	unsigned int materialCount = scene->GetMaterialCount();

	for(unsigned int i = 0; i < materialCount; ++i)
	{
		ModelMaterial material;
		FbxSurfaceMaterial* surfaceMaterial = scene->GetMaterial(i);
		LoadMaterialAttribute(surfaceMaterial, material);
		LoadMaterialTexture(surfaceMaterial, material);
		CreateMaterial(material);
	}
}
void FBXLoader::LoadMaterialAttribute(FbxSurfaceMaterial* inMaterial, ModelMaterial& mat)
{
	FbxDouble3 double3;
	FbxDouble double1;
	
	std::string str = inMaterial->GetName();
	mat.name.assign(str.begin(), str.end());

	std::string className = inMaterial->GetClassId().GetName();
	
	if(className == "FbxSurfaceLambert")
	{
		mat.type = ModelMaterial::MaterialType::Lambert;
	
		//ambient color
		double3 = reinterpret_cast<FbxSurfaceLambert*>(inMaterial)->Ambient;
		mat.ambient = XMFLOAT4((float)double3[0], (float)double3[1], (float)double3[2], 1);

		//diffuse color
		double3 = reinterpret_cast<FbxSurfaceLambert*>(inMaterial)->Diffuse;
		mat.diffuse = XMFLOAT4((float)double3[0], (float)double3[1], (float)double3[2], 1);

		//Emissive color
		double3 = reinterpret_cast<FbxSurfaceLambert*>(inMaterial)->Emissive;
		mat.emissive = XMFLOAT4((float)double3[0], (float)double3[1], (float)double3[2], 1);

		//transparency factor
		double1 = reinterpret_cast<FbxSurfaceLambert*>(inMaterial)->TransparencyFactor;
		mat.transparencyFactor = double1;
	}	

	if(className == "FbxSurfacePhong")
	{
		mat.type = ModelMaterial::MaterialType::Phong;

		//specular color
		double3 = reinterpret_cast<FbxSurfacePhong*>(inMaterial)->Specular;
		mat.specular = XMFLOAT4((float)double3[0], (float)double3[1], (float)double3[2], 1);

		//reflection
		double3 = reinterpret_cast<FbxSurfacePhong*>(inMaterial)->Reflection;
		mat.reflection = XMFLOAT4((float)double3[0], (float)double3[1], (float)double3[2], 1);

		//shininess
		double1 = reinterpret_cast<FbxSurfacePhong*>(inMaterial)->Shininess;
		mat.shininess = double1;

		//specular factor
		double1 = reinterpret_cast<FbxSurfacePhong*>(inMaterial)->SpecularFactor;
		mat.specularIntensity = double1;

		//reflection factor
		double1 = reinterpret_cast<FbxSurfacePhong*>(inMaterial)->ReflectionFactor;
		mat.reflectionFactor = double1;
	}
}
void FBXLoader::LoadMaterialTexture(FbxSurfaceMaterial* inMaterial, ModelMaterial& mat)
{
	unsigned int textureIndex = 0;
	FbxProperty property;

	FBXSDK_FOR_EACH_TEXTURE(textureIndex)
	{
		property = inMaterial->FindProperty(FbxLayerElement::sTextureChannelNames[textureIndex]);
		if(property.IsValid())
		{
			unsigned int textureCount = property.GetSrcObjectCount<FbxTexture>();
			for(unsigned int i = 0; i < textureCount; i++)
			{
				FbxLayeredTexture* layeredTexture = property.GetSrcObject<FbxLayeredTexture>(i);
				if(layeredTexture)
				{
					throw std::exception("layered texture is unsupported\n");
				}
				else
				{
					FbxTexture* texture = property.GetSrcObject<FbxTexture>(i);
					if(texture)
					{
						std::string textureType = property.GetNameAsCStr();
						FbxFileTexture* fileTexture = FbxCast<FbxFileTexture>(texture);

						if(fileTexture)
						{
							std::string str = fileTexture->GetFileName();
							FbxString fbxStr = FbxPathUtils::GetFileName(str.c_str(), true);
							str = fbxStr;
							if(textureType == "DiffuseColor")
							{
								mat.diffuseMapName.assign(str.begin(), str.end());
								fbxStr = FbxPathUtils::GetFileName(str.c_str(), false);
								str = fbxStr;
								inMaterial->SetName(str.c_str());
								mat.name.assign(str.begin(), str.end());
							}
							else if(textureType == "SpecularColor")
							{
								mat.specularMapName.assign(str.begin(), str.end());
							}
							else if(textureType == "Bump")
							{
								mat.normalMapName.assign(str.begin(), str.end());
							}
						}
					}
				}
			}
		}
	}
}

void FBXLoader::ComputeNodeMatrix(FbxNode* inNode, shared_ptr<ModelNode> meshNode)
{
	if(!inNode || !meshNode)
		return;

	FbxAnimEvaluator* evaluator = mFBXScene->GetAnimationEvaluator();
	FbxAMatrix global;
	global.SetIdentity();
	FbxAMatrix local;

	if(inNode != mFBXScene->GetRootNode())
	{
		global = evaluator->GetNodeGlobalTransform(inNode);
		local = evaluator->GetNodeLocalTransform(inNode);
	}

	if(mAxisSystem == AxisSystem::Direct)
	{
		FbxVector4 t = local.GetT();
		FbxQuaternion q = local.GetQ();
		FbxVector4 r = local.GetR();
		FbxVector4 s = local.GetS();
		t[0] = -t[0];
		q[1] = -q[1];
		q[2] = -q[2];
		r[1] = -r[1];
		r[2] = -r[2];
		local = FbxAMatrix(t, r, s);
	}

	FBXMatrixToXMMATRIX(local, meshNode->matrix);
}

void FBXLoader::CleanupFbxManager()
{
	mFBXScene->Destroy();
	mFBXManager->Destroy();
}