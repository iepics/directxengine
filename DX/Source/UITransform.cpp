#include "stdafx.h"
#include "UITransform.h"


UITransform::UITransform()
{
	mPivot.x = 0.5f;
	mPivot.y = 0.5f;
}


UITransform::~UITransform()
{
}

XMMATRIX UITransform::GetWorldMatrix()
{
	auto xPivot = mPivot.x - 0.5f;
	auto yPivot = mPivot.y - 0.5f;
	auto pivotPos = XMVectorSet(xPivot * mWidth, yPivot*mHeight, 0, 0);

	XMVECTOR pos = XMLoadFloat3(&GetLocalPosition());
	XMVECTOR quat = XMLoadFloat4(&GetLocalRotation());
	XMVECTOR scale = XMLoadFloat3(&GetLocalScale());
	XMMATRIX localMat = XMMatrixAffineTransformation(scale, pivotPos, quat, pos-pivotPos);

	auto parentObj = GetGameObject()->GetParent();
	if (parentObj != nullptr)
	{
		auto parentWorldMat = parentObj->GetTransform()->GetWorldMatrix();
		localMat = localMat * parentWorldMat;
	}

	return localMat;
}

void UITransform::Render()
{
	ID3D11DeviceContext* deviceContext = D3D::GetInstance()->GetDeviceContext();

	ShaderBuffer* buffer = ShaderBuffer::GetInstance();
	std::shared_ptr<Effect> effect = ResourceManager::GetInstance()->GetEffect(L"Resource/FX/Debug.fx");
	effect->SetMatrix("gWorld", buffer->GetWorld());
	effect->SetMatrix("gView", buffer->GetView());
	effect->SetMatrix("gProjection", buffer->GetProjection());

	deviceContext->IASetInputLayout(effect->GetInputLayout());
	effect->GetTechnique("Default")->GetPassByIndex(0)->Apply(0, deviceContext);

	//정점 버퍼 생성
	D3D11_BUFFER_DESC vertexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData;
	HRESULT result;

	ModelVertex vertices[4];

	vertices[0].position = XMFLOAT3(mWidth / 2, mHeight / 2, 0);
	vertices[1].position = XMFLOAT3(-mWidth / 2, mHeight / 2, 0);
	vertices[2].position = XMFLOAT3(-mWidth / 2, -mHeight / 2, 0);
	vertices[3].position = XMFLOAT3(mWidth / 2, -mHeight / 2, 0);

	ZeroMemory(&vertexBufferDesc, sizeof(D3D11_BUFFER_DESC));
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(ModelVertex) * 4;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	vertexData.pSysMem = &vertices[0];
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	D3D* d3d = D3D::GetInstance();
	ID3D11Buffer* vertexBuffer;
	result = d3d->GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexData, &vertexBuffer);
	if (FAILED(result))
	{
		return;
	}


	//인덱스 버퍼 생성
	D3D11_BUFFER_DESC indexBufferDesc;
	D3D11_SUBRESOURCE_DATA indexData;

	unsigned int indices[5];
	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;
	indices[3] = 3;
	indices[4] = 0;

	ZeroMemory(&indexBufferDesc, sizeof(D3D11_BUFFER_DESC));
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(UINT) * 5;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	indexData.pSysMem = &indices[0];
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	ID3D11Buffer* indexBuffer;
	result = d3d->GetDevice()->CreateBuffer(&indexBufferDesc, &indexData, &indexBuffer);
	if (FAILED(result))
	{
		return;
	}

	//드로우콜
	UINT stride;
	UINT offset;

	stride = sizeof(ModelVertex);
	offset = 0;

	deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	deviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP);

	deviceContext->DrawIndexed(5, 0, 0);

	vertexBuffer->Release();
	indexBuffer->Release();
}

void UITransform::SetLocal()
{
	//auto xPivot = mPivot.x - 0.5f;
	//auto yPivot = mPivot.y - 0.5f;
	//auto pivotPos = XMVectorSet(xPivot * mWidth, yPivot * mHeight, 0, 0);
	//

	//XMVECTOR pos = XMLoadFloat3(&GetLocalPosition());
	//XMVECTOR quat = XMLoadFloat4(&GetLocalRotation());
	//XMVECTOR scale = XMLoadFloat3(&GetLocalScale());
	//XMMATRIX localMat = XMMatrixAffineTransformation(scale, pivotPos, quat, pos - pivotPos);
	//XMStoreFloat4x4(&mLocalMatrix, localMat);

	//auto worldMat = localMat;
	///*if(mParent.lock() != nullptr)
	//{
	//auto parentWorldMat = mParent.lock()->GetWorldMatrix();
	//worldMat = localMat * parentWorldMat;
	//}*/
	//auto parentObj = GetGameObject()->GetParent();
	//if (parentObj != nullptr)
	//{
	//	auto parentWorldMat = parentObj->GetTransform()->GetWorldMatrix();
	//	worldMat = localMat * parentWorldMat;
	//}
	//XMStoreFloat4x4(&mWorldMatrix, worldMat);

	//XMMatrixDecompose(&scale, &quat, &pos, worldMat);
	//XMStoreFloat3(&mPosition, pos);
	//XMStoreFloat4(&mRotation, quat);
	//XMStoreFloat3(&mScale, scale);
	//XMFLOAT3 angle;
	//angle = MathUtility::ToEulerAngles(mRotation);
	//mAngle = angle;
}

void UITransform::SetWorld()
{
	//auto xPivot = mPivot.x - 0.5f;
	//auto yPivot = mPivot.y - 0.5f;
	//auto pivotPos = XMVectorSet(xPivot * mWidth, yPivot*mHeight, 0, 0);


	//XMVECTOR pos = XMLoadFloat3(&GetPosition());
	//XMVECTOR quat = XMLoadFloat4(&GetRotation());
	//XMVECTOR scale = XMLoadFloat3(&GetScale());
	//XMMATRIX worldMat = XMMatrixAffineTransformation(scale, pivotPos, quat, pos- pivotPos);
	//XMStoreFloat4x4(&mWorldMatrix, worldMat);

	//auto localMat = worldMat;
	////if(mParent.lock() != nullptr)
	////{
	////	auto parentWorldMat = mParent.lock()->GetWorldMatrix();
	////	auto parentWorldMatInv = XMMatrixInverse(0, parentWorldMat);
	////	localMat = worldMat * parentWorldMatInv;
	////}
	//auto parentObj = GetGameObject()->GetParent();
	//if (parentObj != nullptr)
	//{
	//	auto parentWorldMat = parentObj->GetTransform()->GetWorldMatrix();
	//	auto parentWorldMatInv = XMMatrixInverse(0, parentWorldMat);
	//	localMat = worldMat * parentWorldMatInv;
	//}
	//XMStoreFloat4x4(&mLocalMatrix, localMat);

	//XMMatrixDecompose(&scale, &quat, &pos, localMat);
	//XMStoreFloat3(&mLocalPosition, pos);
	//XMStoreFloat4(&mLocalRotation, quat);
	//XMStoreFloat3(&mLocalScale, scale);
	//XMFLOAT3 angle;
	//angle = MathUtility::ToEulerAngles(mLocalRotation);
	//mLocalAngle = angle;
}
