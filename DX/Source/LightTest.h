#pragma once
#include "GameTimer.h"
#include "Input.h"
#include "Behaviour.h"
#include "MathUtilities.h"

class LightTest : public Cloneable<Behaviour, LightTest>
{
public:
	LightTest(){}
	~LightTest(){}

	void Update()
	{
		auto transform = GetGameObject()->GetTransform();

		if (Input::GetInstance()->GetKeyDown(KeyCode::R))
		{	
			rotate = !rotate;			
		}

		if(rotate)
			transform->Rotate(XMFLOAT3(0, 10, 0) * GameTimer::GetInstance()->GetDeltaTime());
		else
			transform->SetLocalAngle(XMFLOAT3(45, 135, 0));
	}

private:
	bool rotate;
};

