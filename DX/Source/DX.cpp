// DX.cpp : 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "DX.h"
#include "System.h"

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{

	//
	AllocConsole();
	freopen("CONOUT$", "wt", stdout);
	//

	bool result;
	GameTimer* timer = GameTimer::GetInstance();
	System* system = System::GetInstance();
	
	//시스템 초기화
	timer->Initialize();
	result = system->Initialize(800, 600, false, false);
	if(result)
	{
		system->Run();
	}

	system->Destroy();
//	delete system;


	return 0;
}