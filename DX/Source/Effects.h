#pragma once
#include <unordered_map>
#include "d3dx11effect.h"
#include "Light.h"
#include "Material.h"


#pragma region Effect
class Effect
{
public:
	Effect();
	~Effect();

	bool FromFile(const std::wstring& fileName);
	bool FromData(const std::string& data);

	CComPtr<ID3DX11EffectTechnique> GetTechnique(std::string name);
	CComPtr<ID3DX11EffectVariable> GetVariable(std::string name);
	CComPtr<ID3D11InputLayout> GetInputLayout(){ return mInputLayout; }

	void SetMatrix(std::string name, CXMMATRIX matrix);
	void SetBoneTransforms(const XMMATRIX* M, int cnt);
	void SetTexture(std::string name, CComPtr<ID3D11ShaderResourceView> texture);
	void SetVariable(std::string name, unsigned int size, void* pData); 

private:
	void OutputErrorMessage(CComPtr<ID3DBlob> pd3dBlob, const std::wstring& fileName);

	CComPtr<ID3DX11Effect> mFX;
	std::unordered_map<std::string, CComPtr<ID3DX11EffectTechnique>> mTechniques;
	std::unordered_map<std::string, CComPtr<ID3DX11EffectVariable>> mVariables;

	ID3D11InputLayout* mInputLayout;
};
#pragma endregion

#pragma region UberShader
class UberShader
{
public:

	UberShader() = default;
	~UberShader() = default;

private:

};
#pragma endregion
