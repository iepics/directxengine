#pragma once

class Object
{
public:
	static std::shared_ptr<Object> Instantiate(std::shared_ptr<Object> origin);

public:
	virtual ~Object();

	virtual void SetName(std::wstring name){ mName = name; }
	std::wstring GetName(){ return mName; }
	virtual std::shared_ptr<Object> Clone() { return std::make_shared<Object>(static_cast<const Object&>(*this)); }

protected:	
	Object();
	std::wstring mName;
};



template <typename Base, typename Derived>
class Cloneable : public Base
{
public:
	virtual std::shared_ptr<Object> Clone()
	{
		return std::make_shared<Derived>(static_cast<const Derived&>(*this));
	}
};