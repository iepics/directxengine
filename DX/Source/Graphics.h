#pragma once
#include "D3D.h"
#include "GameObject.h"
#include "Utilities.h"
#include "Scene.h"
#include "ShaderBuffer.h"
#include "DeferredBuffers.h"
#include "ResourceManager.h"
#include "Font.h"

class Graphics
{
public:
	Graphics(void);
	~Graphics(void);

	bool Initialize(HWND hWnd);
	void Destroy();
	void Frame();

	D3D* GetD3D(){ return m_D3D; }

private:
	void Render();
	void Update();

	D3D *m_D3D;

	std::shared_ptr<Scene> mCurrentScene;
};