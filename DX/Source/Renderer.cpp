#include "stdafx.h"
#include "Renderer.h"

using std::shared_ptr;

std::list<std::weak_ptr<Renderer>> Renderer::allRenderers;
//std::list<std::weak_ptr<UIRenderer>> UIRenderer::allRenderers;

#pragma region MeshRenderer
MeshRenderer::MeshRenderer()
{
}
MeshRenderer::~MeshRenderer()
{
}

void MeshRenderer::Render(const std::shared_ptr<Effect>& fx)
{
	unsigned int materialCount = (unsigned int)mMaterials.size();
	auto deviceContext = D3D::GetInstance()->GetDeviceContext();
	for(unsigned int i = 0; i < materialCount; ++i)
	{
		if(mMaterials[i] != nullptr)
		{
			mMaterials[i]->SetMatBuffer();

			ShaderBuffer* buffer = ShaderBuffer::GetInstance();

			auto effect = fx;
			if(effect == nullptr)
				effect = mMaterials[i]->GetEffect();
			effect->SetMatrix("gWorld", buffer->GetWorld());
			effect->SetMatrix("gView", buffer->GetView());
			effect->SetMatrix("gProjection", buffer->GetProjection());
			effect->SetVariable("gColor", sizeof(buffer->Color), &buffer->Color);
			effect->SetVariable("gPosition", sizeof(buffer->Position), &buffer->Position);
			effect->SetVariable("gMaterial", sizeof(buffer->MatBuffer), &buffer->MatBuffer);
			effect->SetVariable("gCameraPos", sizeof(buffer->CameraPos), &buffer->CameraPos);

			if(mMaterials[i]->GetTexture("_diffuse") != nullptr)
			{
				effect->SetTexture("gDiffuseMap", mMaterials[i]->GetTexture("_diffuse")->GetTexture());
			}
			else
			{
				auto white = ResourceManager::GetInstance()->GetTexture(L"whiteTexture");
				effect->SetTexture("gDiffuseMap", white->GetTexture());
			}

			auto light = Light::GetDirectionalLight();
			if(light != nullptr)
			{
				effect->SetTexture("gShadowMap", light->GetShadowMap()->GetTexture());
				effect->SetMatrix("gLightView", XMLoadFloat4x4(&buffer->LightView));
				effect->SetMatrix("gLightProj", XMLoadFloat4x4(&buffer->LightProj));
			}

			deviceContext->IASetInputLayout(effect->GetInputLayout());
			effect->GetTechnique("Default")->GetPassByIndex(0)->Apply(0, deviceContext);

		}

		UINT stride;
		UINT offset;

		stride = sizeof(ModelVertex);
		offset = 0;

		SubMesh submesh = mMesh->GetSubMesh(i);
		ID3D11Buffer* vertexBuffer = mMesh->GetVertexBuffer();
		ID3D11Buffer* indexBuffer = mMesh->GetIndexBuffer(i);

		deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		deviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
		deviceContext->IASetPrimitiveTopology(mMesh->GetPrimitiveTopology());

		deviceContext->DrawIndexed((unsigned int)submesh.indices.size(), 0, 0);
	}
}
#pragma endregion


#pragma region SkinnedMeshRenderer
SkinnedMeshRenderer::SkinnedMeshRenderer()
{

}
SkinnedMeshRenderer::~SkinnedMeshRenderer()
{

}

void SkinnedMeshRenderer::OnLoad()
{
	if (mRootBoneName != L"")
	{
		auto obj = GetGameObject();
		while (obj->GetParent() != nullptr)
		{
			obj = obj->GetParent();
		}
		auto rootBoneObj = obj->Find(mRootBoneName, true);
		SetRootBone(rootBoneObj);
	}
}

void SkinnedMeshRenderer::Render(const std::shared_ptr<Effect>& fx)
{
	unsigned int materialCount = (unsigned int)mMaterials.size();
	auto deviceContext = D3D::GetInstance()->GetDeviceContext();
	for(unsigned int i = 0; i < materialCount; ++i)
	{
		if (mMaterials[i] != nullptr)
		{
			mMaterials[i]->SetMatBuffer();

			ShaderBuffer* buffer = ShaderBuffer::GetInstance();
			
			auto effect = fx;
			if(effect == nullptr)
				effect = mMaterials[i]->GetEffect();
			effect->SetMatrix("gWorld", buffer->GetWorld());
			effect->SetMatrix("gView", buffer->GetView());
			effect->SetMatrix("gProjection", buffer->GetProjection());
			effect->SetVariable("gColor", sizeof(buffer->Color), &buffer->Color);
			effect->SetVariable("gPosition", sizeof(buffer->Position), &buffer->Position);
			effect->SetVariable("gMaterial", sizeof(buffer->MatBuffer), &buffer->MatBuffer);
			effect->SetVariable("gCameraPos", sizeof(buffer->CameraPos), &buffer->CameraPos);

			if (mMaterials[i]->GetTexture("_diffuse") != nullptr)
			{
				effect->SetTexture("gDiffuseMap", mMaterials[i]->GetTexture("_diffuse")->GetTexture());
			}
			else
			{
				auto white = ResourceManager::GetInstance()->GetTexture(L"whiteTexture");
				effect->SetTexture("gDiffuseMap", white->GetTexture());
			}

			auto light = Light::GetDirectionalLight();
			if(light != nullptr)
			{
				effect->SetTexture("gShadowMap", light->GetShadowMap()->GetTexture());
				effect->SetMatrix("gLightView", XMLoadFloat4x4(&buffer->LightView));
				effect->SetMatrix("gLightProj", XMLoadFloat4x4(&buffer->LightProj));
			}

			deviceContext->IASetInputLayout(effect->GetInputLayout());
			effect->GetTechnique("Skinned")->GetPassByIndex(0)->Apply(0, deviceContext);

			std::vector<XMMATRIX> transforms;
			for(unsigned int i = 0; i < mMesh->GetBindPoseCount()-1; ++i)
			{
				XMMATRIX bindPose = XMLoadFloat4x4(&mMesh->GetBindPose(i));
				XMMATRIX boneTrans = mBones[i]->GetWorldMatrix();
				XMMATRIX mat = bindPose * boneTrans;
				transforms.push_back(mat);
			}
			effect->SetBoneTransforms(&transforms[0], (int)transforms.size());
		}

		UINT stride;
		UINT offset;

		stride = sizeof(ModelVertex);
		offset = 0;

		SubMesh submesh = mMesh->GetSubMesh(i);
		ID3D11Buffer* vertexBuffer = mMesh->GetVertexBuffer();
		ID3D11Buffer* indexBuffer = mMesh->GetIndexBuffer(i);

		deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		deviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
		deviceContext->IASetPrimitiveTopology(mMesh->GetPrimitiveTopology());

		deviceContext->DrawIndexed((unsigned int)submesh.indices.size(), 0, 0);
	}
}

void SkinnedMeshRenderer::SetRootBoneByName(const std::wstring name)
{
	mRootBoneName = name;
}

void SkinnedMeshRenderer::SetRootBone(const shared_ptr<GameObject>& rootBone)
{
	mBones.push_back(rootBone->GetTransform());
	for(unsigned int i = 0; i < rootBone->GetChildCount(); ++i)
	{
		SetRootBone(rootBone->GetChild(i));
	}
}
#pragma endregion


#pragma region UIRenderer
UIRenderer::UIRenderer()
{
	mEffect = ResourceManager::GetInstance()->GetEffect(L"Resource/FX/UI.fx");
}
void UIRenderer::Render()
{
	if(mMesh != nullptr)
	{
		ID3D11DeviceContext* deviceContext = D3D::GetInstance()->GetDeviceContext();
		GlyphBank* gbInstance = GlyphBank::GetInstance();
		//ID3D11ShaderResourceView *texture = gbInstance->GetFTTextureResource();

		ShaderBuffer* buffer = ShaderBuffer::GetInstance();
		mEffect->SetMatrix("gWorld", buffer->GetWorld());
		mEffect->SetMatrix("gView", buffer->GetView());
		mEffect->SetMatrix("gProjection", buffer->GetProjection());
		mEffect->SetTexture("gDiffuseMap", mTexture);
		mEffect->SetVariable("gMainColor", sizeof(mColor), &mColor);

		deviceContext->IASetInputLayout(mEffect->GetInputLayout());
		mEffect->GetTechnique("Default")->GetPassByIndex(0)->Apply(0, deviceContext);

		///��ο���
		deviceContext->OMSetDepthStencilState(RenderStates::DepthOffDDS, 1);
		deviceContext->OMSetBlendState(RenderStates::TransparentBS, 0, 0xffffffff);
		UINT stride;
		UINT offset;

		stride = sizeof(ModelVertex);
		offset = 0;

		SubMesh submesh = mMesh->GetSubMesh(0);
		ID3D11Buffer* vertexBuffer = mMesh->GetVertexBuffer();
		ID3D11Buffer* indexBuffer = mMesh->GetIndexBuffer(0);

		deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		deviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
		deviceContext->IASetPrimitiveTopology(mMesh->GetPrimitiveTopology());

		deviceContext->DrawIndexed((unsigned int)submesh.indices.size(), 0, 0);

		deviceContext->OMSetDepthStencilState(0, 0);
		deviceContext->OMSetBlendState(0, 0, 0xffffffff);
	}
}
#pragma endregion