#include "stdafx.h"
#include "MathUtilities.h"


DirectX::XMFLOAT3 operator+(const DirectX::XMFLOAT3 &left, const DirectX::XMFLOAT3 &right)
{
	DirectX::XMFLOAT3 temp;
	temp.x = left.x + right.x;
	temp.y = left.y + right.y;
	temp.z = left.z + right.z;

	return temp;
}

DirectX::XMFLOAT3 operator-(const DirectX::XMFLOAT3 &left, const DirectX::XMFLOAT3 &right)
{
	DirectX::XMFLOAT3 temp;
	temp.x = left.x - right.x;
	temp.y = left.y - right.y;
	temp.z = left.z - right.z;

	return temp;
}

DirectX::XMFLOAT3& operator+=(DirectX::XMFLOAT3 &left, const DirectX::XMFLOAT3 &right)
{
	left = left + right;

	return left;
}

DirectX::XMFLOAT3& operator-=(DirectX::XMFLOAT3 &left, const DirectX::XMFLOAT3 &right)
{
	left = left - right;

	return left;
}

DirectX::XMFLOAT3 operator*(const DirectX::XMFLOAT3 &left, const float right)
{
	DirectX::XMFLOAT3 temp;
	temp.x = left.x * right;
	temp.y = left.y * right;
	temp.z = left.z * right;

	return temp;
}

DirectX::XMFLOAT3& operator*=(DirectX::XMFLOAT3 &left, const float right)
{
	left = left * right;

	return left;
}

DirectX::XMFLOAT4 operator+(const DirectX::XMFLOAT4 &left, const DirectX::XMFLOAT4 &right)
{
	DirectX::XMFLOAT4 temp;
	temp.x = left.x + right.x;
	temp.y = left.y + right.y;
	temp.z = left.z + right.z;
	temp.w = left.w + right.w;

	return temp;
}
DirectX::XMFLOAT4& operator+=(DirectX::XMFLOAT4 &left, const DirectX::XMFLOAT4 &right)
{
	left = left + right;

	return left;
}



