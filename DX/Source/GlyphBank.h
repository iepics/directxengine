#pragma once
#include <ft2build.h>
#include FT_FREETYPE_H
#include "Texture.h"
#include "Singleton.h"


class GlyphBank : public Singleton<GlyphBank>
{
public:
	GlyphBank();
	~GlyphBank();

	///
	struct Glyph
	{
		FT_Face face;
		wchar_t ch;
		int size;
		bool operator < (const Glyph& g) const
		{
			if (face < g.face) return true;
			else if (face > g.face) return false;
			if (ch < g.ch) return true;
			else if (ch > g.ch) return false;
			if (size < g.size) return true;
			return false;
		}
	};

	struct GlyphData
	{
		float u, v, w, h; //텍스쳐 좌표상의 지점
		int left, top; //왼쪽, 위쪽 여백
		int bmW, bmH; //가로, 세로 픽셀 개수
		int advX, advY; //다음 문자까지의 x, y값
		int height; //높이
		bool used; //텍스쳐 출력 여부
	};

	void Initialize(int width, int height);
	void Destroy();

	FT_Library GetFTL() const { return mFTLibrary; }
	ID3D11Texture2D* GetFTTexture() const { return mFTTexture; }
	ID3D11ShaderResourceView* GetFTTextureResource() const { return mFTTextureResource; }

	std::wstring PrepareGlyph(FT_Face face, std::wstring str, int len, int size);
	GlyphData GetGlyph(FT_Face face, wchar_t ch, int size);
	void CleanGlyphMap();

protected:
	const char& MapAt(int x, int y) const { return mTextureMap[x + y*(mTextureW)]; }
	char& MapAt(int x, int y) { return mTextureMap[x + y*(mTextureW)]; }
	bool TestSpace(int x, int y, int w, int h) const;
	void FillSpace(int x, int y, int w, int h, char fill = 1);
	std::pair<int, int> FindSpace(int w, int h) const;

	FT_Library mFTLibrary;
	ID3D11Texture2D* mFTStagingTexture;
	ID3D11Texture2D* mFTTexture;
	ID3D11ShaderResourceView* mFTTextureResource;
	//std::shared_ptr<Texture2D> mFTStagingTexture;
	//std::shared_ptr<Texture2D> mFTTexture;
	int mTextureW;
	int mTextureH;
	std::vector<char> mTextureMap;
	std::map<Glyph, GlyphData> mGlyphMap;
};

