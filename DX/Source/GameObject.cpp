#include "stdafx.h"
#include "GameObject.h"

using std::shared_ptr;

GameObject::GameObject(void)
{
}

inline GameObject::GameObject(const GameObject & other)
{
	mName = other.mName;

	for (auto com : other.mComponents)
	{
		mComponents[com.first] = std::static_pointer_cast<Component, Object>(com.second->Clone());
	}

	for (int i = 0; i < other.GetChildCount(); ++i)
	{
		auto cloneChild = std::static_pointer_cast<GameObject, Object>(other.GetChild(i)->Clone());
		SetChild(cloneChild);
		cloneChild->SetParent(shared_from_this());
	}

	/*auto trans = other.GetTransform();
	auto childCount = trans->GetChildCount();
	for(int i = 0; i < childCount; ++i)
	{
	auto childTrans = trans->GetChild(i);
	auto childObj = childTrans->GetGameObject();
	auto copyObj = std::make_shared<GameObject>(static_cast<const GameObject&>(*childObj));

	auto parentTrans = GetTransform();
	auto copyChildTrans = copyObj->GetTransform();

	copyChildTrans->SetParent(parentTrans);
	}*/
}

GameObject::~GameObject(void)
{
}

std::shared_ptr<Object> GameObject::Clone()
{
	auto cloneObj = std::make_shared<GameObject>();
	cloneObj->mName = this->mName;

	for (auto com : this->mComponents)
	{
		cloneObj->mComponents[com.first] = std::static_pointer_cast<Component, Object>(com.second->Clone());
		cloneObj->mComponents[com.first]->SetGameObject(cloneObj);
	}

	for (int i = 0; i < this->GetChildCount(); ++i)
	{
		auto cloneChild = std::static_pointer_cast<GameObject, Object>(this->GetChild(i)->Clone());
		cloneObj->SetChild(cloneChild);
		cloneChild->SetParent(cloneObj);
	}

	return cloneObj;
}

void GameObject::SetChild(std::shared_ptr<GameObject>& child)
{
	if (child != nullptr)
	{
		mChilds.push_back(child);
	}
}

std::shared_ptr<GameObject> GameObject::GetChild(unsigned int index) const
{
	return mChilds[index];
}

unsigned int GameObject::GetChildCount() const
{
	return (unsigned int)mChilds.size();
}

void GameObject::SetParent(std::shared_ptr<GameObject>& parent)
{
	if (parent != nullptr)
	{
		mParent = parent;

		/*auto transform = GetTransform();
		transform->SetParent(parent->GetTransform());*/
	}
}

std::shared_ptr<GameObject> GameObject::GetParent()
{
	return mParent.lock();
}

std::shared_ptr<GameObject> GameObject::Find(std::wstring name, bool recursive)
{
	std::shared_ptr<GameObject> result;

	for (auto child : mChilds)
	{
		if (child->GetName() == name)
		{
			result = child;
			break;
		}

		if (recursive)
		{
			FindRecursively(name, child, result);

			if (result != nullptr)
				break;
		}
	}

	return result;
}

shared_ptr<Transform> GameObject::GetTransform() const
{
	return GetComponent<Transform>();
}

void GameObject::FindRecursively(std::wstring name, std::shared_ptr<GameObject>& obj, std::shared_ptr<GameObject>& result)
{
	auto childSize = obj->GetChildCount();
	for (unsigned int i = 0; i < childSize; ++i)
	{
		auto child = obj->GetChild(i);

		if (child->GetName() == name)
		{
			result = child;
			break;
		}
		FindRecursively(name, child, result);
	}
}

