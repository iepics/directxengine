#include "stdafx.h"
#include "Component.h"

#include "GameObject.h"

void Component::SetGameObject(std::shared_ptr<GameObject>& gameObject)
{
	mGameObject = gameObject;
	mName = gameObject->GetName();
}