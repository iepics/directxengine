#include "stdafx.h"
#include "Object.h"


std::shared_ptr<Object> Object::Instantiate(std::shared_ptr<Object> origin)
{
	if (origin == nullptr)
	{
		return nullptr;
	}

	auto clone = origin->Clone();
	auto goClone = std::static_pointer_cast<GameObject, Object>(clone);

	auto currentScene = SceneManager::GetInstance()->GetCurrentScene();
	if (currentScene != nullptr)
	{
		currentScene->AddObject(goClone);
	}

	return clone;
}

Object::Object()
{
	auto time = std::chrono::steady_clock::now();
}

Object::~Object()
{
}
