#pragma once

class Behaviour : public Cloneable<Component, Behaviour>
{
public:
	virtual ~Behaviour(){}

	virtual void Start() {};
	virtual void Update() {};
};
