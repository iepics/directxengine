#include "stdafx.h"
#include "Scene.h"

#include "FBXLoader.h"
#include "Object.h"
#include "GameObject.h"
#include "Camera.h"
#include "Renderer.h"


Scene::Scene()
{
}

Scene::~Scene()
{
}

void Scene::Initialize()
{
	auto width = System::GetInstance()->GetClientWidth();
	auto height = System::GetInstance()->GetClientHeight();

	auto cameraObject = EditorUtility::CreateEmpty();
	auto uiCamera = cameraObject->InitComponent<Camera>();
	uiCamera->SetType(Camera::ProjectionType::Orthographic);
	uiCamera->SetWidth(width);
	uiCamera->SetHeight(height);
	uiCamera->SetDepth(100);

	mUICameraObj = cameraObject;
	mUICamera = uiCamera;

	uiCamera->UpdateMatrix();
}

void Scene::UpdateAll()
{
	for(auto& behaviour : mBehaviours)
	{
		auto behav = behaviour.lock();
		behav->Update();
	}
}

void Scene::RenderAll()
{
	ShaderBuffer* buffer = ShaderBuffer::GetInstance();
	
	for (auto& camera : mCameras)
	{
		auto cam = camera.lock();
		Camera::SetMainCamera(cam);
		cam->UpdateMatrix();
		cam->SetViewport();
		cam->SetCameraBuffer();

		Light::SortLight(mLights);
		auto dirLight = Light::GetDirectionalLight();
		if(dirLight)
		{
			dirLight->RenderShadow();
		}
		cam->SetViewport();

		Light::SetBaseBuffer();
		for (auto& renderer : mRenderers)
		{
			auto ren = renderer.lock();
			auto& transform = ren->GetGameObject()->GetTransform();
			buffer->SetWorld(transform->GetWorldMatrix());
			
			ren->Render();
		}

		for(auto& renderer : mUIRenderers)
		{
			auto uiCam = mUICamera.lock();
			uiCam->SetCameraBuffer();

			auto ren = renderer.lock();
			auto& transform = ren->GetGameObject()->GetTransform();
			buffer->SetWorld(transform->GetWorldMatrix());

			ren->Render();

#ifdef _DEBUG
			auto uiTrans = std::static_pointer_cast<UITransform, Transform>(transform);
			uiTrans->Render();
#endif
		}
	}
}

void Scene::AddModel(std::shared_ptr<ModelData>& model)
{
	auto clone = std::static_pointer_cast<GameObject, Object>(model->gameObject->Clone());
	AddObject(clone);
}

//void Scene::AddObject(const std::vector <std::shared_ptr<GameObject>>& objects)
//{
//	for(auto obj : objects)
//	{
//		mObjects.push_back(obj);
//		for(auto com : obj->mComponents)
//		{
//			if(dynamic_cast<Camera*>(com.second.get()))
//			{
//				auto& camera = std::static_pointer_cast<Camera, Component>(com.second);
//				mCameras.push_back(camera);
//			}
//			else if(dynamic_cast<Light*>(com.second.get()))
//			{
//				auto& light = std::static_pointer_cast<Light, Component>(com.second);
//				mLights.push_back(light);
//			}
//			else if(dynamic_cast<Renderer*>(com.second.get()))
//			{
//				auto& renderer = std::static_pointer_cast<Renderer, Component>(com.second);
//				mRenderers.push_back(renderer);
//			}
//			else if(dynamic_cast<UIRenderer*>(com.second.get()))
//			{
//				auto& renderer = std::static_pointer_cast<UIRenderer, Component>(com.second);
//				mUIRenderers.push_back(renderer);
//			}
//			else if(dynamic_cast<Behaviour*>(com.second.get()))
//			{
//				auto& behav = std::static_pointer_cast<Behaviour, Component>(com.second);
//				mBehaviours.push_back(behav);
//			}
//		}
//	}
//	
//}

void Scene::AddObject(const std::shared_ptr<GameObject>& object)
{
	if(object == nullptr)
		return;

	mObjects.push_back(object);
	for(auto com : object->mComponents)
	{
		if(dynamic_cast<Camera*>(com.second.get()))
		{
			auto& camera = std::static_pointer_cast<Camera, Component>(com.second);
			mCameras.push_back(camera);
		}
		else if(dynamic_cast<Light*>(com.second.get()))
		{
			auto& light = std::static_pointer_cast<Light, Component>(com.second);
			mLights.push_back(light);
		}
		else if(dynamic_cast<Renderer*>(com.second.get()))
		{
			auto& renderer = std::static_pointer_cast<Renderer, Component>(com.second);
			mRenderers.push_back(renderer);
		}
		else if(dynamic_cast<UIRenderer*>(com.second.get()))
		{
			auto& renderer = std::static_pointer_cast<UIRenderer, Component>(com.second);
			mUIRenderers.push_back(renderer);
		}
		else if(dynamic_cast<Behaviour*>(com.second.get()))
		{
			auto& behav = std::static_pointer_cast<Behaviour, Component>(com.second);
			mBehaviours.push_back(behav);
		}
		com.second->OnLoad();
	}

	std::shared_ptr<Transform> transform(object->GetComponent<Transform>());
	for(unsigned int i = 0; i < object->GetChildCount(); ++i)
	{
		//std::shared_ptr<GameObject> child(transform->GetChild(i)->GetGameObject());
		AddObject(object->GetChild(i));
	}
}

void Scene::AddComponent(const std::shared_ptr<Component>& com)
{
	if (com == nullptr)
		return;

	if (dynamic_cast<Camera*>(com.get()))
	{
		auto& camera = std::static_pointer_cast<Camera, Component>(com);
		mCameras.push_back(camera);
	}
	else if (dynamic_cast<Light*>(com.get()))
	{
		auto& light = std::static_pointer_cast<Light, Component>(com);
		mLights.push_back(light);
	}
	else if (dynamic_cast<Renderer*>(com.get()))
	{
		auto& renderer = std::static_pointer_cast<Renderer, Component>(com);
		mRenderers.push_back(renderer);
	}
	else if (dynamic_cast<UIRenderer*>(com.get()))
	{
		auto& renderer = std::static_pointer_cast<UIRenderer, Component>(com);
		mUIRenderers.push_back(renderer);
	}
	else if (dynamic_cast<Behaviour*>(com.get()))
	{
		auto& behav = std::static_pointer_cast<Behaviour, Component>(com);
		mBehaviours.push_back(behav);
	}
	com->OnLoad();
}


const std::shared_ptr<GameObject> Scene::Find(std::wstring name)
{
	for(auto& obj : mObjects)
	{
		if(obj->GetName() == name)
		{
			return obj;
		}
	}

	return nullptr;
}