#pragma once
#include "UIComponent.h"
#include "Mesh.h"
#include "Texture.h"

class ImageUI : public Cloneable<UIComponent, ImageUI>
{
public:
	ImageUI();
	~ImageUI();

	void SetImage(const std::shared_ptr<Texture> image);
	void SetColor(const XMFLOAT4 color);

private:
	void CreateMesh();

	std::weak_ptr<Texture> mImage;
	XMFLOAT4 mColor;
};

