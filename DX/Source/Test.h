#pragma once
#include "GameTimer.h"
#include "Input.h"
#include "Behaviour.h"
#include "MathUtilities.h"

class Test : public Cloneable<Behaviour, Test>
{
public:
	Test(){}
	~Test(){}

	void Update()
	{
		if (animation.expired())
		{
			animation = GetGameObject()->GetComponent<Animation>();
		}
		animation.lock()->Play(L"Take 001");

		if (renderer.expired())
		{
			renderer = GetGameObject()->GetComponentInChildren<Renderer>();
			material = renderer.lock()->GetMaterials().front();
			material.lock()->SetColor("_diffuse", XMFLOAT4(0.7, 0.3, 0.3, 1));
		}

		if (material.expired() == false)
		{
			if (matTrigger)
			{
				material.lock()->SetFloat("_metallic", 0.1);
				material.lock()->SetFloat("_roughness", 0.1);
				std::cout << "non metal" << std::endl;
			}
			else
			{
				material.lock()->SetFloat("_metallic", 0.5);
				material.lock()->SetFloat("_roughness", 0.1);
				std::cout << "metal" << std::endl;
			}
		}

		if (Input::GetInstance()->GetKeyDown(KeyCode::M))
		{
			matTrigger = !matTrigger;
			std::cout << "M" << std::endl;
		}

		//GetGameObject()->GetTransform()->Rotate(XMFLOAT3(0, 60, 0) * GameTimer::GetInstance()->GetDeltaTime());
		//GetGameObject()->GetTransform()->Translate(XMFLOAT3(30, 0, 0) * GameTimer::GetInstance()->GetDeltaTime());
		//GetGameObject()->GetTransform()->SetLocalPosition(XMFLOAT3(-100, 0, 0));
	}

private:
	std::weak_ptr<Animation> animation;

	std::weak_ptr<Renderer> renderer;
	std::weak_ptr<Material> material;

	bool matTrigger;
};

