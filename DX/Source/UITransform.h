#pragma once
#include "Component.h"

class UITransform : public Cloneable<Transform, UITransform>
{
public:
	UITransform();
	~UITransform();

	void SetWidth(float width) { mWidth = width; }
	float GetWidth() { return mWidth; }

	void SetHeight(float height) { mHeight = height; }
	float GetHeight() { return mHeight; }

	void SetPivot(const XMFLOAT2 pivot) { mPivot = pivot; }	

	virtual XMMATRIX GetWorldMatrix();

	///Debug
	void Render();

private:
	virtual void SetLocal();
	virtual void SetWorld();

	float mWidth = 100;
	float mHeight = 30;

	XMFLOAT2 mPivot;	//����:0, �ϴ�:0
};

