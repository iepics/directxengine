#pragma once
#include "ResourceManager.h"
#include "SceneManager.h"
#include "Input.h"
#include "Graphics.h"
#include "GameTimer.h"

class System : public Singleton<System>
{
	friend class Singleton <System> ;
public:
	bool Initialize(int width, int height, bool bFull, bool bVsync);
	void Destroy();
	void Run();

	LRESULT CALLBACK MessageHandler(HWND hWnd, UINT nMessageID,
		WPARAM wParam, LPARAM lParam);

	UINT GetClientWidth(){ return mClientWidth;}
	UINT GetClientHeight(){ return mClientHeight;}
	bool IsFullscreen(){ return mFullscreen; }
	bool IsVsync(){ return mVsync;}

	const HWND& GetHwnd(){ return mHWnd;}

protected:
	System(void);

private:
	~System(void);
	bool Frame();
	void InitializeWindows();
	void ShutdownWindows();
	
	HINSTANCE mHInstance;
	HWND mHWnd;

	std::wstring mAppName;
	int mClientWidth;
	int mClientHeight;
	bool mFullscreen;
	bool mVsync;

	std::shared_ptr<Graphics> mGraphics;
};

static LRESULT CALLBACK WndProc(HWND hWnd, UINT nMessageID,
		WPARAM wParam, LPARAM lParam);