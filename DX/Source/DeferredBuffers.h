#pragma once
using namespace DirectX;

class DeferredBuffers
{
public:
	static const int BUFFER_COUNT = 2;

	DeferredBuffers(void);
	~DeferredBuffers(void);

	bool Initialize(int, int);
	void Destroy();

	void SetRenderTargets();
	void ClearRenderTargets(XMFLOAT4 color);

	ID3D11ShaderResourceView* GetShaderResourceView(int);

private:
	int m_textureWidth, m_textureHeight;

	ID3D11Texture2D* m_renderTargetTextureArray[BUFFER_COUNT];
	ID3D11RenderTargetView* m_renderTargetViewArray[BUFFER_COUNT];
	ID3D11ShaderResourceView* m_shaderResourceViewArray[BUFFER_COUNT];
	ID3D11Texture2D* m_depthStencilBuffer;
	ID3D11DepthStencilView* m_depthStencilView;
	D3D11_VIEWPORT m_viewport;
};

