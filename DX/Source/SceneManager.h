#pragma once
#include "Singleton.h"
#include "Scene.h"

class SceneManager : public Singleton<SceneManager>
{
	friend class Singleton <SceneManager>;
public:
	void LoadScene(std::wstring name);

	const std::shared_ptr<Scene> GetCurrentScene() const;

protected:
	SceneManager(){}
private:
	~SceneManager(){}

	std::shared_ptr<Scene> mCurrentScene;
	std::unordered_map<std::wstring, std::shared_ptr<Scene>> mSceneMap;
};

