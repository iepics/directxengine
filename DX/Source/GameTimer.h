#pragma once
#include <string>

const ULONG MAX_SAMPLE_COUNT = 50;
class GameTimer : public Singleton<GameTimer>
{
	friend class Singleton < GameTimer >;
public:
	void Initialize();

	void Tick(float fLockFPS = 0.0f);	//call per frame
	unsigned long GetFrameRate(LPTSTR lpszString = NULL, int nCharacters = 0);
	unsigned long GetFrameRate(std::wstring &strOut);
	float GetDeltaTime();
	float GetUnscaledDeltaTime();

protected:
	GameTimer(void){}

private:
	~GameTimer(void){}

	float mTimeScale;
	float mDeltaTime;
	std::chrono::steady_clock::time_point mCurrentTime;
	std::chrono::steady_clock::time_point mLastTime;
	
	float mFrameTime[MAX_SAMPLE_COUNT];
	unsigned long mSampleCount;

	unsigned long mCurrentFrameRate;
	unsigned long mFPS;
	float mFPSTimeElapsed;
};

class GlobalTimer
{

};