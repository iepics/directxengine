#include "stdafx.h"
#include "ResourceManager.h"

std::shared_ptr<ModelData> ResourceManager::GetModel(std::wstring name)
{
	auto retValue = mModelMap.find(name);
	if (retValue != mModelMap.cend())
	{
		return retValue->second;
	}
	else
	{
		return nullptr;
	}
}

std::shared_ptr<Material> ResourceManager::GetMaterial(std::wstring name)
{
	auto retValue = mMaterialMap.find(name);
	if (retValue != mMaterialMap.cend())
	{
		return retValue->second;
	}
	else
	{
		return nullptr;
	}
}

std::shared_ptr<Effect> ResourceManager::GetEffect(std::wstring name)
{
	auto retValue = mEffectMap.find(name);
	if (retValue != mEffectMap.cend())
	{
		return retValue->second;
	}
	else
	{
		mEffectMap[name] = std::make_shared<Effect>();
		mEffectMap[name]->FromFile(name);

		return mEffectMap[name];
	}
}

std::shared_ptr<Font> ResourceManager::GetFont(std::wstring name)
{
	auto retValue = mFontMap.find(name);
	if(retValue != mFontMap.cend())
	{
		return retValue->second;
	}
	else
	{
		auto& font = std::make_shared<Font>();
		if(!font->Initialize(L"Resource/Font/" + name))
		{
			font->Destroy();

			return nullptr;
		}
		mFontMap[name] = font;

		return mFontMap[name];
	}
}

std::shared_ptr<Texture> ResourceManager::GetTexture(std::wstring name)
{
	auto retValue = mTextureMap.find(name);
	if(retValue != mTextureMap.cend())
	{
		return retValue->second;
	}
	else
	{
		return nullptr;
	}
}

void ResourceManager::AddModel(std::wstring name, std::shared_ptr<ModelData>& model)
{
	if (mModelMap[name] == nullptr && model != nullptr)
	{
		mModelMap[name] = model;
	}
	else
	{
		//吝汗 贸府 
	}
}

void ResourceManager::AddMaterial(std::wstring name, std::shared_ptr<Material>& material)
{
	if (mMaterialMap[name] == nullptr && material != nullptr)
	{
		mMaterialMap[name] = material;
	}
	else
	{
		//吝汗 贸府 
	}
}

void ResourceManager::AddEffect(std::wstring name, std::shared_ptr<Effect>& effect)
{
	if (mEffectMap[name] == nullptr && effect != nullptr)
	{
		mEffectMap[name] = effect;
	}
	else
	{
		//吝汗 贸府 
	}
}

void ResourceManager::AddFont(std::wstring name, std::shared_ptr<Font>& font)
{
	if(mFontMap[name] == nullptr && font != nullptr)
	{
		mFontMap[name] = font;
	}
	else
	{
		//吝汗 贸府 
	}
}

void ResourceManager::AddTexture(std::wstring name, std::shared_ptr<Texture>& font)
{
	if(mTextureMap[name] == nullptr && font != nullptr)
	{
		mTextureMap[name] = font;
	}
	else
	{
		//吝汗 贸府 
	}
}