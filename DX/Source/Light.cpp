#include "stdafx.h"
#include "Light.h"

#include "ShaderBuffer.h"
#include "Component.h"

using std::shared_ptr;

std::list<std::weak_ptr<Light>> Light::allLights;
std::weak_ptr<Light> Light::directionalLight;
std::list<std::weak_ptr<Light>> Light::pixelLights;
std::list<std::weak_ptr<Light>> Light::vertexLights;
Light::Light(void)
{
}

Light::~Light(void)
{
}

void Light::SetType(LightType type)
{
	mType = type;
}
void Light::SetColor(XMFLOAT4 color)
{
	mColor = color;
}
void Light::SetIntensity(float intensity)
{
	mIntensity = intensity;
}
void Light::SetRange(float range)
{
	mRange = range;
}
void Light::SetRenderMode(RenderMode mode)
{
	mRenderMode = mode;
}
void Light::SetShadowType(ShadowType type)
{
	mShadowType = type;
	if(type != ShadowType::No)
	{
		System* system = System::GetInstance();
		unsigned int width = (unsigned int)2048;
		unsigned int height = (unsigned int)2048;
		mShadowTexture = std::make_shared<RenderTexture>();
		mShadowTexture->SetWidth(width);
		mShadowTexture->SetHeight(height);
		mShadowTexture->SetFormat(Texture::Format::Shadow);
		mShadowTexture->Create();
	}
}

LightType Light::GetType()
{
	return mType;
}
XMFLOAT4 Light::GetColor()
{
	return mColor;
}
float Light::GetIntensity()
{
	return mIntensity;
}
float Light::GetRange()
{
	return mRange;
}
RenderMode Light::GetRenderMode()
{
	return mRenderMode;
}
std::shared_ptr<RenderTexture> Light::GetShadowMap()
{
	if(mShadowType == ShadowType::No)
		return nullptr;

	return mShadowTexture;
}

void Light::RenderShadow()
{
	auto buffer = ShaderBuffer::GetInstance();

	if(mShadowType == ShadowType::No)
	{
		return;
	}

	auto forward = GetGameObject()->GetTransform()->GetForwardVector();
	auto right = GetGameObject()->GetTransform()->GetRightVector();
	auto up = GetGameObject()->GetTransform()->GetUpVector();

	auto mainCamera = Camera::GetMainCamera();
	auto worldMat = mainCamera->GetGameObject()->GetTransform()->GetWorldMatrix();
	auto view = mainCamera->GetViewMatrix();
	auto viewInv = XMMatrixInverse(0, view);
	auto proj = mainCamera->GetProjection();
	auto frustum = BoundingFrustum(proj);
	auto count = frustum.CORNER_COUNT;
	XMFLOAT3* corners = new XMFLOAT3[count];
	frustum.GetCorners(corners);

	for(int i = 0; i < count; ++i)
	{
		auto vec = XMLoadFloat3(&corners[i]);
		vec = XMVector3Transform(vec, worldMat);
		XMStoreFloat3(&corners[i], vec);
	}

	float depth, width, height;
	std::function<float(XMFLOAT3)> lengthCalc;
	lengthCalc = [&](XMFLOAT3 basis)->float
	{
		std::list<float> length;
		for(int i = 0; i < count; ++i)
		{
			auto axis = XMLoadFloat3(&basis);
			auto corner = XMLoadFloat3(&corners[i]);

			auto dotVec = XMVector3Dot(axis, corner);
			auto dot = XMVectorGetX(dotVec);
			axis = axis * dot;
			axis = XMVector3Length(axis);
			
			if(dot < 0)
				dot = -1;
			else
				dot = 1;
			length.push_back(dot * XMVectorGetX(axis));
		}
		length.sort();

		return length.back() - length.front();
	};
	depth = lengthCalc(forward);
	width = lengthCalc(right);
	height = lengthCalc(up);

	//depth = 2000;
	//width = 2000;
	//height = 2000;
		
	if(mShadowType == ShadowType::Hard)
	{
		mShadowTexture->ClearRenderTarget(XMFLOAT4(1, 1, 1, 1));
		mShadowTexture->ApplyRenderTarget();

		D3D11_VIEWPORT viewport;

		viewport.TopLeftX = 0;
		viewport.TopLeftY = 0;
		viewport.Width = (float)mShadowTexture->GetWidth();
		viewport.Height = (float)mShadowTexture->GetHeight();
		viewport.MinDepth = 0.0f;
		viewport.MaxDepth = 1.0f;

		auto deviceContext = D3D::GetInstance()->GetDeviceContext();

		deviceContext->RSSetViewports(1, &viewport);

		System* system = System::GetInstance();
		shared_ptr<Transform> transform = GetGameObject()->GetTransform();

		XMVECTOR upVec, forwardVec, position, lookat;
		upVec = XMLoadFloat3(&up);
		forwardVec = XMLoadFloat3(&forward);
		position = -forwardVec * depth;
		lookat = position + forwardVec;

		XMMATRIX viewMat = XMMatrixLookAtLH(position, lookat, upVec);
		XMMATRIX projectionMat = XMMatrixOrthographicLH(width, height, 0, depth * 2);

		auto cameraView = buffer->GetView();
		auto cameraProj = buffer->GetProjection();

		XMStoreFloat4x4(&mLightView, viewMat);
		XMStoreFloat4x4(&mLightProj, projectionMat);

		buffer->SetView(viewMat);
		buffer->SetProjection(projectionMat);

		auto fx = ResourceManager::GetInstance()->GetEffect(L"Resource/FX/Depthmap.fx");

		auto renderers = SceneManager::GetInstance()->GetCurrentScene()->mRenderers;
		for(auto& renderer : renderers)
		{
			auto ren = renderer.lock();
			auto& transform = ren->GetGameObject()->GetTransform();
			buffer->SetWorld(transform->GetWorldMatrix());

			ren->Render(fx);
		}

		buffer->SetView(cameraView);
		buffer->SetProjection(cameraProj);
	}
}

void Light::SortLight()
{
	if (allLights.size() == 0)
	{
		return;
	}
	pixelLights.clear();
	vertexLights.clear();

	auto buffer = ShaderBuffer::GetInstance();

	const auto& worldMat = buffer->GetWorld();
	XMFLOAT4X4 world;
	XMStoreFloat4x4(&world, worldMat);

	XMFLOAT3 worldPos;
	worldPos.x = world._41;
	worldPos.y = world._42;
	worldPos.z = world._43;

	XMFLOAT3 cameraPos = Camera::GetMainCamera()->GetGameObject()->GetTransform()->GetLocalPosition();

	std::list<std::weak_ptr<Light>> directionals;
	std::list<std::weak_ptr<Light>> others;
	std::list<std::weak_ptr<Light>> autoLights;

	std::function<bool(std::weak_ptr<Light> const, std::weak_ptr<Light> const)> intensitySort;
	intensitySort = [&](std::weak_ptr<Light> const a, std::weak_ptr<Light> const b)->bool
	{
		float aLength = 1.0f;
		float bLength = 1.0f;

		auto aPtr = a.lock();
		auto bPtr = b.lock();
		if(aPtr->GetType() != LightType::Direction)
		{
			XMFLOAT3 aL = aPtr->GetGameObject()->GetTransform()->GetLocalPosition() - cameraPos;
			aLength = XMVectorGetX(XMVector3Length(XMLoadFloat3(&aL)));
		}
		if(bPtr->GetType() != LightType::Direction)
		{
			XMFLOAT3 bL = bPtr->GetGameObject()->GetTransform()->GetLocalPosition() - cameraPos;
			bLength = XMVectorGetX(XMVector3Length(XMLoadFloat3(&bL)));
		}

		float aValue = aPtr->GetIntensity() / (aLength*aLength);
		float bValue = bPtr->GetIntensity() / (bLength*bLength);

		return aValue > bValue;
	};
	allLights.sort(intensitySort);	

	for(auto& light : allLights)
	{
		auto lit = light.lock();
		if(lit->GetRenderMode() == RenderMode::Auto)
		{
			if(lit->GetType() == LightType::Direction && directionalLight.expired())
			{
				directionalLight = light;
			}
			else
			{
				autoLights.push_back(lit);
			}
		}
		else if(lit->GetRenderMode() == RenderMode::Pixel)
		{
			pixelLights.push_back(light);
		}
		else if(lit->GetRenderMode() == RenderMode::Vertex)
		{
			vertexLights.push_back(light);
		}
	}

	int maxCount = 4;	//setting variable
	auto iter = autoLights.begin();
	for(int i = maxCount - (int)pixelLights.size(); i>0; --i)
	{
		pixelLights.push_back(*iter);
		autoLights.erase(iter++);
	}

	iter = autoLights.begin();
	for(auto& light : autoLights)
	{
		vertexLights.push_back(light);
	}
}

void Light::SortLight(std::list<std::weak_ptr<Light>>& lights)
{
	if(lights.size() == 0)
	{
		return;
	}
	pixelLights.clear();
	vertexLights.clear();

	auto buffer = ShaderBuffer::GetInstance();

	const auto& worldMat = buffer->GetWorld();
	XMFLOAT4X4 world;
	XMStoreFloat4x4(&world, worldMat);

	XMFLOAT3 worldPos;
	worldPos.x = world._41;
	worldPos.y = world._42;
	worldPos.z = world._43;

	XMFLOAT3 cameraPos = Camera::GetMainCamera()->GetGameObject()->GetTransform()->GetLocalPosition();

	std::list<std::weak_ptr<Light>> directionals;
	std::list<std::weak_ptr<Light>> others;
	std::list<std::weak_ptr<Light>> autoLights;

	std::function<bool(std::weak_ptr<Light> const, std::weak_ptr<Light> const)> intensitySort;
	intensitySort = [&](std::weak_ptr<Light> const a, std::weak_ptr<Light> const b)->bool
	{
		float aLength = 1.0f;
		float bLength = 1.0f;

		auto aPtr = a.lock();
		auto bPtr = b.lock();
		if(aPtr->GetType() != LightType::Direction)
		{
			XMFLOAT3 aL = aPtr->GetGameObject()->GetTransform()->GetLocalPosition() - cameraPos;
			aLength = XMVectorGetX(XMVector3Length(XMLoadFloat3(&aL)));
		}
		if(bPtr->GetType() != LightType::Direction)
		{
			XMFLOAT3 bL = bPtr->GetGameObject()->GetTransform()->GetLocalPosition() - cameraPos;
			bLength = XMVectorGetX(XMVector3Length(XMLoadFloat3(&bL)));
		}

		float aValue = aPtr->GetIntensity() / (aLength*aLength);
		float bValue = bPtr->GetIntensity() / (bLength*bLength);

		return aValue > bValue;
	};
	lights.sort(intensitySort);

	for(auto& light : lights)
	{
		auto lit = light.lock();
		if(lit->GetRenderMode() == RenderMode::Auto)
		{
			if(lit->GetType() == LightType::Direction && directionalLight.expired())
			{
				directionalLight = light;
			}
			else
			{
				autoLights.push_back(lit);
			}
		}
		else if(lit->GetRenderMode() == RenderMode::Pixel)
		{
			pixelLights.push_back(light);
		}
		else if(lit->GetRenderMode() == RenderMode::Vertex)
		{
			vertexLights.push_back(light);
		}
	}

	int maxCount = 4;	//setting variable
	int lightCount = autoLights.size();
	int count = min(maxCount, lightCount);

	auto iter = autoLights.begin();
	for(int i = count - (int)pixelLights.size(); i>0; --i)
	{
		pixelLights.push_back(*iter);
		autoLights.erase(iter++);
	}

	iter = autoLights.begin();
	for(auto& light : autoLights)
	{
		vertexLights.push_back(light);
	}
}

void Light::OnAttached(std::shared_ptr<Component> com)
{
	auto light = std::static_pointer_cast<Light, Component>(com);
	Light::allLights.push_back(light);
}

void Light::OnDetached(std::shared_ptr<Component> com)
{
	auto light = std::static_pointer_cast<Light, Component>(com);
	Light::allLights.remove(light);
}