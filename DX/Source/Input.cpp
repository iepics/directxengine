#include "stdafx.h"
#include "Input.h"


#pragma region Input
Input::Input()
{
#ifdef _WIN64
	mKeyMap[KeyCode::Mouse0] = VK_LBUTTON;
	mKeyMap[KeyCode::Mouse1] = VK_RBUTTON;
	mKeyMap[KeyCode::Mouse2] = VK_MBUTTON;
	mKeyMap[KeyCode::Mouse3] = VK_XBUTTON1;
	mKeyMap[KeyCode::Mouse4] = VK_XBUTTON2;
	mKeyMap[KeyCode::Backspace] = VK_BACK;
	mKeyMap[KeyCode::Space] = VK_SPACE;
	mKeyMap[KeyCode::Tab] = VK_TAB;
	mKeyMap[KeyCode::LeftArrow] = VK_LEFT;
	mKeyMap[KeyCode::RightArrow] = VK_RIGHT;
	mKeyMap[KeyCode::UpArrow] = VK_UP;
	mKeyMap[KeyCode::DownArrow] = VK_DOWN;
	mKeyMap[KeyCode::Esc] = VK_ESCAPE;
	mKeyMap[KeyCode::NumLock] = VK_NUMLOCK;
	mKeyMap[KeyCode::CapsLock] = VK_CAPITAL;
	mKeyMap[KeyCode::ScrollLock] = VK_SCROLL;
	mKeyMap[KeyCode::PageUp] = VK_PRIOR;
	mKeyMap[KeyCode::PageDown] = VK_NEXT;
	mKeyMap[KeyCode::A] = 'A';
	mKeyMap[KeyCode::B] = 'B';
	mKeyMap[KeyCode::D] = 'D';
	mKeyMap[KeyCode::M] = 'M';
	mKeyMap[KeyCode::R] = 'R';
	mKeyMap[KeyCode::S] = 'S';
	mKeyMap[KeyCode::W] = 'W';
	mKeyMap[KeyCode::Alpha0] = '0';
	mKeyMap[KeyCode::Alpha1] = '1';
	mKeyMap[KeyCode::Alpha2] = '2';
	mKeyMap[KeyCode::Alpha3] = '3';
	mKeyMap[KeyCode::Alpha4] = '4';
	mKeyMap[KeyCode::Alpha5] = '5';
	mKeyMap[KeyCode::Alpha6] = '6';
	mKeyMap[KeyCode::Alpha7] = '7';
	mKeyMap[KeyCode::Alpha8] = '8';
	mKeyMap[KeyCode::Alpha9] = '9';
#endif
}
Input::~Input()
{

}

bool Input::GetKeyDown(KeyCode keyCode)
{
	UpdateKeyState(keyCode);

	if (mKeyStates[keyCode] == KeyState::Down)
	{
		return true;
	}

	return false;
}

bool Input::GetKeyUp(KeyCode keyCode)
{
	UpdateKeyState(keyCode);

	if (mKeyStates[keyCode] == KeyState::Up)
	{
		return true;
	}

	return false;
}

void Input::UpdateKeyState(KeyCode keyCode)
{	
	int key = mKeyMap[keyCode];
	KeyState state;

	if (keyCode == KeyCode::CapsLock || keyCode == KeyCode::NumLock ||
		keyCode == KeyCode::ScrollLock)
	{
#ifdef _WIN64
		if (GetKeyState(key))
		{
			state = KeyState::Toggled;
		}
		else
		{
			state = KeyState::UnToggled;
		}
#endif
	}
	else
	{
#ifdef _WIN64
		auto result = GetAsyncKeyState(key); 
		if (result & 0x8000)
		{
			state = KeyState::Down;
		}
		else if (!(result & 0x8000))
		{
			state = KeyState::Up;
		}
		else if (result & 0x0001)
		{
			state = KeyState::Pressed;
		}
#endif

	}

	mKeyStates[keyCode] = state;	
}
#pragma endregion

