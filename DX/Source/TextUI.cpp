#include "stdafx.h"
#include "TextUI.h"


TextUI::TextUI()
{
}

TextUI::~TextUI()
{
}


void TextUI::SetFont(std::shared_ptr<Font> font)
{
	mFont = font;
}

void TextUI::SetSize(float size)
{
	if (mFont == nullptr)
		return;

	mFont->SetHeight(size);
	CreateMesh();
}

void TextUI::SetText(std::wstring str)
{
	mText = str;

	CreateMesh();
}
std::wstring TextUI::GetText()
{
	return mText;
}

void TextUI::SetColor(const XMFLOAT4 color) 
{ 
	mColor = color; 

	auto& renderer = GetGameObject()->GetComponent<UIRenderer>();
	if (renderer)
	{
		renderer->SetColor(mColor);
	}
}

void TextUI::CreateMesh()
{
	if (mText.length() == 0)
		return;

	if(mFont != nullptr)
	{
		auto uiTrans = GetGameObject()->GetComponent<UITransform>();

		FT_Face face = mFont->GetFace();
		//폰트의 높이
		float height = face->size->metrics.height >> 6;
		//기준선으로부터의 최대 높이
		float ascender = face->size->metrics.ascender >> 6;
		//기준선으로부터의 최저 높이
		float descender = face->size->metrics.descender >> 6;

		//	XMFLOAT3 position = mGameObject->GetTransform()->GetPosition();
		float rectRight = uiTrans->GetWidth() / 2;
		float rectLeft = -uiTrans->GetWidth() / 2;
		float rectTop = uiTrans->GetHeight() / 2;
		float rectBottom = -uiTrans->GetHeight() / 2;
		float startX = rectLeft;
		float startY = rectTop - ascender;

		GlyphBank* gbInstance = GlyphBank::GetInstance();

		// 출력할 글자들을 texture에 채워넣는다
		int size = mFont->GetHeight() * mFont->GetScale();
		std::wstring m2 = gbInstance->PrepareGlyph(face, mText, (int)mText.size(), size);

		std::vector<int> width;
		width.push_back(0);

		for(auto it : mText)
		{
			if(it == '\n')
			{
				width.push_back(0);
				continue;
			}

			GlyphBank::GlyphData gd = gbInstance->GetGlyph(face, it, size);

			////자동 줄바꿈
			//if(width.back() + gd.bmW + gd.left >= rectRight - rectLeft)
			//{
			//	width.push_back(0);
			//}
			width.back() += gd.advX;
		}

		//가로 가운데 정렬
		//startX = (rectRight + rectLeft - width[0]) / 2;

		//세로 정렬
		//startY = (rectTop  + width.size() * height) / 2 - ascender;
		//startY = (rectTop + rectBottom - ascender / 2) + (width.size() * height / 4);
		//startY = rectTop + ascender;	//가운데

		//메시, 버텍스, 인덱스 버퍼 생성
		std::shared_ptr<Mesh> mesh = std::make_shared<Mesh>();
		std::vector<ModelVertex> vertices;
		std::vector<SubMesh> subMeshes;
		subMeshes.resize(1);

		int index = 0;
		int line = 0;
		for(auto it : mText)
		{
			if(it == '\n')
			{
				++line;

				///정렬
				//startX = (rectRight + rectLeft - width[line]) / 2;
				startX = rectLeft;

				startY -= size;
				continue;
			}

			GlyphBank::GlyphData gd = gbInstance->GetGlyph(face, it, size);

			////자동 줄바꿈
			//if(startX + gd.bmW + gd.left >= rectRight)
			//{
			//	++line;

			//	///정렬
			//	startX = (rectRight + rectLeft - width[line]) / 2;
			//	//startX = rectLeft;

			//	startY -= size;
			//}

			/*
			startX + gd.left : destX
			startY + gd.top  : destY
			gd.bmW      : destW
			gd.bmH      : destH
			gd.u        : srcU
			gd.v        : srcV
			gd.w        : srcW
			gd.h        : srcH
			*/
			float destX = startX + gd.left;
			float destY = startY + gd.top;
			ModelVertex vertex;
			vertex.position.x = destX + gd.bmW;
			vertex.position.y = destY;
			vertex.position.z = 0;
			vertex.uv.x = gd.u + gd.w;
			vertex.uv.y = gd.v;
			vertices.push_back(vertex);

			vertex.position.x = destX;
			vertex.position.y = destY;
			vertex.position.z = 0;
			vertex.uv.x = gd.u;
			vertex.uv.y = gd.v;
			vertices.push_back(vertex);

			vertex.position.x = destX + gd.bmW;
			vertex.position.y = destY - gd.bmH;
			vertex.position.z = 0;
			vertex.uv.x = gd.u + gd.w;
			vertex.uv.y = gd.v + gd.h;
			vertices.push_back(vertex);

			vertex.position.x = destX;
			vertex.position.y = destY - gd.bmH;
			vertex.position.z = 0;
			vertex.uv.x = gd.u;
			vertex.uv.y = gd.v + gd.h;
			vertices.push_back(vertex);

			subMeshes[0].indices.push_back(0 + index);
			subMeshes[0].indices.push_back(2 + index);
			subMeshes[0].indices.push_back(1 + index);
			subMeshes[0].indices.push_back(2 + index);
			subMeshes[0].indices.push_back(3 + index);
			subMeshes[0].indices.push_back(1 + index);

			index += 4;

			startX += gd.advX;
			startY += gd.advY;
		}
		//ModelVertex vertex;
		//float aa = 20;
		//vertex.position = XMFLOAT3(aa, aa, 0);
		//vertex.uv = XMFLOAT2(1.0f, 0.0f);
		//vertices.push_back(vertex);

		//vertex.position = XMFLOAT3(-aa, aa, 0);
		//vertex.uv = XMFLOAT2(0.0f, 0.0f);
		//vertices.push_back(vertex);

		//vertex.position = XMFLOAT3(aa, -aa, 0);
		//vertex.uv = XMFLOAT2(1.0f, 1.0f);
		//vertices.push_back(vertex);

		//vertex.position = XMFLOAT3(-aa, -aa, 0);
		//vertex.uv = XMFLOAT2(0.0f, 1.0f);
		//vertices.push_back(vertex);

		//subMeshes[0].indices.push_back(0 );
		//subMeshes[0].indices.push_back(1 );
		//subMeshes[0].indices.push_back(2 );
		//subMeshes[0].indices.push_back(2 );
		//subMeshes[0].indices.push_back(1 );
		//subMeshes[0].indices.push_back(3 );

		mesh->SetVertices(vertices);
		mesh->SetSubMeshes(subMeshes);
		mesh->CreateVertexBuffer();
		mesh->CreateIndexBuffer();

		auto& renderer = GetGameObject()->GetComponent<UIRenderer>();
		if (renderer)
		{
			renderer->SetMesh(mesh);
			renderer->SetTexture(gbInstance->GetFTTextureResource());
			renderer->SetColor(mColor);
		}
	}
}