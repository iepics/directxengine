#pragma once
#include <ft2build.h>
#include FT_FREETYPE_H
#include "GlyphBank.h"

class Font
{
public:
	Font();
	~Font();

	bool Initialize(std::wstring fontPath);
	void Destroy();

	void SetHeight(int height);
	int GetHeight(){ return mHeight; }

	void SetScale(float scale){ mScale = scale; }
	float GetScale(){ return mScale; }

	FT_Face GetFace(){ return mFace; }

protected:
	std::wstring mFontPath;	//폰트 경로
	int mHeight;	//폰트 크기
	float mScale;
	FT_Face mFace;
	
};

