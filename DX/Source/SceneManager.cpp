#include "stdafx.h"
#include "SceneManager.h"

void SceneManager::LoadScene(std::wstring name)
{
	auto iter = mSceneMap.find(name);
	if(iter == mSceneMap.cend())
	{
		mSceneMap[name] = std::make_shared<Scene>();
		mSceneMap[name]->SetName(name);
		mSceneMap[name]->Initialize();
	}
	mCurrentScene = mSceneMap[name];
}

const std::shared_ptr<Scene> SceneManager::GetCurrentScene() const
{
	if(mCurrentScene == nullptr)
	{
		return nullptr;
	}

	return mCurrentScene;
}
