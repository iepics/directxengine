#pragma once
#include "Object.h"
#include "Texture.h"
#include <vector>
#include <unordered_map>
using namespace DirectX;

struct ModelVertex
{
	XMFLOAT3 position;
	XMFLOAT4 color;
	XMFLOAT3 normal;
	XMFLOAT2 uv;
	XMFLOAT3 weights;
	BYTE boneIndices[4];

	ModelVertex()
	{
		position = XMFLOAT3(0, 0, 0);
		color = XMFLOAT4(1, 1, 1, 1);
		normal = XMFLOAT3(0, 0, 0);
	}
};

struct UIVertex
{
	XMFLOAT3 position;
	XMFLOAT4 color;
	XMFLOAT3 normal;
	XMFLOAT2 uv;
};

struct DebugVertex
{
	XMFLOAT3 Position;
	XMFLOAT4 Color;

	DebugVertex()
	{
		Position = XMFLOAT3(0, 0, 0);
		Color = XMFLOAT4(0, 0, 0, 1);
	}
};

struct Triangle
{	
	std::vector<UINT> mIndices;
};

struct SubMesh
{
	//UINT triangleStart;
	//UINT triangleCount;

	std::vector<UINT> indices;
};

class Mesh : public Object
{
public:
	Mesh();
	virtual ~Mesh(void);

	virtual void Initialize();
	virtual void Destroy();

	void AddVertex(ModelVertex vertex){ mVertices.push_back(vertex); }
	void SetVertices(std::vector<ModelVertex> vertices){ mVertices.swap(vertices); }

	void AddSubMesh(SubMesh subMesh){ mSubMeshes.push_back(subMesh); }
	void SetSubMeshes(std::vector<SubMesh> subMeshes){ mSubMeshes.swap(subMeshes); }
	SubMesh GetSubMesh(int index){ return mSubMeshes[index]; }
	unsigned int GetSubMeshCount(){ return (unsigned int)mSubMeshes.size(); }

	void SetBindPoses(std::vector<XMFLOAT4X4> bindPoses){ mBindPoses.swap(bindPoses); }
	XMFLOAT4X4 GetBindPose(int index){ return mBindPoses[index]; }
	unsigned int GetBindPoseCount(){ return (unsigned int)mBindPoses.size(); }

	void SetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitive);
	D3D11_PRIMITIVE_TOPOLOGY GetPrimitiveTopology();

	bool CreateVertexBuffer();
	bool CreateIndexBuffer();
	ID3D11Buffer* GetVertexBuffer(){ return mVertexBuffer; }
	ID3D11Buffer* GetIndexBuffer(int index){ return mIndexBuffers[index]; }
	std::vector<ID3D11Buffer*> GetIndexBuffers(){ return mIndexBuffers; }

protected:
	std::vector<ModelVertex> mVertices;
	std::vector<SubMesh> mSubMeshes;	//무조건 하나는 존재한다.
	std::vector<XMFLOAT4X4> mBindPoses;

	D3D11_PRIMITIVE_TOPOLOGY m_d3dPrimitiveTopology;

	ID3D11Buffer* mVertexBuffer;
	std::vector<ID3D11Buffer*> mIndexBuffers;
};



class InputLayouts
{
public:
	static bool InitAll();
	static void DestroyAll();

	static ID3D11InputLayout* UIInputLayout;
	static ID3D11InputLayout* DebugInputLayout;
	static ID3D11InputLayout* ModelInputLayout;

private:
	std::unordered_map<std::string, ID3D11InputLayout*> mInputLayout;
};
