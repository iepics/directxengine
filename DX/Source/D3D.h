#pragma once
#include "Effects.h"
#include "Mesh.h"
#include "RenderStates.h"
using namespace DirectX;

class D3D
{
public:
	D3D(void);
	~D3D(void);

	//������ �ڵ��� ���ڷ� �޾� D3D�� �ʱ�ȭ
	bool Initialize(HWND hWnd);
	void Destroy();

	//����Ÿ���� D3D�� �⺻ ����Ÿ�ٺ�� ����
	bool ApplyRenderTarget();

	void BeginScene(const XMFLOAT4& color);
	void EndScene();

	CComPtr<ID3D11Device> GetDevice();
	CComPtr<ID3D11DeviceContext> GetDeviceContext();

	void TurnZBufferOn();
	void TurnZBufferOff();
	void GetVideoCardInfo(std::wstring &cardName, int &memory);

	static D3D* GetInstance(){ return instance; }

private:
	static D3D* instance;
	bool CreateView();
	bool CreateRasterState();

	int m_videoMemory;
	std::wstring m_videoCardDescription;

	CComPtr<IDXGISwapChain> m_pdxgiSwapChain;
	CComPtr<ID3D11Device> m_pd3dDevice;
	CComPtr<ID3D11DeviceContext> m_pd3dDeviceContext;
	CComPtr<ID3D11RenderTargetView> m_pd3dRenderTargetView;
	CComPtr<ID3D11Texture2D> m_pd3dDepthStencilBuffer;
	CComPtr<ID3D11DepthStencilState> m_pd3dDepthStencilState;
	CComPtr<ID3D11DepthStencilState> m_pd3dDepthStencilDisableState;
	CComPtr<ID3D11DepthStencilView> m_pd3dDepthStencilView;
	CComPtr<ID3D11RasterizerState> m_pd3dRasterState;
};
