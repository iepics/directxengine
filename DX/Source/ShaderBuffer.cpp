#include "stdafx.h"
#include "ShaderBuffer.h"
#include "D3D.h"

ShaderBuffer::ShaderBuffer(void)
{
}


ShaderBuffer::~ShaderBuffer(void)
{
}
//
//bool ShaderBuffer::CreateBuffer()
//{
//	HRESULT result;
//
//	D3D *d3d = D3D::GetInstance();
//
//	D3D11_BUFFER_DESC worldBufferDesc;
//	ZeroMemory(&worldBufferDesc, sizeof(D3D11_BUFFER_DESC));
//	worldBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
//	worldBufferDesc.ByteWidth = sizeof(ObjectBuffer);
//	worldBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//	worldBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//
//	result = d3d->GetDevice()->CreateBuffer(&worldBufferDesc, NULL, &mObjectBuffer);
//	if(FAILED(result))
//	{
//		return false;
//	}
//
//	D3D11_BUFFER_DESC cameraBufferDesc;
//	ZeroMemory(&cameraBufferDesc, sizeof(D3D11_BUFFER_DESC));
//	cameraBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
//	cameraBufferDesc.ByteWidth = sizeof(CameraBuffer);
//	cameraBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//	cameraBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//
//	result = d3d->GetDevice()->CreateBuffer(&cameraBufferDesc, NULL, &mCameraBuffer);
//	if(FAILED(result))
//	{
//		return false;
//	}
//
//	D3D11_BUFFER_DESC lightBufferType;
//	ZeroMemory(&lightBufferType, sizeof(D3D11_BUFFER_DESC));
//	lightBufferType.Usage = D3D11_USAGE_DYNAMIC;
//	lightBufferType.ByteWidth = sizeof(LightBuffer);
//	lightBufferType.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//	lightBufferType.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//	result = d3d->GetDevice()->CreateBuffer(&lightBufferType, NULL, &mLightBuffer);
//	if(FAILED(result))
//	{
//		return false;
//	}
//	
//	D3D11_BUFFER_DESC boneBufferType;
//	ZeroMemory(&boneBufferType, sizeof(D3D11_BUFFER_DESC));
//	boneBufferType.Usage = D3D11_USAGE_DYNAMIC;
//	boneBufferType.ByteWidth = sizeof(BoneBuffer);
//	boneBufferType.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//	boneBufferType.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//	result = d3d->GetDevice()->CreateBuffer(&boneBufferType, NULL, &mBoneBuffer);
//	if(FAILED(result))
//	{
//		return false;
//	}
//
//
//	return true;
//}
//bool ShaderBuffer::SetObjectBuffer(ObjectBuffer& matrixBuffer)
//{
//	HRESULT result;
//	D3D11_MAPPED_SUBRESOURCE mappedResource;
//	ObjectBuffer *pData;
//	
//	XMMATRIX worldMat = XMMatrixTranspose(matrixBuffer.World);
//
//	D3D *d3d = D3D::GetInstance();
//	result = d3d->GetDeviceContext()->Map(mObjectBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
//	if(FAILED(result))
//	{
//		return false;
//	}
//
//	pData = (ObjectBuffer*)mappedResource.pData;
//	pData->World = worldMat;
//
//	d3d->GetDeviceContext()->Unmap(mObjectBuffer, 0);
//
////	Effects::DiffuseFX->SetObjectBuffer(mObjectBuffer);
////	Effects::DiffuseFX->SetWorldMatrix(matrixBuffer.World);
//
//	return true;
//}
//bool ShaderBuffer::SetCameraBuffer(CameraBuffer& matrixBuffer)
//{
//	HRESULT result;
//	D3D11_MAPPED_SUBRESOURCE mappedResource;
//	CameraBuffer *pData;
//	
//	XMMATRIX viewMat = XMMatrixTranspose(matrixBuffer.View);
//	XMMATRIX projectionMat = XMMatrixTranspose(matrixBuffer.Projection);
//
//	D3D *d3d = D3D::GetInstance();
//	result = d3d->GetDeviceContext()->Map(mCameraBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
//	if(FAILED(result))
//	{
//		return false;
//	}
//
//	pData = (CameraBuffer*)mappedResource.pData;
//	pData->View = viewMat;
//	pData->Projection = projectionMat;
//
//	d3d->GetDeviceContext()->Unmap(mCameraBuffer, 0);
//
//	d3d->GetDeviceContext()->VSSetConstantBuffers(1, 1, &mCameraBuffer);
////	Effects::DiffuseFX->SetCameraBuffer(mCameraBuffer);
////	Effects::DiffuseFX->SetViewMatrix(matrixBuffer.View);
////	Effects::DiffuseFX->SetProjection(matrixBuffer.Projection);
//
//	return true;
//}
//bool ShaderBuffer::SetLightBuffer(LightBuffer& lightBuffer)
//{
//	HRESULT result;
//	D3D11_MAPPED_SUBRESOURCE mappedResource;
//	LightBuffer *pData;
//	
//	D3D *d3d = D3D::GetInstance();
//	result = d3d->GetDeviceContext()->Map(mLightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
//	if(FAILED(result))
//	{
//		return false;
//	}
//	pData = (LightBuffer*)mappedResource.pData;
//	pData->Diffuse = lightBuffer.Diffuse;
//	pData->Direction = lightBuffer.Direction;
//	pData->Position = lightBuffer.Position;
//	pData->Range = lightBuffer.Range;
//	pData->Specular = lightBuffer.Specular;
//	pData->Spot = lightBuffer.Spot;
//
//	d3d->GetDeviceContext()->Unmap(mLightBuffer, 0);
//
////	Effects::DiffuseFX->SetLightBuffer(mLightBuffer);
//
//	return true;
//}
//
//bool ShaderBuffer::SetBoneBuffer(std::vector<XMFLOAT4X4> &boneTransforms)
//{
//	HRESULT result;
//	D3D11_MAPPED_SUBRESOURCE mappedResource;
//	BoneBuffer *pData;
//	
//	D3D *d3d = D3D::GetInstance();
//	result = d3d->GetDeviceContext()->Map(mBoneBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
//	if(FAILED(result))
//	{
//		return false;
//	}
//	pData = (BoneBuffer*)mappedResource.pData;
//	for(unsigned int i = 0; i<boneTransforms.size(); ++i)
//	{
//		XMMATRIX mat = XMMatrixTranspose(XMLoadFloat4x4(&boneTransforms[i]));		
//		XMStoreFloat4x4(&pData->BoneTransforms[i], mat);
//	}
//	d3d->GetDeviceContext()->Unmap(mBoneBuffer, 0);
//	
////	d3d->GetDeviceContext()->VSSetConstantBuffers(VS_SLOT_SKINNED, 1, &mBoneBuffer);
//
//	return true;
//}