#include "stdafx.h"
#include "GlyphBank.h"


GlyphBank::GlyphBank()
{
}

GlyphBank::~GlyphBank()
{
}

void GlyphBank::Initialize(int width, int height)
{
	HRESULT result;
	ID3D11Device* device = D3D::GetInstance()->GetDevice();

	FT_Init_FreeType(&mFTLibrary);

	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Width = width;
	textureDesc.Height = height;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_STAGING;
	textureDesc.BindFlags = 0;
	textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	textureDesc.MiscFlags = 0;
	result = device->CreateTexture2D(&textureDesc, NULL, &mFTStagingTexture);

	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Width = width;
	textureDesc.Height = height;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;
	result = device->CreateTexture2D(&textureDesc, NULL, &mFTTexture);

	//create shader resource views
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	shaderResourceViewDesc.Format = textureDesc.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	result = device->CreateShaderResourceView(mFTTexture, &shaderResourceViewDesc, &mFTTextureResource);
	
	mTextureW = width;
	mTextureH = height;

	mTextureMap.resize((width) * (height));
}

bool GlyphBank::TestSpace(int x, int y, int w, int h) const
{
	if (x + w >= mTextureW)
		return false;
	if (y + h >= mTextureH)
		return false;

	for (int i = x; i < x + w; ++i)
	{
		for (int j = y; j < y + h; ++j)
		{
			if (MapAt(i, j))
				return false;
		}
	}
	return true;
}

void GlyphBank::FillSpace(int x, int y, int w, int h, char fill)
{
	for (int i = x; i < x + w; ++i)
	{
		for (int j = y; j < y + h; ++j)
		{
			MapAt(i, j) = fill;
		}
	}
}

std::pair<int, int> GlyphBank::FindSpace(int w, int h) const
{
	for (int j = 0; j < mTextureH; ++j)
	{
		for (int i = 0; i < mTextureW; ++i)
		{
			if (TestSpace(i, j, w, h))
			{
				return std::make_pair(i, j);
			}
		}
	}

	return std::make_pair(-1, -1);
}

GlyphBank::GlyphData GlyphBank::GetGlyph(FT_Face face, wchar_t ch, int size)
{
	ID3D11DeviceContext* deviceContext = D3D::GetInstance()->GetDeviceContext();

	Glyph g = { face, ch, size };
	GlyphData gd = { 0, };

	//글리프맵에 글리프가 있으면 찾아서 리턴
	auto it = mGlyphMap.find(g);
	if (it != mGlyphMap.cend())
	{
		it->second.used = true;
		return it->second;
	}

	FT_UInt ix = FT_Get_Char_Index(face, ch);
	//문자가 없다면 리턴
	if (ix == 0)
	{
		return gd;
	}

	//글리프를 읽는 것에 실패하면 리턴
	if (FT_Load_Glyph(face, ix, FT_LOAD_RENDER | FT_LOAD_NO_BITMAP))
	{
		return gd;
	}

	gd.bmW = face->glyph->bitmap.width;
	gd.bmH = face->glyph->bitmap.rows;

	///4x4픽셀을 한 블럭으로
	auto p = FindSpace((gd.bmW + 3), (gd.bmH + 3));
	if (p.first < 0)
		return gd;

	gd.advX = face->glyph->advance.x >> 6;
	gd.advY = face->glyph->advance.y >> 6;
	gd.height = face->height >> 6;
	gd.left = face->glyph->bitmap_left;
	gd.top = face->glyph->bitmap_top;
	gd.u = p.first / (float)mTextureW;
	gd.v = p.second / (float)mTextureH;
	gd.w = gd.bmW / (float)mTextureW;
	gd.h = gd.bmH / (float)mTextureH;
	gd.used = true;

	D3D11_MAP eMapType = D3D11_MAP_WRITE;
	D3D11_MAPPED_SUBRESOURCE mappedResource;

	deviceContext->Map(mFTStagingTexture, 0, eMapType, 0, &mappedResource);
	
	unsigned char* pData = (unsigned char*)mappedResource.pData;
	unsigned int pitch = mappedResource.RowPitch;
	for (int j = 0; j < gd.bmH; ++j)
	{
		for (int i = 0; i < gd.bmW; ++i)
		{
			unsigned char b = face->glyph->bitmap.buffer[i + j*gd.bmW];
			pData[(i + p.first) * 4 + (j + p.second)*pitch + 0] = 255;
			pData[(i + p.first) * 4 + (j + p.second)*pitch + 1] = 255;
			pData[(i + p.first) * 4 + (j + p.second)*pitch + 2] = 255;
			pData[(i + p.first) * 4 + (j + p.second)*pitch + 3] = b;
		}
	}
	deviceContext->Unmap(mFTStagingTexture, 0);
	D3D11_BOX box;
	ZeroMemory(&box, sizeof(D3D11_BOX));
	box.right = gd.bmW;
	box.bottom = gd.bmH;
	deviceContext->CopySubresourceRegion(mFTTexture, 0, p.first, p.second, 0, mFTStagingTexture, 0, &box);

	FillSpace(p.first, p.second, (gd.bmW + 3), (gd.bmH + 3));

	mGlyphMap[g] = gd;

	return gd;
}

std::wstring GlyphBank::PrepareGlyph(FT_Face face, std::wstring str, int len, int size)
{
	std::wstring ret;
	ID3D11DeviceContext* deviceContext = D3D::GetInstance()->GetDeviceContext();
	
	D3D11_MAP eMapType = D3D11_MAP_WRITE;
	D3D11_MAPPED_SUBRESOURCE mappedResource; 
	deviceContext->Map(mFTStagingTexture, 0, eMapType, 0, &mappedResource);
	unsigned char* pData = (unsigned char*)mappedResource.pData;
	unsigned int pitch = mappedResource.RowPitch;

	for (int i = 0; i < len; ++i)
	{
		Glyph g = { face, str[i], size };

		auto it = mGlyphMap.find(g);
		if (it != mGlyphMap.cend())
		{
			continue;
		}

		FT_UInt index = FT_Get_Char_Index(face, str[i]);
		if (index == 0)		
		{
			//글리프가 없으면
			ret.push_back(str[i]);
			continue;
		}

		if (FT_Load_Glyph(face, index, FT_LOAD_RENDER | FT_LOAD_NO_BITMAP))
		{
			//글리프 읽는 것에 실패하면
			continue;
		}

		GlyphData gd = { 0, };
		gd.bmW = face->glyph->bitmap.width;
		gd.bmH = face->glyph->bitmap.rows;

		///4x4픽셀을 한 블럭으로
		auto p = FindSpace((gd.bmW + 3), (gd.bmH + 3));
		if (p.first < 0)
		{
			CleanGlyphMap();
			p = FindSpace((gd.bmW + 3), (gd.bmH + 3));
			if (p.first < 0) continue;
		}

		gd.advX = face->glyph->advance.x >> 6;
		gd.advY = face->glyph->advance.y >> 6;
		gd.height = face->size->metrics.height >> 6;
		gd.left = face->glyph->bitmap_left;
		gd.top = face->glyph->bitmap_top;
		gd.u = p.first / (float)mTextureW;
		gd.v = p.second / (float)mTextureH;
		gd.w = gd.bmW / (float)mTextureW;
		gd.h = gd.bmH / (float)mTextureH;
		gd.used = false;
		
		for (int j = 0; j < gd.bmH; ++j)
		{
			for (int i = 0; i < gd.bmW; ++i)
			{
				unsigned char b = face->glyph->bitmap.buffer[i + j*gd.bmW];
				pData[(i + p.first ) * 4 + (j + p.second )*pitch + 0] = 255;
				pData[(i + p.first ) * 4 + (j + p.second )*pitch + 1] = 255;
				pData[(i + p.first ) * 4 + (j + p.second )*pitch + 2] = 255;
				pData[(i + p.first ) * 4 + (j + p.second )*pitch + 3] = b;
			}
		}
		FillSpace(p.first, p.second, (gd.bmW + 3), (gd.bmH + 3));
		mGlyphMap[g] = gd;

	}
	deviceContext->Unmap(mFTStagingTexture, 0);
	deviceContext->CopyResource(mFTTexture, mFTStagingTexture);

	return ret;
}

void GlyphBank::CleanGlyphMap()
{
	std::map<Glyph, GlyphData> remain;
	for (auto p : mGlyphMap)
	{
		if (p.second.used == true)
		{
			remain.insert(p);
		}
		else
		{
			FillSpace(p.second.u * mTextureW, p.second.v*mTextureH, (p.second.bmW + 3), (p.second.bmH + 3), 0);
		}
	}

	mGlyphMap.swap(remain);
}