#pragma once

#pragma region Texture
class Texture 
{
public:
	enum FilterMode
	{
		Point, Bilinear, Trilinear, Aniso
	};
	enum WrapMode
	{
		Wrap, Clamp
	};
	enum Format
	{
		RGBA32 = DXGI_FORMAT_R8G8B8A8_UNORM, ARGB32 = DXGI_FORMAT_B8G8R8A8_UNORM, Depth = DXGI_FORMAT_R32_FLOAT, Shadow = DXGI_FORMAT_R32G32_FLOAT,
	};

	Texture(void);
	~Texture(void);

	unsigned int GetWidth(){ return mWidth; }
	unsigned int GetHeight(){ return mHeight; }
	Format GetFormat(){ return mFormat; }
	CComPtr<ID3D11ShaderResourceView> GetTexture();
	
protected:

	unsigned int mWidth;
	unsigned int mHeight;
	Format mFormat;

	CComPtr<ID3D11ShaderResourceView> mTextureResource;
};
#pragma endregion

#pragma region Texture2D
class Texture2D : public Texture
{
public:	
	Texture2D(unsigned int width = 0, unsigned int height = 0, Format format = Format::RGBA32);
	~Texture2D();

	void Apply();
	bool LoadFile(std::wstring name);

private:
	unsigned int mMipLevels;

	CComPtr<ID3D11Texture2D> mTexture;
};
#pragma endregion

#pragma region RenderTexture
class RenderTexture : public Texture
{
public:
	RenderTexture(unsigned int width = 0, unsigned int height = 0, int depth = 24, Format format = Format::RGBA32);
	~RenderTexture();

	void SetWidth(unsigned int w){ mWidth = w; }
	void SetHeight(unsigned int h){ mHeight = h; }
	void SetFormat(Format format){ mFormat = format; }

	CComPtr<ID3D11RenderTargetView> GetRenderTargetView(){ return mRenderTargetView; }
	CComPtr<ID3D11DepthStencilView> GetDepthStencilView(){ return mDepthStencilView; }

	bool Create();
	bool ApplyRenderTarget();
	void ClearRenderTarget(XMFLOAT4 color);

private:
	int mDepth;

	CComPtr<ID3D11Texture2D> mTexture;
	CComPtr<ID3D11RenderTargetView> mRenderTargetView;
	CComPtr<ID3D11Texture2D> mDepthTexture;
	CComPtr<ID3D11DepthStencilView> mDepthStencilView;
};
#pragma endregion
