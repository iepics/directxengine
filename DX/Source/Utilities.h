#pragma once
#include "GameObject.h"
#include "Components.h"

class EditorUtility
{
public:
	static std::shared_ptr<GameObject> CreateEmpty()
	{
		std::shared_ptr<GameObject> gameObj = std::make_shared<GameObject>();
		gameObj->SetName(L"gameobject");
		gameObj->InitComponent<Transform>();
		
		return gameObj;
	}

	static std::shared_ptr<GameObject> CreateLight(LightType type)
	{
		std::shared_ptr<GameObject> lightObj = std::make_shared<GameObject>();
		lightObj->SetName(L"light");
		lightObj->InitComponent<Transform>();
		auto& light = lightObj->InitComponent<Light>();

		if(type == LightType::Direction)
		{
			light->SetType(LightType::Direction);
		}
		if(type == LightType::Point)
		{
			light->SetType(LightType::Point);
			light->SetRange(10);
		}

		return lightObj;
	}

	static std::shared_ptr<GameObject> CreateText(std::wstring str)
	{		
		std::shared_ptr<GameObject> obj = std::make_shared<GameObject>();
		obj->InitComponent<UITransform>();

		obj->SetName(L"text");

		//게임오브젝트에 UIRenderer, TextUI 컴포넌트를 추가
		auto& renderer = obj->InitComponent<UIRenderer>();
		auto& text = obj->InitComponent<TextUI>();

		auto& font = ResourceManager::GetInstance()->GetFont(L"malgun.ttf");
		
		text->SetFont(font);
		text->SetSize(30);
		text->SetText(str);

		return obj;
	}

	static std::shared_ptr<GameObject> CreateImage(std::shared_ptr<Texture> image)
	{
		std::shared_ptr<GameObject> obj = std::make_shared<GameObject>();
		obj->InitComponent<UITransform>();

		obj->SetName(L"image");

		auto& renderer = obj->InitComponent<UIRenderer>();
		auto& imageUI = obj->InitComponent<ImageUI>();

		imageUI->SetImage(image);

		return obj;
	}
};

class MathUtility
{
public:
	static XMFLOAT3 ToEulerAngles(const XMFLOAT4& q)
	{
		XMFLOAT3 angle;
		float sqw = q.w*q.w;
		float sqx = q.x*q.x;
		float sqy = q.y*q.y;
		float sqz = q.z*q.z;
		angle.x = atan2(2 * ((q.w * q.x) + (q.y*q.z)), 1 - (2 * (sqx + sqy)));
		angle.y = asin(2 * ((q.w*q.y) - (q.z*q.x)));
		angle.z = atan2(2 * ((q.w * q.z) + (q.x*q.y)), 1 - (2 * (sqy + sqz)));

		angle.x = XMConvertToDegrees(angle.x);
		angle.y = XMConvertToDegrees(angle.y);
		angle.z = XMConvertToDegrees(angle.z);

		return angle;
	}
};

