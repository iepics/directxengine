#pragma once
#include "Object.h"

class GameObject;
class Component : public Cloneable<Object, Component>
{
friend class GameObject;
public:
	virtual ~Component(void) = default;
	virtual void OnLoad(){}
	
	std::shared_ptr<GameObject> GetGameObject() const { return mGameObject.lock(); }
protected:
	Component(void) = default;
	void SetGameObject(std::shared_ptr<GameObject>& gameObject);
	
	std::weak_ptr<GameObject> mGameObject;
};