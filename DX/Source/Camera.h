#pragma once
#include "Transform.h"
#include "ShaderBuffer.h"
using namespace DirectX;

class Camera : public Cloneable<Component, Camera>
{
public:
	enum ProjectionType
	{
		Perspective,
		Orthographic,
	};

	Camera();
	~Camera();

	void SetViewMatrix(XMFLOAT4X4 matrix){ mViewMatrix = matrix; }
	void SetProjectionMatrix(XMFLOAT4X4 matrix){ mProjectionMatrix = matrix; }
	XMMATRIX GetViewMatrix() const{ return XMLoadFloat4x4(&mViewMatrix); }
	XMMATRIX GetProjection() const{ return XMLoadFloat4x4(&mProjectionMatrix); }

	void SetClearColor(XMFLOAT4 color){ mClearColor = color; }
	void SetType(ProjectionType type) { mProjType = type; }

	///Perspective
	void SetClipping(XMFLOAT2 clipping){ mClipping = clipping; }
	void SetViewRect(const XMFLOAT4& viewRect){ mViewRect = viewRect; }
	void SetFOV(float fov){ mFieldOfView = fov; }
	XMFLOAT2 GetClipping() const{ return mClipping; }
	XMFLOAT4 GetViewRect() const{ return mViewRect; }
	float GetFOV() const{ return mFieldOfView; }

	///Orthographic
	void SetWidth(float width){ mWidth = width; }
	void SetHeight(float height){ mHeight = height; }
	void SetDepth(float depth){ mDepth = depth; }

	void UpdateMatrix();

	void SetCameraBuffer();
	void SetRenderTexture(std::shared_ptr<RenderTexture>& renderTexture)
	{
		if(dynamic_cast<RenderTexture*>(renderTexture.get()))
		{
			mRenderTexture = renderTexture;
		}
	}

	void SetViewport();

	static void SetMainCamera(std::shared_ptr<Camera> camera){ mainCamera = camera; }
	static const std::shared_ptr<Camera> GetMainCamera(){ return mainCamera.lock(); }

private:
	static std::weak_ptr<Camera> mainCamera;

	ProjectionType mProjType = ProjectionType::Perspective;
	XMFLOAT4X4 mViewMatrix;
	XMFLOAT4X4 mProjectionMatrix;

	XMFLOAT4 mClearColor = XMFLOAT4(0, 1, 1, 1);

	///Perspective
	XMFLOAT2 mClipping;
	XMFLOAT4 mViewRect;
	float mFieldOfView;

	///Orthographic
	float mWidth;
	float mHeight;
	float mDepth;
	
	std::weak_ptr<RenderTexture> mRenderTexture;
};