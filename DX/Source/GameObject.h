#pragma once
#include <list>
#include <memory>
#include <unordered_map>
#include "Object.h"
#include "Component.h"
#include "Transform.h"

using namespace DirectX;


class GameObject : public std::enable_shared_from_this<GameObject>, public Cloneable<Object, GameObject>
{
friend class Scene;
public:
	GameObject();
	GameObject(const GameObject& other);
	~GameObject();

	virtual std::shared_ptr<Object> Clone();

	//씬에 등록되기 전에 생성된 게임오브젝트에 추가할 때 (instantiate 전)
	template<typename T>
	std::shared_ptr<T> InitComponent()
	{
		std::shared_ptr<T> retValue = nullptr;
		//if (std::is_base_of<Component, T>::value == false) 
		//{
		//	printf("추상 클래스는 컴포넌트로 추가할 수 없습니다.");
		//	return retValue;
		//}

		std::string typeName = typeid(T).name();
		auto pos = typeName.find_last_of(" ") + 1;
		typeName = typeName.substr(pos);
		auto findValue = mComponents.find(typeName);
		if (findValue == mComponents.cend())
		{
			retValue = std::make_shared<T>();
			mComponents[typeName] = retValue;
			mComponents[typeName]->SetGameObject(shared_from_this());
			mComponents[typeName]->SetName(mName);
		}

		return retValue;
	}

	//씬에 등록된 게임오브젝트에 추가할 때 (instantiate 후)
	template<typename T>
	std::shared_ptr<T> AddComponent()
	{
		std::shared_ptr<T> retValue = nullptr;
		//if (std::is_base_of<Component, T>::value == false) 
		//{
		//	printf("추상 클래스는 컴포넌트로 추가할 수 없습니다.");
		//	return retValue;
		//}

		std::string typeName = typeid(T).name();
		auto pos = typeName.find_last_of(" ") + 1;
		typeName = typeName.substr(pos);
		auto findValue = mComponents.find(typeName);
		if (findValue == mComponents.cend())
		{		
			retValue = std::make_shared<T>();
			mComponents[typeName] = retValue;
			mComponents[typeName]->SetGameObject(shared_from_this());
			mComponents[typeName]->SetName(mName);

			auto currScene = SceneManager::GetInstance()->GetCurrentScene();
			if (currScene != nullptr)
			{
				currScene->AddComponent(retValue);
			}
		}

		return retValue;
	}

	template<typename T>
	std::shared_ptr<T> GetComponent() const
	{
		std::shared_ptr<T> retValue = nullptr;

		for (auto value : mComponents)
		{
			if (dynamic_cast<T*>(value.second.get()))
			{
				retValue = std::static_pointer_cast<T, Component>(value.second);
				return retValue;
			}
		}

		return retValue;
	}

	template<typename T>
	std::shared_ptr<T> GetComponentInChildren()
	{
		std::shared_ptr<T> retValue = nullptr;

		GetComponentInChildrenRecursively<T>(shared_from_this(), retValue);

		return retValue;
	}

	std::shared_ptr<Transform> GetTransform() const;

	void SetChild(std::shared_ptr<GameObject>& transform);
	std::shared_ptr<GameObject> GetChild(unsigned int index) const;
	unsigned int GetChildCount() const;

	void SetParent(std::shared_ptr<GameObject>& transform);
	std::shared_ptr<GameObject> GetParent();

	std::shared_ptr<GameObject> Find(std::wstring name, bool recursive);


private:	
	void FindRecursively(std::wstring name, std::shared_ptr<GameObject>& transform, std::shared_ptr<GameObject>& result);
	
	template<typename T>
	void GetComponentInChildrenRecursively(std::shared_ptr<GameObject>& go, std::shared_ptr<T>& result)
	{
		auto retValue = go->GetComponent<T>();

		if (retValue != nullptr)
		{
			result = retValue;
			return;
		}

		auto childSize = go->GetChildCount();
		for (unsigned int i = 0; i < childSize; ++i)
		{
			auto child = go->GetChild(i);

			GetComponentInChildrenRecursively(child, result);
		}
	}

	std::unordered_map<std::string, std::shared_ptr<Component>> mComponents;
	std::vector<std::shared_ptr<GameObject>> mChilds;
	std::weak_ptr<GameObject> mParent;
};