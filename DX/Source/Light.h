#pragma once

enum LightType
{
	Direction,
	Point,
	Spot,
};

enum ShadowType
{
	No,
	Hard,
	Soft,
};

enum RenderMode
{
	Auto,
	Pixel,
	Vertex,
};

class ShaderBuffer;
class Component;
class Light : public Cloneable<Component, Light>
{
public:
	Light(void);
	~Light(void);

	void SetType(LightType type);
	void SetColor(XMFLOAT4 color);
	void SetIntensity(float intensity);
	void SetRange(float range);
	void SetSpot(float spot);
	void SetRenderMode(RenderMode mode);
	void SetShadowType(ShadowType type);

	LightType GetType();
	XMFLOAT4 GetColor();
	float GetIntensity();
	float GetRange();
	float GetSpot();
	RenderMode GetRenderMode();
	ShadowType GetShadowType(){ return mShadowType; }
	std::shared_ptr<RenderTexture> GetShadowMap();
	XMMATRIX GetLightView(){ return XMLoadFloat4x4(&mLightView); }
	XMMATRIX GetLightProj(){ return XMLoadFloat4x4(&mLightProj); }

	void RenderShadow();

	static void SortLight();
	static void SortLight(std::list<std::weak_ptr<Light>>& lights);
	static void SetBaseBuffer()
	{
		auto buffer = ShaderBuffer::GetInstance();
		
		if(!directionalLight.expired())
		{
			auto light = directionalLight.lock();
			buffer->Color = light->GetColor();
			XMFLOAT3 dir = light->GetGameObject()->GetTransform()->GetForwardVector();
			buffer->Position.x = dir.x;
			buffer->Position.y = dir.y;
			buffer->Position.z = dir.z;
			buffer->Position.w = 0;
			light->GetGameObject();

			if(light->GetShadowType() != ShadowType::No)
			{
				XMStoreFloat4x4(&buffer->LightView, light->GetLightView());
				XMStoreFloat4x4(&buffer->LightProj, light->GetLightProj());
			}
		}

		/*int index = 0;
		for(auto& light : vertexLights)
		{
			if(index < 4 && !light.expired())
			{
				auto lit = light.lock();

				buffer->LightColor[index] = lit->GetColor();
				XMFLOAT3 pos = lit->GetGameObject()->GetTransform()->GetPosition();
				buffer->LightPos[index].x = pos.x;
				buffer->LightPos[index].y = pos.y;
				buffer->LightPos[index].z = pos.z;
				buffer->LightPos[index].w = 1;

				if(lit->GetType() == LightType::Direction)
				{
					XMFLOAT3 dir = lit->GetGameObject()->GetTransform()->GetForwardVector();
					buffer->LightPos[index].x = dir.x;
					buffer->LightPos[index].y = dir.y;
					buffer->LightPos[index].z = dir.z;
					buffer->LightPos[index].w = 0;
				}
			}
			++index;
		}	*/	
	}
	static const std::shared_ptr<Light> GetDirectionalLight(){ return directionalLight.lock(); }
	static const std::list<std::weak_ptr<Light>>& GetPixelLights(){ return pixelLights; }
//	static const std::list<Light*>& GetVertexLights(){ return vertexLights; }

protected:
	virtual void OnAttached(std::shared_ptr<Component> com);
	virtual void OnDetached(std::shared_ptr<Component> com);

private:
	static std::list<std::weak_ptr<Light>> allLights;
	static std::weak_ptr<Light> directionalLight;	//brightest Directional Light
	static std::list<std::weak_ptr<Light>> pixelLights;
	static std::list<std::weak_ptr<Light>> vertexLights;

	LightType mType = LightType::Direction;

	///DirectionalLight
	XMFLOAT4 mColor = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	float mIntensity = 1.0f;
	
	///PointLight
	float mRange = 1.0f;

	///SpotLight
	float mSpot;
	
	///Render Mode
	RenderMode mRenderMode = RenderMode::Auto;

	///Shadow
	ShadowType mShadowType = ShadowType::No;
	std::shared_ptr<RenderTexture> mShadowTexture;
	XMFLOAT4X4 mLightView;
	XMFLOAT4X4 mLightProj;

};