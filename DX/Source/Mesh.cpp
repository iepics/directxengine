#include "stdafx.h"
#include "Mesh.h"


Mesh::Mesh()
{
	mVertexBuffer = nullptr;

	m_d3dPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
}

Mesh::~Mesh(void)
{
	Destroy();
}

void Mesh::Initialize()
{
	CreateVertexBuffer();
	CreateIndexBuffer();
}

void Mesh::Destroy()
{
	mVertices.clear();
	mSubMeshes.clear();
	
	if (mVertexBuffer != nullptr)
		mVertexBuffer->Release();

	for (auto it : mIndexBuffers)
	{
		it->Release();
	}
}

D3D11_PRIMITIVE_TOPOLOGY Mesh::GetPrimitiveTopology()
{
	return m_d3dPrimitiveTopology;
}

void Mesh::SetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitive)
{
	m_d3dPrimitiveTopology = primitive;
}

bool Mesh::CreateVertexBuffer()
{	
	D3D11_BUFFER_DESC vertexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData;
	HRESULT result;

	//정점 버퍼 생성
	ZeroMemory(&vertexBufferDesc, sizeof(D3D11_BUFFER_DESC));
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = (unsigned int)(sizeof(ModelVertex) * mVertices.size());
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	vertexData.pSysMem = &mVertices[0];
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	D3D* d3d = D3D::GetInstance();
	result = d3d->GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexData, &mVertexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	return true;
}
bool Mesh::CreateIndexBuffer()
{
	D3D11_BUFFER_DESC indexBufferDesc;
	D3D11_SUBRESOURCE_DATA indexData;
	HRESULT result;
	
	mIndexBuffers.resize(mSubMeshes.size());
	for(unsigned int i = 0; i<mSubMeshes.size(); i++)
	{
		//인덱스 버퍼 생성
		ZeroMemory(&indexBufferDesc, sizeof(D3D11_BUFFER_DESC));
		indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		indexBufferDesc.ByteWidth = (unsigned int)(sizeof(UINT) * mSubMeshes[i].indices.size());
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

		indexData.pSysMem = &mSubMeshes[i].indices[0];
		indexData.SysMemPitch = 0;
		indexData.SysMemSlicePitch = 0;
	
		D3D* d3d = D3D::GetInstance();
		result = d3d->GetDevice()->CreateBuffer(&indexBufferDesc, &indexData, &mIndexBuffers[i]);
		if(FAILED(result))
		{
			return false;
		}
	}

	return true;
}

#pragma region InputLayouts
ID3D11InputLayout* InputLayouts::UIInputLayout = 0;
ID3D11InputLayout* InputLayouts::DebugInputLayout = 0;
ID3D11InputLayout* InputLayouts::ModelInputLayout = 0; 

bool InputLayouts::InitAll()
{
	//ID3D11Device* device = D3D::GetInstance()->GetDevice();

	//HRESULT result;
	//D3DX11_PASS_DESC passDesc;
	//unsigned int numElements;
	
	//Effects::UIFX->mDefault->GetPassByIndex(0)->GetDesc(&passDesc);
	//D3D11_INPUT_ELEMENT_DESC uiInputDesc[] =
	//{
	//	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//	{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//	{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	//};
	//numElements = ARRAYSIZE(uiInputDesc);
	//result = device->CreateInputLayout(uiInputDesc, numElements, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &UIInputLayout);
	//if (FAILED(result))
	//{
	//	return false;
	//}

	//Effects::DebugFX->mDefault->GetPassByIndex(0)->GetDesc(&passDesc);
	//D3D11_INPUT_ELEMENT_DESC debugInputDesc[] =
	//{
	//	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//	{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//};
	//numElements = ARRAYSIZE(debugInputDesc);
	//result = device->CreateInputLayout(debugInputDesc, numElements, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &DebugInputLayout);
	//if (FAILED(result))
	//{
	//	return false;
	//}

	//Effects::DiffuseFX->mDefault->GetPassByIndex(0)->GetDesc(&passDesc);
	//D3D11_INPUT_ELEMENT_DESC modelInputDesc[6] =
	//{
	//	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//	{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//	{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//	{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//	{ "BONEINDICES", 0, DXGI_FORMAT_R8G8B8A8_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	//};
	//numElements = ARRAYSIZE(modelInputDesc);
	//result = device->CreateInputLayout(modelInputDesc, numElements, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &ModelInputLayout);
	//if (FAILED(result))
	//{
	//	return false;
	//}

	return true;
}

void InputLayouts::DestroyAll()
{
	ModelInputLayout->Release();
}
#pragma endregion