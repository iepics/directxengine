// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once
#define FBXSDK_SHARED
#pragma comment (lib, "libfbxsdk.lib")
#pragma comment (lib, "FreeImage.lib")
#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "dxgi.lib")

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
// Windows 헤더 파일:
#include <windows.h>

// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>


// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.
#include <D3D11.h>
#include <D3Dcompiler.h>
#include <DirectXMath.h>
#include <DirectXCollision.h>
#include <mmsystem.h>
#include <fstream>

#include <iostream>
#include <math.h>
#include <vector>
#include <unordered_map>
#include <map>


///external
#include <fbxsdk.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include <FreeImage.h>

///
#include "System.h"
#include "MathUtilities.h"
#include "Object.h"