#pragma once
#include <vector>
#include "Material.h"
#include "Mesh.h"
#include "Component.h"
#include "MathUtilities.h"
using namespace DirectX;

#pragma region Renderer
class Renderer : public Cloneable<Component, Renderer>
{
public:
	virtual ~Renderer(void){}

	virtual void Render(const std::shared_ptr<Effect>& fx = nullptr) {}

	virtual void SetMesh(std::shared_ptr<Mesh> pMesh){ mMesh = pMesh; }
	virtual void SetMaterials(std::vector<std::shared_ptr<Material>> materials){ mMaterials = materials; }
	virtual void AddMaterial(std::shared_ptr<Material>& pMaterial){ mMaterials.push_back(pMaterial); }

	std::shared_ptr<Mesh> GetMesh(){ return mMesh; }
	virtual std::vector<std::shared_ptr<Material>> GetMaterials(){ return mMaterials; }

	static const std::list<std::weak_ptr<Renderer>>& GetAllRenderers(){ return allRenderers; }

protected:
	Renderer(void){}
	virtual void OnAttached(std::shared_ptr<Component> com)
	{
		auto renderer = std::static_pointer_cast<Renderer, Component>(com);
		Renderer::allRenderers.push_back(renderer);
	}
	virtual void OnDetached(std::shared_ptr<Component> com)
	{
		auto renderer = std::static_pointer_cast<Renderer, Component>(com);
		Renderer::allRenderers.remove(renderer);
	}

	std::vector<std::shared_ptr<Material>> mMaterials;
	std::shared_ptr<Mesh> mMesh;

private:
	static std::list<std::weak_ptr<Renderer>> allRenderers;
};
#pragma endregion

#pragma region MeshRenderer
class MeshRenderer : public Cloneable<Renderer, MeshRenderer>
{
public:
	MeshRenderer();
	~MeshRenderer();

	virtual void Render(const std::shared_ptr<Effect>& fx = nullptr);
};
#pragma endregion

#pragma region SkinnedMeshRenderer
class SkinnedMeshRenderer : public Cloneable<Renderer, SkinnedMeshRenderer>
{
public:
	SkinnedMeshRenderer();
	~SkinnedMeshRenderer();

	virtual void OnLoad();

	virtual void Render(const std::shared_ptr<Effect>& fx = nullptr);

	void SetRootBoneByName(const std::wstring name);
	void SetRootBone(const std::shared_ptr<GameObject>& rootBone);

private:
	std::wstring mRootBoneName = L"";
	std::shared_ptr<GameObject> mRootBone;
	std::vector<std::shared_ptr<Transform>> mBones;
	std::vector<XMFLOAT4X4> mBindPoses;
};
#pragma endregion

#pragma region UIRenderer
class UIRenderer : public Cloneable<Component, UIRenderer>
{
public:
	UIRenderer(void);
	~UIRenderer(void){}

	void Render();

	void SetMesh(std::shared_ptr<Mesh> pMesh){ mMesh = pMesh; }
	void SetMaterial(std::shared_ptr<Material>& material){ mMaterial = material; }
	void SetTexture(ID3D11ShaderResourceView *texture) { mTexture = texture; }
	void SetColor(XMFLOAT4 color) { mColor = color; }
	//std::shared_ptr<Material>& GetMaterial(){ return mMaterial; }

protected:
	std::shared_ptr<Material> mMaterial;
	std::shared_ptr<Effect> mEffect;
	std::shared_ptr<Mesh> mMesh;
	XMFLOAT4 mColor = XMFLOAT4(1, 1, 1, 1);
	ID3D11ShaderResourceView *mTexture;

//private:
//	static std::list<std::weak_ptr<UIRenderer>> allRenderers;
};
#pragma endregion