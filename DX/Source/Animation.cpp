#include "stdafx.h"
#include "Animation.h"

using std::shared_ptr;

#pragma region AnimationCurve
AnimationCurve::AnimationCurve()
{

}
AnimationCurve::~AnimationCurve()
{

}

void AnimationCurve::AddKey(Keyframetemp key)
{
	mKeys.push_back(key);
}
float AnimationCurve::Evaluate(float time)
{
	float retValue = 0;
	if (time <= mKeys.front().time)
	{
		retValue = mKeys.front().value;
	}
	else if (time >= mKeys.back().time)
	{
		retValue = mKeys.back().value;
	}
	else
	{
		for (unsigned int i = 0; i<mKeys.size() - 1; ++i)
		{
			if (time >= mKeys[i].time && time <= mKeys[i + 1].time)
			{
				float lerpPercent = (time - mKeys[i].time) / (mKeys[i + 1].time - mKeys[i].time);

				retValue = lerpPercent*(mKeys[i + 1].value - mKeys[i].value) + mKeys[i].value;
				
				break;
			}
		}
	}

	return retValue;
}
#pragma endregion

#pragma region AnimationClip
AnimationClip::AnimationClip()
{

}
AnimationClip::~AnimationClip()
{

}

void AnimationClip::SetCurve(shared_ptr<AnimCurveInfo>& curveInfo)
{
	mCurveInfos.push_back(curveInfo);
}
#pragma endregion

#pragma region Animation
Animation::Animation()
{
}

Animation::~Animation()
{
}

void Animation::OnLoad()
{
	mBoneTransforms.clear();
}

void Animation::Play(std::wstring const& name)
{
	auto iter = mAnimations.find(name);
	if(iter != mAnimations.cend())
	{
		auto& clip = iter->second;
		if(mCurrentTime > clip->GetLength())
		{
			mCurrentTime = 0.0f;
		}
		mCurrentTime += GameTimer::GetInstance()->GetDeltaTime() * 0.5f;

		auto& curves = clip->GetCurves();
		unsigned int size = (unsigned int)curves.size();
		for(unsigned int i = 0; i < size; ++i)
		{
			auto value = curves[i]->Evaluate(mCurrentTime);

			auto& boneName = curves[i]->GetBoneName();
			if(mBoneTransforms[boneName].expired())
			{
				auto& boneTrans = GetGameObject()->Find(boneName, true)->GetTransform();
				mBoneTransforms[boneName] = boneTrans;
			}

			auto type = curves[i]->GetType();
			auto& prop = curves[i]->GetProperty();

			if(type == AnimCurveInfo::Transform)
			{
				auto position = mBoneTransforms[boneName].lock()->GetLocalPosition();
				auto angle = mBoneTransforms[boneName].lock()->GetLocalAngle();
				if(prop == "TranslationX")
					position.x = value;
				else if(prop == "TranslationY")
					position.y = value;
				else if(prop == "TranslationZ")
					position.z = value;
				else if(prop == "RotationX")
					angle.x = value;
				else if(prop == "RotationY")
					angle.y = value;
				else if(prop == "RotationZ")
					angle.z = value;

				mBoneTransforms[boneName].lock()->SetLocalPosition(position);
				mBoneTransforms[boneName].lock()->SetLocalAngle(angle);
			}
		}

		mIsPlaying = true;
	}
}

void Animation::AddClip(shared_ptr<AnimationClip>& clip)
{
	if(clip == nullptr)
	{
		return;
	}
	
	auto& name = clip->GetName();
	mAnimations[name] = clip;
}
#pragma endregion

