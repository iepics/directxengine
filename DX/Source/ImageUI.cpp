#include "stdafx.h"
#include "ImageUI.h"


ImageUI::ImageUI()
{
}

ImageUI::~ImageUI()
{
}

void ImageUI::SetImage(const std::shared_ptr<Texture> image)
{
	mImage = image;

	CreateMesh();
}

void ImageUI::SetColor(const XMFLOAT4 color)
{
	mColor = color; 

	const auto& renderer = GetGameObject()->GetComponent<UIRenderer>();
	if (renderer != nullptr)
	{
		renderer->SetColor(mColor);
	}
}

void ImageUI::CreateMesh()
{
	std::shared_ptr<Mesh> mesh = std::make_shared<Mesh>();
	std::vector<ModelVertex> vertices;
	std::vector<SubMesh> subMeshes;
	subMeshes.resize(1);

	auto uiTrans = GetGameObject()->GetComponent<UITransform>();

	float x = uiTrans->GetWidth() / 2;
	float y = uiTrans->GetHeight() / 2;
	ModelVertex vertex;
	vertex.position = XMFLOAT3(-x, y, 0);
	vertex.uv.x = 0;
	vertex.uv.y = 0;
	vertices.push_back(vertex);

	vertex.position = XMFLOAT3(x, y, 0);
	vertex.uv.x = 1;
	vertex.uv.y = 0;
	vertices.push_back(vertex);

	vertex.position = XMFLOAT3(x, -y, 0);
	vertex.uv.x = 1;
	vertex.uv.y = 1;
	vertices.push_back(vertex);

	vertex.position = XMFLOAT3(-x, -y, 0);
	vertex.uv.x = 0;
	vertex.uv.y = 1;
	vertices.push_back(vertex);

	subMeshes[0].indices.push_back(0);
	subMeshes[0].indices.push_back(1);
	subMeshes[0].indices.push_back(2);
	subMeshes[0].indices.push_back(2);
	subMeshes[0].indices.push_back(3);
	subMeshes[0].indices.push_back(0);

	mesh->SetVertices(vertices);
	mesh->SetSubMeshes(subMeshes);
	mesh->CreateVertexBuffer();
	mesh->CreateIndexBuffer();

	const auto& renderer = GetGameObject()->GetComponent<UIRenderer>();
	if(renderer != nullptr && !mImage.expired())
	{
		renderer->SetMesh(mesh);
		renderer->SetTexture(mImage.lock()->GetTexture());
	}
}