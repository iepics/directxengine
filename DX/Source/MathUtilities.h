#pragma once
#include <DirectXMath.h>

namespace Color
{
	const DirectX::XMFLOAT4 red = DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	const DirectX::XMFLOAT4 green = DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);
	const DirectX::XMFLOAT4 blue = DirectX::XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);
};


///operator overloading
DirectX::XMFLOAT3 operator+(const DirectX::XMFLOAT3 &left, const DirectX::XMFLOAT3 &right);
DirectX::XMFLOAT3 operator-(const DirectX::XMFLOAT3 &left, const DirectX::XMFLOAT3 &right);
DirectX::XMFLOAT3& operator+=(DirectX::XMFLOAT3 &left, const DirectX::XMFLOAT3 &right);
DirectX::XMFLOAT3& operator-=(DirectX::XMFLOAT3 &left, const DirectX::XMFLOAT3 &right);
DirectX::XMFLOAT3 operator*(const DirectX::XMFLOAT3 &left, const float right);
DirectX::XMFLOAT3& operator*=(DirectX::XMFLOAT3 &left, const float right);
DirectX::XMFLOAT4 operator+(const DirectX::XMFLOAT4 &left, const DirectX::XMFLOAT4 &right);
DirectX::XMFLOAT4& operator+=(DirectX::XMFLOAT4 &left, const DirectX::XMFLOAT4 &right);



///
template<class T>
bool operator==(const std::weak_ptr<T> left, const std::weak_ptr<T> right)
{
	std::shared_ptr<T> l = left.lock();
	std::shared_ptr<T> r = right.lock();
	if(l && r)
		return l == r;
	return false;
}