#include "stdafx.h"
#include "System.h"


System::System(void)
{
}

System::~System(void)
{
}

bool System::Initialize(int width, int height, bool bFull, bool bVsync)
{
	bool result;

	mClientWidth = width;
	mClientHeight = height;
	mFullscreen = bFull;
	mVsync = bVsync;

	InitializeWindows();

	mGraphics = std::make_shared<Graphics>();
	result = mGraphics->Initialize(mHWnd);
	if(!result)
	{
		return false;
	}

	return true;
}

void System::Destroy()
{
	ShutdownWindows();
}

void System::Run()
{
	MSG msg;

	ZeroMemory(&msg, sizeof(MSG));

	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			Frame();
		}
	}
}

bool System::Frame()
{
	GameTimer::GetInstance()->Tick();

	mGraphics->Frame();

	return true;
}

LRESULT CALLBACK System::MessageHandler(HWND hWnd, UINT nMessageID,
		WPARAM wParam, LPARAM lParam)
{
	switch(nMessageID)
	{

	default:
		return DefWindowProc(hWnd, nMessageID, wParam, lParam);
	}
}

void System::InitializeWindows()
{
	WNDCLASSEX wc;
	int posX, posY;

	mHInstance = GetModuleHandle(NULL);
	mAppName = _T("Engine");

	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = mHInstance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = mAppName.c_str();
	wc.cbSize = sizeof(WNDCLASSEX);

	RegisterClassEx(&wc);


	DWORD dwStyle = WS_OVERLAPPED | WS_CAPTION | WS_MINIMIZEBOX
		| WS_SYSMENU | WS_BORDER;
	RECT rc = {0, 0, mClientWidth, mClientHeight};
	AdjustWindowRect(&rc, dwStyle, FALSE);

	posX = (GetSystemMetrics(SM_CXSCREEN) - mClientWidth) / 2;
	posY = (GetSystemMetrics(SM_CYSCREEN) - mClientHeight) / 2;

	posX = posX <= 0 ? 0 : posX;
	posY = posY <= 0 ? 0 : posY;

	mHWnd = CreateWindowEx(WS_EX_APPWINDOW, mAppName.c_str(), mAppName.c_str(),
		dwStyle, posX, posY, rc.right - rc.left, rc.bottom - rc.top,
		NULL, NULL, mHInstance, NULL);

	ShowWindow(mHWnd, SW_SHOW);
	UpdateWindow(mHWnd);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT nMessageID,
		WPARAM wParam, LPARAM lParam)
{
	switch(nMessageID)
	{
	case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
	case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}
	default:
		return System::GetInstance()->MessageHandler(hWnd, nMessageID,
		wParam, lParam);
	}
}

void System::ShutdownWindows()
{
	ShowCursor(true);

	DestroyWindow(mHWnd);
	mHWnd = NULL;

	UnregisterClass(mAppName.c_str(), mHInstance);
	mHInstance = NULL;
}