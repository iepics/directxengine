#include "stdafx.h"
#include "Effects.h"

#pragma region Effect
Effect::Effect()
{
	mFX = nullptr;
	mInputLayout = nullptr;
}

Effect::~Effect()
{
}

bool Effect::FromFile(const std::wstring& fileName)
{
	HRESULT result;

	DWORD shaderFlags = 0;
#ifdef _DEBUG
	shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif
	CComPtr<ID3DBlob> compileShader = nullptr;
	CComPtr<ID3DBlob> compileMsg = nullptr;
		
	result = D3DCompileFromFile(fileName.c_str(), 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, 0, "fx_5_0", D3D10_SHADER_DEBUG | D3D10_SHADER_SKIP_OPTIMIZATION, 0, &compileShader.p, &compileMsg.p);
	if (FAILED(result))
	{
		OutputErrorMessage(compileMsg, L"shaderCompile.log");

		return false;
	}

	D3D* d3dInstance = D3D::GetInstance();
	result = D3DX11CreateEffectFromMemory(compileShader->GetBufferPointer(), compileShader->GetBufferSize(), 0, d3dInstance->GetDevice(), &mFX);
	if (FAILED(result))
	{
		return false;
	}


	///effect variable
	D3DX11_EFFECT_DESC fxDesc;
	mFX->GetDesc(&fxDesc);

	CComPtr<ID3DX11EffectVariable> fxVariable;
	CComPtr<ID3DX11EffectTechnique> fxTech;
	D3DX11_EFFECT_VARIABLE_DESC varDesc;
	D3DX11_TECHNIQUE_DESC techDesc;
	D3DX11_EFFECT_TYPE_DESC typeDesc;
	
	for (unsigned int i = 0; i < fxDesc.GlobalVariables; ++i)
	{
		fxVariable = mFX->GetVariableByIndex(i);
		fxVariable->GetDesc(&varDesc);
		mVariables[varDesc.Name] = fxVariable;
	}
	
	for (unsigned int i = 0; i < fxDesc.Techniques; ++i)
	{
		fxTech = mFX->GetTechniqueByIndex(i);
		fxTech->GetDesc(&techDesc);
		mTechniques[techDesc.Name] = fxTech;
	}


	///create inputlayout
	ID3D11Device* device = D3D::GetInstance()->GetDevice();
	D3DX11_PASS_DESC passDesc;
	unsigned int numElements;
	mTechniques.begin()->second->GetPassByIndex(0)->GetDesc(&passDesc);	
	
	D3D11_INPUT_ELEMENT_DESC modelInputDesc[6] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONEINDICES", 0, DXGI_FORMAT_R8G8B8A8_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	numElements = ARRAYSIZE(modelInputDesc);
	result = device->CreateInputLayout(modelInputDesc, numElements, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &mInputLayout);
	if (FAILED(result))
	{
		return false;
	}

	return true;
}

CComPtr<ID3DX11EffectTechnique> Effect::GetTechnique(std::string name)
{
	auto retValue = mTechniques.find(name);

	if (retValue != mTechniques.cend())
	{
		return retValue->second;
	}
	else
	{
		return nullptr;
	}
}
CComPtr<ID3DX11EffectVariable> Effect::GetVariable(std::string name)
{
	auto retValue = mVariables.find(name);

	if (retValue != mVariables.cend())
	{
		return retValue->second;
	}
	else
	{
		return nullptr;
	}
}

void Effect::SetMatrix(std::string name, CXMMATRIX matrix)
{
	auto value = mVariables.find(name);
	
	if (value != mVariables.cend())
	{
		CComPtr<ID3DX11EffectVariable> fxVariable = value->second;
		fxVariable->AsMatrix()->SetMatrix(reinterpret_cast<const float*>(&matrix));
	}
	else
	{

	}
}
void Effect::SetBoneTransforms(const XMMATRIX* matrices, int cnt)
{
	auto value = mVariables.find("gBoneTransforms");

	if (value != mVariables.cend())
	{
		CComPtr<ID3DX11EffectVariable> fxVariable = value->second;
		fxVariable->AsMatrix()->SetMatrixArray(reinterpret_cast<const float*>(matrices), 0, cnt);
	}
	else
	{

	}
}
void Effect::SetTexture(std::string name, CComPtr<ID3D11ShaderResourceView> texture)
{
	auto value = mVariables.find(name);

	if (value != mVariables.cend())
	{
		CComPtr<ID3DX11EffectVariable> fxVariable = value->second;
		fxVariable->AsShaderResource()->SetResource(texture);
	}
	else
	{

	}
}
void Effect::SetVariable(std::string name, unsigned int size, void* pData)
{
	auto value = mVariables.find(name);

	if (value != mVariables.cend())
	{
		CComPtr<ID3DX11EffectVariable> fxVariable = value->second;
		fxVariable->SetRawValue(pData, 0, size);
	}
	else
	{

	}
}

void Effect::OutputErrorMessage(CComPtr<ID3DBlob> pd3dBlob, const std::wstring& fileName)
{
	char* compileErrors;
	unsigned long bufferSize;
	std::ofstream fout;

	compileErrors = (char*)(pd3dBlob->GetBufferPointer());
	bufferSize = (unsigned int)pd3dBlob->GetBufferSize();
	fout.open(fileName);

	for (unsigned int i = 0; i<bufferSize; i++)
	{
		fout << compileErrors[i];
	}

	fout.close();
}
#pragma endregion

