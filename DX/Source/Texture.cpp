#include "stdafx.h"
#include "Texture.h"


Texture::Texture(void)
{
}

Texture::~Texture(void)
{
}

CComPtr<ID3D11ShaderResourceView> Texture::GetTexture()
{
	return mTextureResource;
}

#pragma region Texture2D
Texture2D::Texture2D(unsigned int width, unsigned int height, Format format)
{

	//Create depth stencil texture
	auto device = D3D::GetInstance()->GetDevice();

	D3D11_TEXTURE2D_DESC texDesc;
	ZeroMemory(&texDesc, sizeof(D3D11_TEXTURE2D_DESC));
	texDesc.Width = width;
	texDesc.Height = height;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = (DXGI_FORMAT)format;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DYNAMIC;
	texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	texDesc.MiscFlags = 0;
	device->CreateTexture2D(&texDesc, NULL, &mTexture);

	mWidth = width;
	mHeight = height;
	mFormat = format;
	mMipLevels = 1;


	device->CreateShaderResourceView(mTexture, 0, &mTextureResource);
}

Texture2D::~Texture2D()
{
}

bool Texture2D::LoadFile(std::wstring name)
{
	HRESULT hr;

	auto device = D3D::GetInstance()->GetDevice();
	auto deviceContext = D3D::GetInstance()->GetDeviceContext();

	std::string strName;
	strName.assign(name.cbegin(), name.cend());

	// 파일로부터 image data를 불러온다
	FIBITMAP* bitmap = nullptr;
	auto fif = FreeImage_GetFIFFromFilename(strName.c_str());
	if(fif != FIF_UNKNOWN && FreeImage_FIFSupportsReading(fif))
	{
		bitmap = FreeImage_Load(fif, strName.c_str());
	}

	if(bitmap == nullptr)
	{
		return false;
	}
	
	// 처리를 단순하게 하기 위해 image data의 포맷 변경
	auto type = FreeImage_GetBPP(bitmap);
	if(type != 32)
	{
		bitmap = FreeImage_ConvertTo32Bits(bitmap);
	}

	int width = FreeImage_GetWidth(bitmap);
	int height = FreeImage_GetHeight(bitmap);
	int pitch = FreeImage_GetPitch(bitmap);
	type = FreeImage_GetBPP(bitmap);
	FreeImage_FlipVertical(bitmap);
	auto data = FreeImage_GetBits(bitmap);


	// 얻어온 image data를 이용하여 D3D texture를 생성
	D3D11_SUBRESOURCE_DATA subresData;
	subresData.pSysMem = data;
	subresData.SysMemPitch = pitch;
	subresData.SysMemSlicePitch = pitch * height;

	D3D11_TEXTURE2D_DESC texDesc;
	ZeroMemory(&texDesc, sizeof(D3D11_TEXTURE2D_DESC));
	texDesc.Width = width;
	texDesc.Height = height;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;
	hr = device->CreateTexture2D(&texDesc, &subresData, &mTexture);
	if(FAILED(hr))
	{
		return false;
	}

	mWidth = width;
	mHeight = height;
	mFormat = (Format)DXGI_FORMAT_B8G8R8A8_UNORM;
	mMipLevels = 1;
	hr = device->CreateShaderResourceView(mTexture, 0, &mTextureResource);
	if(FAILED(hr))
	{
		return false;
	}

	return true;
}
#pragma endregion

#pragma region RenderTexture
RenderTexture::RenderTexture(unsigned int width, unsigned int height, int depth, Format format)
{
	mWidth = width;
	mHeight = height;
	mDepth = depth;
	mFormat = format;
}

RenderTexture::~RenderTexture()
{
}

bool RenderTexture::Create()
{
	HRESULT result;

	auto device = D3D::GetInstance()->GetDevice();

	D3D11_TEXTURE2D_DESC texDesc;
	memset(&texDesc, 0, sizeof(texDesc));
	texDesc.Width = mWidth;
	texDesc.Height = mHeight;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = (DXGI_FORMAT)mFormat;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;

	result = device->CreateTexture2D(&texDesc, 0, &mTexture);
	if(FAILED(result))
	{
		return false;
	}

	result = device->CreateRenderTargetView(mTexture, 0, &mRenderTargetView);
	if(FAILED(result))
	{
		return false;
	}


	///Create DepthStencilView
	DXGI_FORMAT depthFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	if(mDepth == 16)
	{
		depthFormat = DXGI_FORMAT_D16_UNORM;
	}

	D3D11_TEXTURE2D_DESC depthTexDesc;
	memset(&depthTexDesc, 0, sizeof(depthTexDesc));
	depthTexDesc.Width = mWidth;
	depthTexDesc.Height = mHeight;
	depthTexDesc.MipLevels = 1;
	depthTexDesc.ArraySize = 1;
	depthTexDesc.Format = depthFormat;
	depthTexDesc.SampleDesc.Count = 1;
	depthTexDesc.SampleDesc.Quality = 0;
	depthTexDesc.Usage = D3D11_USAGE_DEFAULT;
	depthTexDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthTexDesc.CPUAccessFlags = 0;
	depthTexDesc.MiscFlags = 0;
	result = device->CreateTexture2D(&depthTexDesc, 0, &mDepthTexture);
	if(FAILED(result))
	{
		return false;
	}

	result = device->CreateDepthStencilView(mDepthTexture, 0, &mDepthStencilView);
	if(FAILED(result))
	{
		return false;
	}


	//Create ShaderResourceView for RenderTexture
	result = device->CreateShaderResourceView(mTexture, 0, &mTextureResource);
	if(FAILED(result))
	{
		return false;
	}

	return true;
}

bool RenderTexture::ApplyRenderTarget()
{
	if(mRenderTargetView == nullptr || mDepthStencilView == nullptr)
	{
		return false;
	}

	auto deviceContext = D3D::GetInstance()->GetDeviceContext();

	deviceContext->OMSetRenderTargets(1, &mRenderTargetView.p, mDepthStencilView);

	return true;
}

void RenderTexture::ClearRenderTarget(XMFLOAT4 color)
{
	if(mRenderTargetView == nullptr || mDepthStencilView == nullptr)
	{
		return;
	}
	float rgba[4];
	rgba[0] = color.x;
	rgba[1] = color.y;
	rgba[2] = color.z;
	rgba[3] = color.w;

	auto deviceContext = D3D::GetInstance()->GetDeviceContext();

	deviceContext->ClearRenderTargetView(mRenderTargetView, rgba);
	deviceContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}
#pragma endregion
