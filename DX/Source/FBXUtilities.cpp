#include "stdafx.h"
#include "FBXUtilities.h"

FbxAMatrix FBXUtil::GetGeometryTransformation(FbxNode* inNode)
{
	if(!inNode)
	{
		throw std::exception("Null for mesh geometry");
	}
	FbxAMatrix geometry;

	const FbxVector4 T = inNode->GetGeometricTranslation(FbxNode::eSourcePivot);
	const FbxVector4 R = inNode->GetGeometricRotation(FbxNode::eSourcePivot);
	const FbxVector4 S = inNode->GetGeometricScaling(FbxNode::eSourcePivot);

	geometry.SetT(T);
	geometry.SetR(R);
	geometry.SetS(S);

	return geometry;
}