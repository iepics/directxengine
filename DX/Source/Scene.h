#pragma once
#include <map>
#include "GameObject.h"

class Scene : public Object
{
	friend class Object;
	friend class GameObject;
	friend class Light;
public:
	Scene();
	~Scene();

	void Initialize();

	void UpdateAll();
	void RenderAll();

	void AddModel(std::shared_ptr<ModelData>& model);

private:
	void AddObject(const std::shared_ptr<GameObject>& object);
	void AddComponent(const std::shared_ptr<Component>& com);
	const std::shared_ptr<GameObject> Find(std::wstring name);

private:
	std::list<std::shared_ptr<GameObject>> mObjects;
	std::list<std::weak_ptr<Camera>> mCameras;
	std::list<std::weak_ptr<Light>> mLights;
	std::list<std::weak_ptr<Renderer>> mRenderers;
	std::list<std::weak_ptr<UIRenderer>> mUIRenderers;
	std::list<std::weak_ptr<Behaviour>> mBehaviours;

	std::shared_ptr<GameObject> mUICameraObj;
	std::weak_ptr<Camera> mUICamera;
};

