#pragma once
#include <atlmem.h>
#include <memory>
#include <mutex>

template<typename T>
class Singleton
{
public:
	static T* GetInstance()
	{
		/*if(m_pInstance == NULL)
		{
			static T instance;
			m_pInstance = &instance;
		}*/
		std::call_once(mOnceFlag, []{
			if (mInstance == nullptr)
			{
				mInstance = new T();
				atexit(Destroy);
			}
		});

		return mInstance;
	}
protected:
	Singleton()
	{
	}
	virtual ~Singleton()
	{
	}
private:
	static void Destroy(){ delete mInstance; }

	static T* mInstance;
	static std::once_flag mOnceFlag;
};
template<typename T> T* Singleton<T>::mInstance = nullptr;
template<typename T> std::once_flag Singleton<T>::mOnceFlag;