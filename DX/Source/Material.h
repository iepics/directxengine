#pragma once
#include <unordered_map>
#include "Texture.h"
#include "Effects.h"
using namespace DirectX;

class Effect;
class Material
{
public:
	Material(void);
	~Material(void);

	XMFLOAT4 GetColor(std::string name);
	std::shared_ptr<Texture> GetTexture(std::string name);

	void SetName(std::wstring name){ mName = name; }
	void SetFloat(std::string name, float value);
	void SetColor(std::string name, XMFLOAT4 color);
	void SetTexture(std::string name, std::shared_ptr<Texture> texture);
	void SetEffect(std::shared_ptr<Effect> effect);

	void SetMatBuffer();
	
	std::wstring GetName(){ return mName; }
	std::shared_ptr<Effect> GetEffect(){ return mEffect; }

private:
	std::wstring mName;
	XMFLOAT4 mDiffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	float mMetallic = 0.0f;
	float mRoughness = 0.5f;

	XMFLOAT4 mEmissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	std::weak_ptr<Texture> mDiffuseMap;
	Texture* mNormalMap = nullptr;

	std::shared_ptr<Effect> mEffect = nullptr;
};

