#include "stdafx.h"
#include "Font.h"


Font::Font()
{
}

Font::~Font()
{
}

bool Font::Initialize(std::wstring fontPath)
{
	FT_Error error;

	mScale = 1;
	mFontPath = fontPath;
	std::string str(fontPath.begin(), fontPath.end());

	GlyphBank* glyphBank = GlyphBank::GetInstance();
	error = FT_New_Face(glyphBank->GetFTL(), str.c_str(), 0, &mFace);
	if (error)
	{
		return false;
	}
	SetHeight(14);
	return true;
}

void Font::Destroy()
{
	FT_Done_Face(mFace);
}

void Font::SetHeight(int height)
{
	mHeight = height;

	FT_Set_Pixel_Sizes(mFace, FT_UInt(mHeight*mScale), FT_UInt(mHeight*mScale));
}