#pragma once
#include "Component.h"
using namespace DirectX;

class Transform : public Cloneable<Component, Transform>
{
public:
	Transform(void);
//	Transform(const Transform& other);
	virtual ~Transform(void);

	/*void SetPosition(const XMFLOAT3& position);
	void SetAngle(const XMFLOAT3& rotation);
	void SetRotation(const XMFLOAT4& quaternion);
	void SetScale(const XMFLOAT3& scale);
	XMFLOAT3 GetPosition() const;
	XMFLOAT3 GetAngle() const;
	XMFLOAT4 GetRotation() const;
	XMFLOAT3 GetScale() const;*/

	void SetLocalPosition(const XMFLOAT3& position);
	void SetLocalAngle(const XMFLOAT3& rotation);
	void SetLocalRotation(const XMFLOAT4& quaternion);
	void SetLocalScale(const XMFLOAT3& scale);
	XMFLOAT3 GetLocalPosition() const;
	XMFLOAT3 GetLocalAngle() const;
	XMFLOAT4 GetLocalRotation() const;
	XMFLOAT3 GetLocalScale() const;

	XMFLOAT3 GetRightVector() const;
	XMFLOAT3 GetUpVector() const;
	XMFLOAT3 GetForwardVector() const;

	virtual XMMATRIX GetWorldMatrix();
	XMMATRIX GetLocalMatrix();

	void Translate(XMFLOAT3 move);
	void Rotate(XMFLOAT3 rotate, bool self = true);


protected:
	void SetDirectionVector();
	virtual void SetLocal();
	virtual void SetWorld();

	//XMFLOAT3 mPosition;
	//XMFLOAT4 mRotation;
	//XMFLOAT3 mScale;
	//XMFLOAT3 mAngle;

	XMFLOAT3 mLocalPosition;
	XMFLOAT4 mLocalRotation;
	XMFLOAT3 mLocalScale;
	XMFLOAT3 mLocalAngle;

	XMFLOAT3 mRightVector;
	XMFLOAT3 mUpVector;
	XMFLOAT3 mForwardVector;

	XMFLOAT4X4 mWorldMatrix;
	XMFLOAT4X4 mLocalMatrix;
	XMFLOAT4X4 mParentMatrix;
};

