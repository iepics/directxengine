#include "stdafx.h"
#include "D3D.h"

D3D* D3D::instance = nullptr;
D3D::D3D(void)
{
	instance = this;
}

D3D::~D3D(void)
{
	instance = 0;
}

bool D3D::Initialize(HWND hWnd)
{
	HRESULT result;
	System* system = System::GetInstance();

	IDXGIFactory *factory;
	IDXGIAdapter *adapter;
	IDXGIOutput *adapterOutput;
	UINT numModes, numerator, denominator;
	DXGI_MODE_DESC *displayModeList;
	DXGI_ADAPTER_DESC adapterDesc;

	RECT rcClient;
	GetClientRect(hWnd, &rcClient);
	UINT width = rcClient.right - rcClient.left;
	UINT height = rcClient.bottom - rcClient.top;

	result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
	if(FAILED(result))
	{
		return false;
	}

	result = factory->EnumAdapters(0, &adapter);
	if(FAILED(result))
	{
		return false;
	}

	result = adapter->EnumOutputs(0, &adapterOutput);
	if(FAILED(result))
	{
		return false;
	}

	result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, 
		DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
	if(FAILED(result))
	{
		return false;
	}

	displayModeList = new DXGI_MODE_DESC[numModes];

	result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, 
		DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
	if(FAILED(result))
	{
		return false;
	}

	for(unsigned int i = 0; i<numModes; i++)
	{
		if(displayModeList[i].Width == width)
		{
			if(displayModeList[i].Height == height)
			{
				numerator = displayModeList[i].RefreshRate.Numerator;
				denominator = displayModeList[i].RefreshRate.Denominator;
			}
		}
	}

	result = adapter->GetDesc(&adapterDesc);	
	if(FAILED(result))
	{
		return false;
	}

	m_videoMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

	m_videoCardDescription = adapterDesc.Description;

	delete[] displayModeList;
	adapterOutput->Release();
	adapter->Release();
	factory->Release();


	//스왑 체인 생성
	D3D_DRIVER_TYPE d3dDriverTypes[] = {
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE
	};
	UINT nDriverTypes = sizeof(d3dDriverTypes) / sizeof(D3D_DRIVER_TYPE);

	D3D_FEATURE_LEVEL d3dFeatureLevels[] = {
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT nFeatureLevels = sizeof(d3dFeatureLevels) / sizeof(D3D_FEATURE_LEVEL);

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	// 스왑 체인 description을 초기화합니다.
	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc)); 
	// 하나의 백버퍼만을 사용하도록 합니다.
	swapChainDesc.BufferCount = 1; 
	// 백버퍼의 너비와 높이를 설정합니다.
	swapChainDesc.BufferDesc.Width = width;
	swapChainDesc.BufferDesc.Height = height; 
	// 백버퍼로 일반적인 32bit의 서페이스를 지정합니다.
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	// 백버퍼의 새로고침 비율을 설정합니다.
	if (system->IsVsync())
	{
		swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
	}
	else
	{
		swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	} 
	// 백버퍼의 용도를 설정합니다.
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; 
	// 렌더링이 이루어질 윈도우의 핸들을 설정합니다.
	swapChainDesc.OutputWindow = hWnd; 
	// 멀티샘플링을 끕니다.
	swapChainDesc.SampleDesc.Count = 4;
	swapChainDesc.SampleDesc.Quality = 0; 
	// 윈도우 모드 또는 풀스크린 모드를 설정합니다.
	if (system->IsFullscreen())
	{
		swapChainDesc.Windowed = false;
	}
	else
	{
		swapChainDesc.Windowed = true;
	} 
	// 스캔라인의 정렬과 스캔라이닝을 지정되지 않음으로(unspecified) 설정합니다.
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED; 
	// 출력된 이후의 백버퍼의 내용을 버리도록 합니다.
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD; 
	// 추가 옵션 플래그를 사용하지 않습니다.
	swapChainDesc.Flags = 0;

	D3D_DRIVER_TYPE nd3dDriverType = D3D_DRIVER_TYPE_NULL;
	D3D_FEATURE_LEVEL nd3dFeatureLevel = D3D_FEATURE_LEVEL_11_0;
	for(UINT i = 0; i < nDriverTypes; i++)
	{
		nd3dDriverType = d3dDriverTypes[i];
		result = D3D11CreateDeviceAndSwapChain(NULL, 
			nd3dDriverType, NULL, 0, d3dFeatureLevels, 
			nFeatureLevels, D3D11_SDK_VERSION, &swapChainDesc, 
			&m_pdxgiSwapChain, &m_pd3dDevice, NULL, &m_pd3dDeviceContext);
		if(SUCCEEDED(result))
			break;
	}

	if(!CreateView())
	{
		return false;
	}

	if(!CreateRasterState())
	{
		return false;
	}

	if (!RenderStates::InitAll())
	{
		return false;
	}
	
	return true;
}

bool D3D::CreateView()
{
	System* system = System::GetInstance();
	HRESULT result;

	CComPtr<ID3D11Texture2D> backBuffer;
	result = m_pdxgiSwapChain->GetBuffer(0, _uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer.p);
	if(FAILED(result))
	{
		return false;
	}
	
	result = m_pd3dDevice->CreateRenderTargetView(backBuffer, 0, &m_pd3dRenderTargetView);
	if(FAILED(result))
	{
		return false;
	}


	//Create depth stencil texture
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));
	depthBufferDesc.Width = system->GetClientWidth();
	depthBufferDesc.Height = system->GetClientHeight();
    depthBufferDesc.MipLevels = 1;
    depthBufferDesc.ArraySize = 1;
    depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 4;
    depthBufferDesc.SampleDesc.Quality = 0;
    depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    depthBufferDesc.CPUAccessFlags = 0;
    depthBufferDesc.MiscFlags = 0;
	result = m_pd3dDevice->CreateTexture2D(&depthBufferDesc, 0, &m_pd3dDepthStencilBuffer.p);
	if(FAILED(result))
	{
		return false;
	}

    D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xff;
	depthStencilDesc.StencilWriteMask = 0xff;
	// Stencil operations if pixel is front-facing.
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS; 
	// Stencil operations if pixel is back-facing.
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	//depthStencilDesc.DepthEnable = true;
	//depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	//depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	//depthStencilDesc.StencilEnable = false;
	//depthStencilDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
	//depthStencilDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
	//// Stencil operations if pixel is front-facing.
	//depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	//depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	//depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	//depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS; 
	//// Stencil operations if pixel is back-facing.
	//depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	//depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	//depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	//depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	result = m_pd3dDevice->CreateDepthStencilState(&depthStencilDesc, &m_pd3dDepthStencilState.p);
	if(FAILED(result))
	{
		return false;
	}
	m_pd3dDeviceContext->OMSetDepthStencilState(m_pd3dDepthStencilState, 1);


	result = m_pd3dDevice->CreateDepthStencilView(m_pd3dDepthStencilBuffer, 0, &m_pd3dDepthStencilView.p);
	if(FAILED(result))
	{
		return false;
	}
	m_pd3dDeviceContext->OMSetRenderTargets(1, &m_pd3dRenderTargetView.p, m_pd3dDepthStencilView);

	
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
	depthStencilDesc.DepthEnable = false;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xff;
	depthStencilDesc.StencilWriteMask = 0xff;
	// Stencil operations if pixel is front-facing.
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS; 
	// Stencil operations if pixel is back-facing.
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	result = m_pd3dDevice->CreateDepthStencilState(&depthStencilDesc, &m_pd3dDepthStencilDisableState.p);
	if(FAILED(result))
	{
		return false;
	}

	return true;
}

bool D3D::CreateRasterState()
{
	HRESULT result;

	D3D11_RASTERIZER_DESC rasterDesc;
	// 어떤 도형을 어떻게 그릴 것인지 결정하는 래스터화기 description을 작성합니다.
	rasterDesc.AntialiasedLineEnable = true;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = true;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;
 
	// 작성한 description으로부터 래스터화기 상태를 생성합니다.
	result = m_pd3dDevice->CreateRasterizerState(&rasterDesc, &m_pd3dRasterState.p);
	if(FAILED(result))
	{
		return false;
	}
 
	// 래스터화기 상태를 설정합니다.
	m_pd3dDeviceContext->RSSetState(m_pd3dRasterState);

	return true;
}

void D3D::Destroy()
{
	if(m_pdxgiSwapChain)
	{
		m_pdxgiSwapChain->SetFullscreenState(false, NULL);
	}

	RenderStates::DestroyAll();
}

bool D3D::ApplyRenderTarget()
{
	if(m_pd3dRenderTargetView == nullptr || m_pd3dDepthStencilView == nullptr)
	{
		return false;
	}
	
	m_pd3dDeviceContext->OMSetRenderTargets(1, &m_pd3dRenderTargetView.p, m_pd3dDepthStencilView);

	return true;
}

void D3D::BeginScene(const XMFLOAT4& color)
{
	float colorArray[4];
	colorArray[0] = color.x;
	colorArray[1] = color.y;
	colorArray[2] = color.z;
	colorArray[3] = color.w;
	
	m_pd3dDeviceContext->ClearRenderTargetView(m_pd3dRenderTargetView, colorArray);
	m_pd3dDeviceContext->ClearDepthStencilView(m_pd3dDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void D3D::EndScene()
{
	if(System::GetInstance()->IsVsync())
	{
		m_pdxgiSwapChain->Present(1, 0);
	}
	else
	{
		m_pdxgiSwapChain->Present(0, 0);
	}
}

CComPtr<ID3D11Device> D3D::GetDevice()
{
	return m_pd3dDevice;
}

CComPtr<ID3D11DeviceContext> D3D::GetDeviceContext()
{
	return m_pd3dDeviceContext;
}

void D3D::GetVideoCardInfo(std::wstring &cardName, int &memory)
{
	cardName = m_videoCardDescription;
	memory = m_videoMemory;

	printf("%s  %dMB\n", m_videoCardDescription.c_str(), m_videoMemory);
}



void D3D::TurnZBufferOn()
{
	m_pd3dDeviceContext->OMSetDepthStencilState(m_pd3dDepthStencilState, 1);
}
void D3D::TurnZBufferOff()
{
	m_pd3dDeviceContext->OMSetDepthStencilState(m_pd3dDepthStencilDisableState, 1);
}