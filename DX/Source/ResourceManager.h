#pragma once
#include "Singleton.h"
#include "FBXLoader.h"
#include "GameObject.h"
#include "Material.h"
#include "Effects.h"
#include "Font.h"

class ResourceManager : public Singleton<ResourceManager>
{
	friend class Singleton<ResourceManager>;
public:

	std::shared_ptr<ModelData> GetModel(std::wstring name);
	std::shared_ptr<Material> GetMaterial(std::wstring name);
	std::shared_ptr<Effect> GetEffect(std::wstring name);
	std::shared_ptr<Font> GetFont(std::wstring name);
	std::shared_ptr<Texture> GetTexture(std::wstring name);

	void AddModel(std::wstring name, std::shared_ptr<ModelData>& model);
	void AddMaterial(std::wstring name, std::shared_ptr<Material>& material);
	void AddEffect(std::wstring name, std::shared_ptr<Effect>& effect);
	void AddFont(std::wstring name, std::shared_ptr<Font>& font);
	void AddTexture(std::wstring name, std::shared_ptr<Texture>& texture);

protected:
	ResourceManager(){}

private:
	~ResourceManager(){}

	std::map<std::wstring, std::shared_ptr<ModelData>> mModelMap;
	std::map<std::wstring, std::shared_ptr<Material>> mMaterialMap;
	std::map<std::wstring, std::shared_ptr<Effect>> mEffectMap;
	std::map<std::wstring, std::shared_ptr<Font>> mFontMap;
	std::map<std::wstring, std::shared_ptr<Texture>> mTextureMap;
};

