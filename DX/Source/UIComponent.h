#pragma once
#include "GameObject.h"
#include "Effects.h"

class UIComponent : public Cloneable<Component, UIComponent>
{
public:

	UIComponent() = default;
	virtual ~UIComponent() = default;

	//void SetWidth(float width){ mWidth = width; }
	//float GetWidth(){ return mWidth; }

	//void SetHeight(float height){ mHeight = height; }
	//float GetHeight(){ return mHeight; }

	void SetPivot(XMFLOAT2 pivot){ mPivot = pivot; }
	XMFLOAT2 GetPivot(){ return mPivot; }


protected:
	//float mWidth = 100;
	//float mHeight = 30;	

	XMFLOAT2 mPivot;
};

