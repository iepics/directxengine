#include "stdafx.h"
#include "Material.h"


Material::Material(void)
{
}

Material::~Material(void)
{
}

XMFLOAT4 Material::GetColor(std::string name)
{
	return XMFLOAT4(0, 0, 0, 0);
}

std::shared_ptr<Texture> Material::GetTexture(std::string name)
{
	std::shared_ptr<Texture> ret = nullptr;
	if (name == "_diffuse")
	{
		ret = mDiffuseMap.lock();
	}
	else if (name == "_normal")
	{
		//ret = mNormalMap.lock();
	}
	return ret;
}

void Material::SetFloat(std::string name, float value)
{
	if(name == "_metallic")
	{
		mMetallic = value;
	}
	else if(name == "_roughness")
	{
		mRoughness = value;
	}
}
void Material::SetColor(std::string name, XMFLOAT4 color)
{
	if(name == "_diffuse")
	{
		mDiffuse = color;
	}
	else if(name == "_emissive")
	{
		mEmissive = color;
	}
}
void Material::SetTexture(std::string name, std::shared_ptr<Texture> texture)
{
	if(name == "_diffuse")
	{
		mDiffuseMap = texture;
	}
	else if(name == "_normal")
	{
		//mNormalMap = texture;
	}
}
void Material::SetEffect(std::shared_ptr<Effect> effect)
{
	mEffect = effect;
}

void Material::SetMatBuffer()
{
	auto buffer = ShaderBuffer::GetInstance();

	MaterialBuffer material;
	material.Diffuse = mDiffuse;
	material.Metallic = mMetallic;
	material.Roughness = mRoughness;
	material.Emissive = mEmissive;

	buffer->MatBuffer = material;
}