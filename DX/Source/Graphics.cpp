#include "stdafx.h"
#include "Graphics.h"
#include "FBXLoader.h"

#include "Test.h"
#include "CameraTest.h"
#include "LightTest.h"

Graphics::Graphics(void)
{
	m_D3D = 0;
}

Graphics::~Graphics(void) 
{
}

bool Graphics::Initialize(HWND hWnd)
{
	bool result;

	m_D3D = new D3D();
	result = m_D3D->Initialize(hWnd);
	if(!result)
	{
		MessageBox(hWnd, _T("Could not initialize Direct3D."), _T("Error"), MB_OK);
		return false;
	}

	GlyphBank::GetInstance()->Initialize(1024, 1024);

	auto resourceMng = ResourceManager::GetInstance();
	auto white = std::make_shared<Texture2D>();
	white->LoadFile(L"Resource/Texture/white.bmp");
	resourceMng->AddTexture(L"whiteTexture", std::static_pointer_cast<Texture, Texture2D>(white));

	auto black = std::make_shared<Texture2D>();
	black->LoadFile(L"Resource/Texture/black.bmp");
	resourceMng->AddTexture(L"blackTexture", std::static_pointer_cast<Texture, Texture2D>(black));

	auto cat = std::make_shared<Texture2D>();
	cat->LoadFile(L"Resource/Texture/cat.png");
	resourceMng->AddTexture(L"catImage", std::static_pointer_cast<Texture, Texture2D>(cat));
	
	SceneManager::GetInstance()->LoadScene(L"scene");
	mCurrentScene = SceneManager::GetInstance()->GetCurrentScene();


	//auto textObj = EditorUtility::CreateText(L"Text UI test\n텍스트 출력 테스트");
	//textObj->GetComponent<UITransform>()->SetPivot(XMFLOAT2(0, 1));
	//textObj->GetTransform()->SetLocalPosition(XMFLOAT3(-400, 300, 0));
	//textObj->GetComponent<TextUI>()->SetColor(XMFLOAT4(1, 0, 0, 1));
	//Object::Instantiate(textObj);

	//auto textObj2 = EditorUtility::CreateText(L"Text UI test\n텍스트 출력 테스트");
	//textObj2->GetComponent<UITransform>()->SetPivot(XMFLOAT2(0, 1));
	//textObj2->GetTransform()->SetLocalPosition(XMFLOAT3(-400, 220, 0));
	//textObj2->GetComponent<TextUI>()->SetColor(XMFLOAT4(1, 1, 0, 1));
	//Object::Instantiate(textObj2);

	//auto imageObj = EditorUtility::CreateImage(nullptr);
	//imageObj->GetComponent<UITransform>()->SetPivot(XMFLOAT2(1, 1));
	//imageObj->GetComponent<UITransform>()->SetWidth(130);
	//imageObj->GetComponent<UITransform>()->SetHeight(130);
	//imageObj->GetTransform()->SetLocalPosition(XMFLOAT3(400, 300, 0));
	//imageObj->GetComponent<ImageUI>()->SetImage(cat);
	//Object::Instantiate(imageObj);

	FBXLoader fbxLoader;
	fbxLoader.Initialize();
	fbxLoader.LoadScene(L"Resource/Model/attack01.fbx", FBXLoader::Direct);
	fbxLoader.LoadScene(L"Resource/Model/plane.fbx", FBXLoader::Direct);
	fbxLoader.LoadScene(L"Resource/Model/NightWingAS/NW.fbx", FBXLoader::Direct);
	fbxLoader.CleanupFbxManager();

	auto model = ResourceManager::GetInstance()->GetModel(L"attack01");
	auto attack01 = std::static_pointer_cast<GameObject, Object>(Object::Instantiate(model->gameObject));
	attack01->GetTransform()->SetLocalPosition(XMFLOAT3(30, 0, 0));
	attack01->GetTransform()->SetLocalScale(XMFLOAT3(1, 1, 1));
//	attack01->GetTransform()->SetLocalAngle(XMFLOAT3(0, 90, 0));
	attack01->AddComponent<Test>();

	auto model2 = ResourceManager::GetInstance()->GetModel(L"plane");
	auto plane = std::static_pointer_cast<GameObject, Object>(Object::Instantiate(model2->gameObject));
	plane->GetTransform()->SetLocalPosition(XMFLOAT3(0, 0, 0));
	plane->GetTransform()->SetLocalScale(XMFLOAT3(300, 300, 300));

	//auto model3 = ResourceManager::GetInstance()->GetModel(L"NW");
	//auto nightwing = std::static_pointer_cast<GameObject, Object>(Object::Instantiate(model3->gameObject));
	//nightwing->GetTransform()->SetLocalPosition(XMFLOAT3(-30, 0, -20));
	//nightwing->GetTransform()->SetLocalScale(XMFLOAT3(7, 7, 7));
//	nightwing->GetTransform()->SetLocalAngle(XMFLOAT3(0, 180, 0));
	
	auto cameraObject = EditorUtility::CreateEmpty();
	cameraObject->InitComponent<CameraTest>();
	auto camera = cameraObject->InitComponent<Camera>();
	cameraObject->GetTransform()->SetLocalPosition(XMFLOAT3(0.0f, 30.0f, 150.0f));
	cameraObject->GetTransform()->SetLocalAngle(XMFLOAT3(0, 180, 0));
	camera->SetViewRect(XMFLOAT4(0, 0, 1, 1));
	Object::Instantiate(cameraObject);

	auto light1 = EditorUtility::CreateLight(LightType::Direction);
	light1->InitComponent<LightTest>();
	light1->GetComponent<Light>()->SetShadowType(ShadowType::Hard);
	light1->GetTransform()->SetLocalAngle(XMFLOAT3(45, 160, 0));
	Object::Instantiate(light1);

	return true;
}
void Graphics::Destroy()
{
	if(m_D3D)
	{
		m_D3D->Destroy();
		delete m_D3D;
	}
}
void Graphics::Frame()
{
	Update();
	Render();
}

void Graphics::Render()
{
	m_D3D->BeginScene(XMFLOAT4(0, 0.3f, 0.3f, 1.0f));

	mCurrentScene->RenderAll();

	m_D3D->EndScene();
}

void Graphics::Update()
{
	mCurrentScene->UpdateAll();
}