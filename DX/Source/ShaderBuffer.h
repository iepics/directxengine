#pragma once
#include <vector>
#include "Singleton.h"
using namespace DirectX;

struct ObjectBuffer
{
	XMMATRIX World;
};

struct CameraBuffer
{
	XMMATRIX View;
	XMMATRIX Projection;
};

struct LightBuffer
{
	LightBuffer(){ ZeroMemory(this, sizeof(this)); }

	XMFLOAT4 Color;
	XMFLOAT4 Position;
	float Range;

	XMFLOAT4 LightColor[4];
	XMFLOAT4 LightPos[4];
	float LightRange[4];
};

struct MaterialBuffer
{
	XMFLOAT4 Diffuse;
	float Metallic;
	float Roughness;
	XMFLOAT4 Emissive;
};

struct BoneBuffer
{
	XMFLOAT4X4 BoneTransforms[96];
};

class ShaderBuffer : public Singleton<ShaderBuffer>
{
public:
	ShaderBuffer(void);
	~ShaderBuffer(void);

	//bool CreateBuffer();
	//bool SetObjectBuffer(ObjectBuffer &buffer);
	//bool SetCameraBuffer(CameraBuffer &buffer);
	//bool SetLightBuffer(LightBuffer &lightBuffer);
	//bool SetBoneBuffer(std::vector<XMFLOAT4X4> &boneTransforms);

	void SetWorld(XMMATRIX matrix){ XMStoreFloat4x4(&mWorld, matrix); }
	XMMATRIX GetWorld(){ return XMLoadFloat4x4(&mWorld); }

	void SetView(XMMATRIX matrix){ XMStoreFloat4x4(&mView, matrix); }
	XMMATRIX GetView(){ return XMLoadFloat4x4(&mView); }

	void SetProjection(XMMATRIX matrix){ XMStoreFloat4x4(&mProjection, matrix); }
	XMMATRIX GetProjection(){ return XMLoadFloat4x4(&mProjection); }
	
	XMFLOAT4 Color;
	XMFLOAT4 Position;
	float Range;

	XMFLOAT4X4 LightView;
	XMFLOAT4X4 LightProj;

	MaterialBuffer MatBuffer;

	XMFLOAT3 CameraPos;
private:
	XMFLOAT4X4 mWorld;
	XMFLOAT4X4 mView;
	XMFLOAT4X4 mProjection;
};

