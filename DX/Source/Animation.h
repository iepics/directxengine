#pragma once
#include "Object.h"


#pragma region Keyframe
struct Keyframetemp
{
	float time;
	float value;
	float leftTangent;
	float rightTangent;
};
#pragma endregion

#pragma region AnimationCurve
class AnimationCurve
{
public:
	AnimationCurve();
	~AnimationCurve();

	void AddKey(Keyframetemp key);
	float Evaluate(float time);

private:
	std::vector<Keyframetemp> mKeys;
};
#pragma endregion

class AnimCurveInfo
{
public:
	enum Type
	{
		Transform,
	};

	void SetType(Type type){ mType = type; }
	void SetProperty(std::string prop){ mProperty = prop; }
	void SetBoneName(std::wstring name){ mBoneName = name; }
	void SetBoneIndex(int index){ mBoneIndex = index; }
	void SetCurve(std::shared_ptr<AnimationCurve> curve){ mCurve = curve; }

	Type GetType(){ return mType; }
	const std::string& GetProperty(){ return mProperty; }
	const std::wstring& GetBoneName(){ return mBoneName; }
	int GetBoneIndex(){ return mBoneIndex; }

	float Evaluate(float time)
	{
		if(!mCurve)
		{
			return 0;
		}
		return mCurve->Evaluate(time);
	}

protected:
	Type mType;
	std::string mProperty;
	std::wstring mBoneName;
	int mBoneIndex;
	std::shared_ptr<AnimationCurve> mCurve;
};


#pragma region AnimationClip
/*
Keyframe 애니메이션을 위한 정보를 저장
*/
class AnimationClip : public Object
{
public:
	AnimationClip();
	~AnimationClip();

	void SetCurve(std::shared_ptr<AnimCurveInfo>& curveInfo);
	void SetFrameRate(float frameRate){ mFrameRate = frameRate; }
	void SetLength(float length){ mLength = length; }

	const std::vector<std::shared_ptr<AnimCurveInfo>>& GetCurves(){ return mCurveInfos; }
	float GetFrameRate(){ return mFrameRate; }
	float GetLength(){ return mLength; }

private:
	float mFrameRate;
	float mLength;
	std::vector<std::shared_ptr<AnimCurveInfo>> mCurveInfos;
};
#pragma endregion

//#pragma region AnimationState
//class AnimationState
//{
//public:
//
//private:
//
//};
//#pragma endregion

#pragma region Animation
/*
AnimationClip을 바탕으로 Animation을 처리
*/
class Animation : public Cloneable<Component, Animation>
{
public:
	Animation();
	~Animation();
	
	virtual void OnLoad();
		
	void Play(std::wstring const& name);
	void Play(unsigned int index);

	void AddClip(std::shared_ptr<AnimationClip>& clip);

	bool IsPlaying(){ return mIsPlaying; }

private:
	bool mIsPlaying = false;
	float mCurrentTime = 0.0f;
	std::map<std::wstring, std::shared_ptr<AnimationClip>> mAnimations;

	std::map<std::wstring, std::weak_ptr<Transform>> mBoneTransforms;
};
#pragma endregion
