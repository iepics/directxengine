#pragma once
#include "GameTimer.h"
#include "Input.h"
#include "Behaviour.h"
#include "MathUtilities.h"

class CameraTest : public Cloneable<Behaviour, CameraTest>
{
public:
	void Update()
	{
		auto transform = GetGameObject()->GetTransform();
		auto right = transform->GetRightVector();
		auto forward = transform->GetForwardVector();

		if(Input::GetInstance()->GetKeyDown(KeyCode::LeftArrow))
		{
			transform->Translate(right * 60 * -GameTimer::GetInstance()->GetDeltaTime());
		}
		if(Input::GetInstance()->GetKeyDown(KeyCode::RightArrow))
		{
			transform->Translate(right * 60 * GameTimer::GetInstance()->GetDeltaTime());
		}
		if(Input::GetInstance()->GetKeyDown(KeyCode::UpArrow))
		{
			GetGameObject()->GetTransform()->Translate(forward * 60 * GameTimer::GetInstance()->GetDeltaTime());
		}
		if(Input::GetInstance()->GetKeyDown(KeyCode::DownArrow))
		{
			GetGameObject()->GetTransform()->Translate(forward * -60 * GameTimer::GetInstance()->GetDeltaTime());
		}
		if(Input::GetInstance()->GetKeyDown(KeyCode::W))
		{
			GetGameObject()->GetTransform()->Rotate(XMFLOAT3(-60, 0, 0) * GameTimer::GetInstance()->GetDeltaTime());
		}
		if(Input::GetInstance()->GetKeyDown(KeyCode::S))
		{
			GetGameObject()->GetTransform()->Rotate(XMFLOAT3(60, 0, 0) * GameTimer::GetInstance()->GetDeltaTime());
		}
		if(Input::GetInstance()->GetKeyDown(KeyCode::A))
		{
			GetGameObject()->GetTransform()->Rotate(XMFLOAT3(0, -60, 0) * GameTimer::GetInstance()->GetDeltaTime(), false);
		}
		if(Input::GetInstance()->GetKeyDown(KeyCode::D))
		{
			GetGameObject()->GetTransform()->Rotate(XMFLOAT3(0, 60, 0) * GameTimer::GetInstance()->GetDeltaTime(), false);
		}
	}
};

