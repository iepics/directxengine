#include "stdafx.h"
#include "Camera.h"

std::weak_ptr<Camera> Camera::mainCamera;
Camera::Camera()
{
	mFieldOfView = 60;
	mClipping.x = 0.1f;
	mClipping.y = 1000.0f;
	mViewRect.x = 0;
	mViewRect.y = 0;
	mViewRect.z = 1;
	mViewRect.w = 1;

	XMStoreFloat4x4(&mViewMatrix, XMMatrixIdentity());
	XMStoreFloat4x4(&mProjectionMatrix, XMMatrixIdentity());
}

Camera::~Camera()
{
}

void Camera::SetViewport()
{
	float width;
	float height;

	auto dc = D3D::GetInstance()->GetDeviceContext();
	if(mRenderTexture.expired() == false)
	{
		auto rt = mRenderTexture.lock();
		rt->ClearRenderTarget(mClearColor);
		rt->ApplyRenderTarget();
		width = (float)rt->GetWidth();
		height = (float)rt->GetHeight();
	}
	else
	{
		D3D::GetInstance()->ApplyRenderTarget();
		System* system = System::GetInstance();
		width = (float)system->GetClientWidth();
		height = (float)system->GetClientHeight();

		/*mRenderTarget->ClearRenderTarget(XMFLOAT4(1, 1, 0, 1));
		mRenderTarget->ApplyRenderTarget();
		width = mRenderTarget->GetWidth();
		height = mRenderTarget->GetHeight();*/
	}

	float x = mViewRect.x * width;
	float y = mViewRect.y * height;
	float w = mViewRect.z * width;
	float h = mViewRect.w * height;

	D3D11_VIEWPORT viewport;

	viewport.TopLeftX = x;
	viewport.TopLeftY = y;
	viewport.Width = w;
	viewport.Height = h;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	
	auto deviceContext = D3D::GetInstance()->GetDeviceContext();
	
	deviceContext->RSSetViewports(1, &viewport);
}

void Camera::UpdateMatrix()
{
	System* system = System::GetInstance();
	auto transform = GetGameObject()->GetTransform();

	XMVECTOR up, forward, position;
	up = XMLoadFloat3(&transform->GetUpVector());
	forward = XMLoadFloat3(&transform->GetForwardVector());
	position = XMLoadFloat3(&transform->GetLocalPosition());
	XMVECTOR lookat = position + forward;
	
	XMMATRIX viewMat = XMMatrixLookAtLH(position, lookat, up);
	XMMATRIX projectionMat;
	if(mProjType == Perspective)
	{
		float screenAspect = (float)system->GetClientWidth() / (float)system->GetClientHeight();
		projectionMat = XMMatrixPerspectiveFovLH(XMConvertToRadians(mFieldOfView), screenAspect, mClipping.x, mClipping.y);
	}
	else if(mProjType == Orthographic)
	{
		projectionMat = XMMatrixOrthographicLH(mWidth, mHeight, 0, mDepth);
	}

	XMStoreFloat4x4(&mViewMatrix, viewMat);
	XMStoreFloat4x4(&mProjectionMatrix, projectionMat);
}

void Camera::SetCameraBuffer()
{
	auto transform = GetGameObject()->GetTransform();

	ShaderBuffer* buffer = ShaderBuffer::GetInstance();

	buffer->SetView(XMLoadFloat4x4(&mViewMatrix));
	buffer->SetProjection(XMLoadFloat4x4(&mProjectionMatrix));
	buffer->CameraPos = transform->GetLocalPosition();
}