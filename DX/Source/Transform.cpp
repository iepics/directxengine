#include "stdafx.h"
#include "Transform.h"

using std::shared_ptr;

Transform::Transform(void)
{
	mLocalPosition = XMFLOAT3(0.0f, 0.0f, 0.0f);
	mLocalRotation = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	mLocalScale = XMFLOAT3(1.0f, 1.0f, 1.0f);
	mLocalAngle = XMFLOAT3(0.0f, 0.0f, 0.0f);

	mRightVector = XMFLOAT3(1.0f, 0.0f, 0.0f);
	mUpVector = XMFLOAT3(0.0f, 1.0f, 0.0f);
	mForwardVector = XMFLOAT3(0.0f, 0.0f, 1.0f);

	/////matrix initialize
	//XMVECTOR pos = XMLoadFloat3(&mLocalPosition);
	//XMVECTOR quat = XMLoadFloat4(&mLocalRotation);
	//XMVECTOR scale = XMLoadFloat3(&mLocalScale);
	//XMMATRIX localMat = XMMatrixAffineTransformation(scale, XMVectorZero(), quat, pos);
	//XMStoreFloat4x4(&mLocalMatrix, localMat);

	//auto worldMat = localMat;
	//XMStoreFloat4x4(&mWorldMatrix, worldMat);

	//XMMatrixDecompose(&scale, &quat, &pos, worldMat);
	//XMStoreFloat3(&mPosition, pos);
	//XMStoreFloat4(&mRotation, quat);
	//XMStoreFloat3(&mScale, scale);
	//XMFLOAT3 angle;
	//angle = MathUtility::ToEulerAngles(mRotation);
	//mAngle = angle;
}

Transform::~Transform(void)
{
}

void Transform::SetLocalPosition(const XMFLOAT3& position)
{
	mLocalPosition = position;
	SetLocal();
}
void Transform::SetLocalAngle(const XMFLOAT3& angle)
{
	float x = XMConvertToRadians(angle.x);
	float y = XMConvertToRadians(angle.y);
	float z = XMConvertToRadians(angle.z);
	XMVECTOR quater;
	XMVECTOR xQuater = XMQuaternionRotationAxis(XMVectorSet(1, 0, 0, 0), x);
	XMVECTOR yQuater = XMQuaternionRotationAxis(XMVectorSet(0, 1, 0, 0), y);
	XMVECTOR zQuater = XMQuaternionRotationAxis(XMVectorSet(0, 0, 1, 0), z);
	quater = XMQuaternionMultiply(xQuater, yQuater);
	quater = XMQuaternionMultiply(quater, zQuater);

	XMFLOAT4 quaterFloat4;
	XMStoreFloat4(&quaterFloat4, quater);
	SetLocalRotation(quaterFloat4);
}
void Transform::SetLocalRotation(const XMFLOAT4& q)
{
	XMFLOAT3 angle;
	angle = MathUtility::ToEulerAngles(q);
	mLocalAngle.x = angle.x;
	mLocalAngle.y = angle.y;
	mLocalAngle.z = angle.z;
	mLocalRotation = q;

	SetLocal();
	SetDirectionVector();
}
void Transform::SetLocalScale(const XMFLOAT3& scale)
{
	mLocalScale = scale;
	SetLocal();
}
XMFLOAT3 Transform::GetLocalPosition() const
{
	return mLocalPosition;
}
XMFLOAT3 Transform::GetLocalAngle() const
{
	return mLocalAngle;
}
XMFLOAT4 Transform::GetLocalRotation() const
{
	return mLocalRotation;
}
XMFLOAT3 Transform::GetLocalScale() const
{
	return mLocalScale;
}

//void Transform::SetPosition(const XMFLOAT3& position)
//{
//	mPosition = position;
//	SetWorld();
//}
//void Transform::SetAngle(const XMFLOAT3& angle)
//{
//	float x = XMConvertToRadians(angle.x);
//	float y = XMConvertToRadians(angle.y);
//	float z = XMConvertToRadians(angle.z);
//	XMVECTOR quater;
//	XMVECTOR xQuater = XMQuaternionRotationAxis(XMVectorSet(1, 0, 0, 0), x);
//	XMVECTOR yQuater = XMQuaternionRotationAxis(XMVectorSet(0, 1, 0, 0), y);
//	XMVECTOR zQuater = XMQuaternionRotationAxis(XMVectorSet(0, 0, 1, 0), z);
//	quater = XMQuaternionMultiply(xQuater, yQuater);
//	quater = XMQuaternionMultiply(quater, zQuater);
//
//	XMFLOAT4 quaterFloat4;
//	XMStoreFloat4(&quaterFloat4, quater);
//	SetRotation(quaterFloat4);
//}
//void Transform::SetRotation(const XMFLOAT4& q)
//{
//	XMFLOAT3 angle;
//	angle = MathUtility::ToEulerAngles(q);
//	mAngle.x = angle.x;
//	mAngle.y = angle.y;
//	mAngle.z = angle.z;
//	mRotation = q;
//	
//	SetWorld();
//	SetDirectionVector();
//}
//
//void Transform::SetScale(const XMFLOAT3& scale)
//{
//	mScale = scale;
//	SetWorld();
//}
//
//XMFLOAT3 Transform::GetPosition() const
//{
//	return mPosition;
//}
//
//XMFLOAT3 Transform::GetAngle() const
//{
//	return mAngle;
//}
//XMFLOAT4 Transform::GetRotation() const
//{	
//	//auto rotation = XMLoadFloat4(&mLocalRotation);
//
//	//auto parentObj = GetGameObject()->GetParent();
//	//if (parentObj != nullptr)
//	//{
//	//	auto parentLocalRot = XMLoadFloat4(&parentObj->GetTransform()->GetRotation());
//	//	rotation = parentLocalRot * rotation;
//	//}
//
//	//XMFLOAT4 retValue;
//	//XMStoreFloat4(&retValue, rotation);
//
//	//return retValue;
//	return mRotation;
//}
//XMFLOAT3 Transform::GetScale() const
//{
//	return mScale;
//}

void Transform::SetDirectionVector()
{
	auto worldMat = GetWorldMatrix();
	XMVECTOR scale, quat, pos;
	XMMatrixDecompose(&scale, &quat, &pos, worldMat);

//	XMVECTOR quater = XMLoadFloat4(&this->GetRotation());

	//회전을 right, up, forward vector에 적용
	XMMATRIX rotateMat = XMMatrixRotationQuaternion(quat);
	XMVECTOR right = XMVectorSet(1, 0, 0, 0);
	XMVECTOR up = XMVectorSet(0, 1, 0, 0);
	XMVECTOR forward = XMVectorSet(0, 0, 1, 0);
	right = XMVector3Transform(right, rotateMat);
	up = XMVector3Transform(up, rotateMat);
	forward = XMVector3Transform(forward, rotateMat);

	right = XMVector3Normalize(right);
	up = XMVector3Normalize(up);
	forward = XMVector3Normalize(forward);

	XMStoreFloat3(&mRightVector, right);
	XMStoreFloat3(&mUpVector, up);
	XMStoreFloat3(&mForwardVector, forward);
}

XMFLOAT3 Transform::GetRightVector() const
{
	return mRightVector;
}
XMFLOAT3 Transform::GetUpVector() const
{
	return mUpVector;
}
XMFLOAT3 Transform::GetForwardVector() const
{
	return mForwardVector;
}

void Transform::SetLocal()
{
//	XMVECTOR pos = XMLoadFloat3(&GetLocalPosition());
//	XMVECTOR quat = XMLoadFloat4(&GetLocalRotation());
//	XMVECTOR scale = XMLoadFloat3(&GetLocalScale());
//	XMMATRIX localMat = XMMatrixAffineTransformation(scale, XMVectorZero(), quat, pos);
//	XMStoreFloat4x4(&mLocalMatrix, localMat);
//
//	auto worldMat = localMat;
//	/*if(mParent.lock() != nullptr)
//	{
//		auto parentWorldMat = mParent.lock()->GetWorldMatrix();
//		worldMat = localMat * parentWorldMat;
//	}*/
//	auto parentObj = GetGameObject()->GetParent();
//	if (parentObj != nullptr)
//	{
//		auto parentWorldMat = parentObj->GetTransform()->GetWorldMatrix();
//		worldMat = localMat * parentWorldMat;
//	}
//	XMStoreFloat4x4(&mWorldMatrix, worldMat);
//	
//	XMMatrixDecompose(&scale, &quat, &pos, worldMat);
//	XMStoreFloat3(&mPosition, pos);
//	XMStoreFloat4(&mRotation, quat);
//	XMStoreFloat3(&mScale, scale);	
//	XMFLOAT3 angle;
//	angle = MathUtility::ToEulerAngles(mRotation);
//	mAngle = angle;
}

void Transform::SetWorld()
{
	//XMVECTOR pos = XMLoadFloat3(&GetPosition());
	//XMVECTOR quat = XMLoadFloat4(&GetRotation());
	//XMVECTOR scale = XMLoadFloat3(&GetScale());
	//XMMATRIX worldMat = XMMatrixAffineTransformation(scale, XMVectorZero(), quat, pos);
	//XMStoreFloat4x4(&mWorldMatrix, worldMat);

	//auto localMat = worldMat;
	////if(mParent.lock() != nullptr)
	////{
	////	auto parentWorldMat = mParent.lock()->GetWorldMatrix();
	////	auto parentWorldMatInv = XMMatrixInverse(0, parentWorldMat);
	////	localMat = worldMat * parentWorldMatInv;
	////}
	//auto parentObj = GetGameObject()->GetParent();
	//if (parentObj != nullptr)
	//{
	//	auto parentWorldMat = parentObj->GetTransform()->GetWorldMatrix();
	//	auto parentWorldMatInv = XMMatrixInverse(0, parentWorldMat);
	//	localMat = worldMat * parentWorldMatInv;
	//}
	//XMStoreFloat4x4(&mLocalMatrix, localMat);

	//XMMatrixDecompose(&scale, &quat, &pos, localMat);
	//XMStoreFloat3(&mLocalPosition, pos);
	//XMStoreFloat4(&mLocalRotation, quat);
	//XMStoreFloat3(&mLocalScale, scale);
	//XMFLOAT3 angle;
	//angle = MathUtility::ToEulerAngles(mLocalRotation);
	//mLocalAngle = angle;
}

XMMATRIX Transform::GetWorldMatrix()
{
	XMVECTOR pos = XMLoadFloat3(&GetLocalPosition());
	XMVECTOR quat = XMLoadFloat4(&GetLocalRotation());
	XMVECTOR scale = XMLoadFloat3(&GetLocalScale());
	XMMATRIX localMat = XMMatrixAffineTransformation(scale, XMVectorZero(), quat, pos);

	auto parentObj = GetGameObject()->GetParent();
	if (parentObj != nullptr)
	{
		auto parentWorldMat = parentObj->GetTransform()->GetWorldMatrix();
		//auto parentWorldMatInv = XMMatrixInverse(0, parentWorldMat);
		localMat = localMat * parentWorldMat;
	}
	XMMatrixDecompose(&scale, &quat, &pos, localMat);
	XMFLOAT4 rotation;
	XMStoreFloat4(&rotation, quat);
	XMFLOAT3 angle;
	angle = MathUtility::ToEulerAngles(rotation);

	return localMat;
	//return XMLoadFloat4x4(&mWorldMatrix);
}

XMMATRIX Transform::GetLocalMatrix()
{
	return XMLoadFloat4x4(&mLocalMatrix);
}

void Transform::Translate(XMFLOAT3 move)
{
	SetLocalPosition(GetLocalPosition() + move);
}
void Transform::Rotate(XMFLOAT3 rotate, bool self)
{
	float x = XMConvertToRadians(rotate.x);
	float y = XMConvertToRadians(rotate.y);
	float z = XMConvertToRadians(rotate.z);
	auto right = XMLoadFloat3(&mRightVector);
	auto forward = XMLoadFloat3(&mForwardVector);
	auto up = XMLoadFloat3(&mUpVector);

	XMVECTOR xQuater; 
	XMVECTOR yQuater; 
	XMVECTOR zQuater;
	if (self)
	{
		xQuater = XMQuaternionRotationAxis(right, x);
		yQuater = XMQuaternionRotationAxis(up, y);
		zQuater = XMQuaternionRotationAxis(forward, z);
	}
	else
	{
		xQuater = XMQuaternionRotationAxis(XMVectorSet(1, 0, 0, 0), x);
		yQuater = XMQuaternionRotationAxis(XMVectorSet(0, 1, 0, 0), y);
		zQuater = XMQuaternionRotationAxis(XMVectorSet(0, 0, 1, 0), z);
	}

	XMVECTOR quater;
	quater = XMQuaternionMultiply(xQuater, yQuater);
	quater = XMQuaternionMultiply(quater, zQuater);
	XMVECTOR currentQuater = XMLoadFloat4(&this->GetLocalRotation());

	currentQuater = XMQuaternionMultiply(currentQuater, quater);

	XMFLOAT4 rotation;
	XMStoreFloat4(&rotation, currentQuater);
	SetLocalRotation(rotation);
}


//void Transform::SetChild(shared_ptr<Transform>& transform)
//{
//	if (transform != nullptr)
//	{
//		mChilds.push_back(transform);
//	}
//}
//shared_ptr<Transform> Transform::GetChild(unsigned int index)
//{
//	return mChilds[index].lock();
//}
//unsigned int Transform::GetChildCount()
//{
//	return (unsigned int)mChilds.size();
//}
//void Transform::SetParent(shared_ptr<Transform>& transform)
//{
//	if (transform != nullptr)
//	{
//		mParent = transform;
//
//		//SetWorld();
//	}
//}
//shared_ptr<Transform> Transform::GetParent()
//{
//	return mParent.lock();
//}
//
//std::shared_ptr<Transform> Transform::Find(std::wstring name, bool recursive)
//{
//	std::shared_ptr<Transform> result;
//
//	for(auto child : mChilds)
//	{
//		if(child.lock()->GetName() == name)
//		{
//			result = child.lock();
//			break;
//		}
//		
//		if(recursive)
//		{
//			FindRecursively(name, child.lock(), result);
//
//			if(result != nullptr)
//				break;
//		}
//	}
//	
//	return result;
//}
//
//void Transform::FindRecursively(std::wstring name, std::shared_ptr<Transform>& transform, std::shared_ptr<Transform>& result)
//{
//	auto childSize = transform->GetChildCount();
//	for(unsigned int i = 0; i < childSize; ++i)
//	{
//		auto child = transform->GetChild(i);
//
//		if(child->GetName() == name)
//		{
//			result = child;
//			break;
//		}
//		FindRecursively(name, child, result);
//	}
//}