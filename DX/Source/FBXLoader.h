#pragma once
#include <unordered_map>
#include <fbxsdk.h>
#include "FBXUtilities.h"
#include "Mesh.h"
#include "GameObject.h"
#include "Components.h"
using namespace DirectX;

struct FBXAnimCurveInfo
{
	FbxAnimCurve* curve;
	int boneIndex;
	std::wstring boneName;
	std::string type;
	std::string prop;
};

struct ModelMaterial
{
	enum MaterialType
	{
		Lambert = 0,
		Phong,
	};

	MaterialType type;

	std::wstring name;
	XMFLOAT4 ambient;
	XMFLOAT4 diffuse;
	XMFLOAT4 emissive;
	double transparencyFactor;

	XMFLOAT4 specular;
	XMFLOAT4 reflection;
	double specularIntensity;
	double shininess;
	double reflectionFactor;

	std::wstring diffuseMapName;
	std::wstring emissiveMapName;
	std::wstring glossMapName;
	std::wstring normalMapName;
	std::wstring specularMapName;	
};

struct ModelNode
{
	enum NodeType
	{
		Empty,
		Mesh,
		SkinnedMesh,
		Bone
	};
	NodeType type;
	uint64_t nodeID;
	std::wstring name;
	std::wstring parentName;

	std::vector<std::shared_ptr<ModelNode>> childs;

	std::vector<std::wstring> materialNames;	

	std::unordered_map<unsigned int, std::vector<unsigned int>> submeshIndices;
	std::vector<PNTIWVertex> vertices;
	
	std::vector<XMMATRIX> bindPoses;
	uint64_t rootBoneID;
	XMMATRIX matrix;
	
	ModelNode(std::wstring name, std::wstring parentName)
	{
		this->name = name;
		this->parentName = parentName;
	}

	~ModelNode()
	{
	}
};

struct ModelData
{
	std::wstring name;
	std::vector<std::shared_ptr<ModelNode>> meshRootNodes;
	std::unordered_map<uint64_t, std::shared_ptr<ModelNode>> boneRootNodes;

	std::shared_ptr<GameObject> gameObject;
	std::unordered_map<std::wstring, std::shared_ptr<Mesh>> meshes;
	std::vector<std::shared_ptr<AnimationClip>> animationClips;
};

class FBXLoader
{
public:
	enum AxisSystem
	{
		Direct = 0,
		OpenGL,
	};
	
public:
	FBXLoader(void);
	~FBXLoader(void);
	
	bool Initialize();
	void CleanupFbxManager();
	bool LoadScene(std::wstring inFileName, AxisSystem axis);

private:
	void LoadAnimationClip();
	void LoadGeometry(FbxNode* inNode, std::wstring parentName, std::shared_ptr<ModelNode>& modelNode);

	// 폴리곤의 정점 정보를 불러온다
	void LoadControlPoints(FbxNode* inNode);

	// 메시 정보를 불러온다
	void LoadMesh(FbxNode* inNode, std::shared_ptr<ModelNode>& meshNode);

	void AssociateMaterialToMesh(FbxNode* inNode, std::shared_ptr<ModelNode>& meshNode);

	void LoadMaterials(FbxScene* scene);
	void LoadMaterialAttribute(FbxSurfaceMaterial* inMaterial, ModelMaterial& mat);
	void LoadMaterialTexture(FbxSurfaceMaterial* inMaterial, ModelMaterial& mat);
	
	void LoadBoneNode(FbxNode* inNode, std::shared_ptr<ModelNode>& modelNode);
	void ProcessSkeletonHierarchy(FbxNode* inRootNode);
	void ProcessSkeletonHierarchyRecursively(FbxNode* inNode, int myIndex, int inParentIndex);
	void ProcessAnimCurve(FbxAnimCurve* curve, std::shared_ptr<AnimationCurve>& out, std::wstring type, char component);
	void ProcessAnimCurveNode(FbxAnimCurveNode* curveNode, int boneIndex, std::wstring boneName, std::shared_ptr<AnimationClip>& animClip, FbxAMatrix& preRotMatrix);
	void ProcessAnimCurve(std::vector<FBXAnimCurveInfo>& in, std::shared_ptr<AnimationClip>& animClip, FbxAMatrix& preRotMatrix);
//	void LoadJointsAndAnimations(FbxNode* inNode);
//	unsigned int FindJointIndexUsingName(const std::wstring& inJointName);

	void ReadUV(FbxMesh* inMesh, int inCtrlPointIndex, int inVertexCounter, PNTIWVertex& vertex);
	void ReadNormal(FbxMesh* inMesh, int inCtrlPointIndex, int inVertexCounter, XMFLOAT3& outNormal);
	void ReadBinormal(FbxMesh* inMesh, int inCtrlPointIndex, int inVertexCounter, XMFLOAT3& outBinormal);
	void ReadTangent(FbxMesh* inMesh, int inCtrlPointIndex, int inVertexCounter, XMFLOAT3& outTangent);

	void ComputeNodeMatrix(FbxNode* inMesh, std::shared_ptr<ModelNode> modelNode);

	std::shared_ptr<ModelData> CreateModel();
	std::shared_ptr<GameObject> CreateGameObject(std::shared_ptr<ModelData>& modelData);
	std::shared_ptr<GameObject> CreateGameObjectRecursively(std::shared_ptr<ModelData>& modelData, std::shared_ptr<ModelNode>& modelNode);
	std::shared_ptr<Mesh> CreateMesh(std::shared_ptr<ModelNode>& modelNode);
	std::shared_ptr<Material> CreateMaterial(const ModelMaterial& inMaterial);


	static void FBXMatrixToXMMATRIX(FbxAMatrix& src, XMMATRIX& out)
	{
		XMFLOAT4X4 temp;
		for (int i = 0; i<4; i++)
		{
			for (int j = 0; j<4; j++)
			{
				temp.m[i][j] = (float)src.Get(i, j);
			}
		}
		out = XMLoadFloat4x4(&temp);
	}
	
	FbxManager* mFBXManager;
	FbxScene* mFBXScene;
	std::wstring mFileName;
	AxisSystem mAxisSystem;

	bool mHasBone;
	std::vector<std::shared_ptr<AnimationClip>> mAnimationClips;

	std::unordered_map<UINT, std::shared_ptr<CtrlPoint>> mControlPoints;
	std::shared_ptr<ModelNode> mRootNode;
	std::shared_ptr<ModelData> mModelData;
};

