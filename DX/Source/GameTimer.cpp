#include "stdafx.h"
#include "GameTimer.h"

using namespace std::chrono;

void GameTimer::Initialize()
{
	mTimeScale = 1.0f;
	mLastTime = steady_clock::now();

	mSampleCount = 0;
	mCurrentFrameRate = 0;
	mFPS = 0;
	mFPSTimeElapsed = 0.0f;
}

void GameTimer::Tick(float fLockFPS)
{

	mCurrentTime = steady_clock::now();

	duration<float> fTimeElapsed = (mCurrentTime - mLastTime);
	if(fLockFPS > 0.0f)
	{
		while(fTimeElapsed.count() < (1.0f / fLockFPS))
		{
			mCurrentTime = steady_clock::now();

			fTimeElapsed = (mCurrentTime - mLastTime);
		}
	}

	mLastTime = mCurrentTime;

	if (fabsf(fTimeElapsed.count() - mDeltaTime) < 1.0f)
	{
		memmove(&mFrameTime[1], mFrameTime, (MAX_SAMPLE_COUNT-1)*sizeof(float));
		mFrameTime[0] = fTimeElapsed.count();
		if(mSampleCount < MAX_SAMPLE_COUNT) mSampleCount++;
	}

	mFPS++;

	mFPSTimeElapsed += fTimeElapsed.count();
	if(mFPSTimeElapsed > 1.0f)
	{
		mCurrentFrameRate = mFPS;
		mFPS = 0;
		mFPSTimeElapsed = 0.0f;
	}


	mDeltaTime = 0.0f;
	for(ULONG i = 0; i<mSampleCount; i++)
		mDeltaTime += mFrameTime[i];
	if(mSampleCount > 0)
		mDeltaTime /= mSampleCount;
}

unsigned long GameTimer::GetFrameRate(LPTSTR lpszString, int nCharacters)
{
	if(lpszString)
	{
		_itow_s(mCurrentFrameRate, lpszString, nCharacters, 10);
		wcscat_s(lpszString, nCharacters, _T(" FPS)"));
	}

	return mCurrentFrameRate;
}
unsigned long GameTimer::GetFrameRate(std::wstring &strOut)
{
	strOut = std::to_wstring(mCurrentFrameRate) + _T(" FPS)");
	
	return mCurrentFrameRate;
}

float GameTimer::GetDeltaTime()
{
	return mDeltaTime * mTimeScale;
}

float GameTimer::GetUnscaledDeltaTime()
{
	return mDeltaTime;
}