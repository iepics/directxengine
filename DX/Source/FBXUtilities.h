#pragma once
#include <algorithm>
using namespace DirectX;

struct PNTVertex
{
	XMFLOAT3 mPosition;
	XMFLOAT3 mNormal;
	XMFLOAT2 mUV;

};

struct VertexBlendingInfo
{
	unsigned int mBlendingIndex;
	double mBlendingWeight;

	VertexBlendingInfo():
		mBlendingIndex(0),
		mBlendingWeight(0.0)
	{}

	bool operator < (const VertexBlendingInfo& rhs)
	{
		return (mBlendingWeight > rhs.mBlendingWeight);
	}
};

struct PNTIWVertex
{
	XMFLOAT3 mPosition;
	XMFLOAT3 mNormal;
	std::vector<XMFLOAT2> mUVs;
	std::vector<VertexBlendingInfo> mVertexBlendingInfos;

	void SortBlendingInfoByWeight()
	{
		std::sort(mVertexBlendingInfos.begin(), mVertexBlendingInfos.end());
	}
};


struct BlendingIndexWeightPair
{
	UINT mBlendingIndex;
	double mBlendingWeight;

	BlendingIndexWeightPair() :
		mBlendingIndex(0), mBlendingWeight(0)
	{}
};
struct CtrlPoint
{
	XMFLOAT3 mPosition;
	std::vector<BlendingIndexWeightPair> mBlendingInfo;

	CtrlPoint()
	{
		mBlendingInfo.reserve(4);
	}
};

struct KeyframeFBX
{
	FbxLongLong mFrameNum;
	FbxAMatrix mGlobalTransform;
	KeyframeFBX* mNext;

	KeyframeFBX() :
		mNext(nullptr)
	{}
};

struct Joint
{
	std::wstring mName;
	int mParentIndex;
	FbxAMatrix mGlobalBindposeInverse;
	FbxAMatrix mBoneOffset;
	KeyframeFBX* mAnimation;
	FbxNode* mNode;

	Joint() :
		mNode(nullptr), mAnimation(nullptr)
	{
		mGlobalBindposeInverse.SetIdentity();
		mParentIndex = -1;
	}

	~Joint()
	{
		while (mAnimation)
		{
			KeyframeFBX* temp = mAnimation->mNext;
			delete mAnimation;
			mAnimation = temp;
		}
	}
};

struct Skeleton
{
	std::vector<Joint> mJoints;
};

struct TriangleFBX
{
	std::vector<UINT> mIndices;
	std::wstring mMaterialName;
	UINT mMaterialIndex;

	bool operator<(const TriangleFBX& rhs)
	{
		return mMaterialIndex < rhs.mMaterialIndex;
	}
};

class FBXUtil
{
public:
	static FbxAMatrix GetGeometryTransformation(FbxNode* inNode);
};


