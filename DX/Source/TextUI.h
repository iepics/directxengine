#pragma once
#include "UIComponent.h"
#include "Font.h"
#include "Mesh.h"
#include "Material.h"

class TextUI : public Cloneable<UIComponent, TextUI>
{
public:
	TextUI();
	~TextUI();
	
	void SetFont(std::shared_ptr<Font> font);
	void SetSize(float size);
	void SetText(std::wstring str);
	std::wstring GetText();

	void SetColor(const XMFLOAT4 color);

private:
	void CreateMesh();

	std::shared_ptr<Font> mFont;
	std::wstring mText;
	XMFLOAT4 mColor = XMFLOAT4(1, 1, 1, 1);

	

	std::shared_ptr<Effect> mEffect;
};

