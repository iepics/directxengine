#include "LightHelper.fx"

float4x4 gWorld;
float4x4 gView;
float4x4 gProjection;

float4 gMainColor;
Texture2D gDiffuseMap;
SamplerState samLinear
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

struct VertexInput
{
	float4 Position : POSITION;
	float4 Color : COLOR;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
};

struct PixelInput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
};

PixelInput VS(VertexInput input)
{
	PixelInput output = (PixelInput)0;
	
	output.Position = mul(input.Position, gWorld);
	output.Position = mul(output.Position, gView);
	output.Position = mul(output.Position, gProjection);
	output.Normal = mul(input.Normal, (float3x3)gWorld);

	output.Tex = input.Tex;

	return output;
}

float4 PS(PixelInput input) :SV_TARGET
{
	float4 textureColor;

	textureColor = gDiffuseMap.Sample(samLinear, input.Tex);

	return textureColor * gMainColor;
}

technique11 Default
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
};