#include "LightHelper.fx"

float4x4 gWorld;
float4x4 gView;
float4x4 gProjection;

float4x4 gBoneTransforms[96];

Texture2D gDiffuseMap;
Texture2D gShadowMap;
SamplerState samLinear
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};
SamplerState samLinearClamp
{
	Filter = MIN_MAG_LINEAR_MIP_POINT;
	AddressU = CLAMP;
	AddressV = CLAMP;
	AddressW = CLAMP;
	BorderColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
};

struct VertexInput
{
	float4 Position : POSITION;
	float4 Color : COLOR;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
	float3 Weights : WEIGHTS;
	uint4 BoneIndices : BONEINDICES;
};

struct PixelInput
{
	float4 Position : SV_POSITION;
	float4 WorldPos : POSITION0;
	float4 LightViewPos : TEXCOORD1;
	float4 Color : COLOR;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
};

PixelInput VS(VertexInput input)
{
	PixelInput output = (PixelInput)0;

	input.Position.w = 1.0f;
	output.WorldPos = mul(input.Position, gWorld);
	output.Position = output.WorldPos;
	output.Position = mul(output.Position, gView);
	output.Position = mul(output.Position, gProjection);

	output.LightViewPos = mul(input.Position, gWorld);
	output.LightViewPos = mul(output.LightViewPos, gLightView);
	output.LightViewPos = mul(output.LightViewPos, gLightProj);

	output.Normal = mul(input.Normal, (float3x3)gWorld);

	output.Tex = input.Tex;
	output.Color = input.Color;

	return output;
}

PixelInput SkinnedVS(VertexInput input)
{
	PixelInput output = (PixelInput)0;

	input.Position.w = 1.0f;

	///vertex blending
	float weights[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	weights[0] = input.Weights.x;
	weights[1] = input.Weights.y;
	weights[2] = input.Weights.z;
	weights[3] = 1.0f - weights[0] - weights[1] - weights[2];

	float4 posL = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float3 normalL = float3(0.0f, 0.0f, 0.0f);
	float4x4 mat = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	for(int i = 0; i<4; ++i)
	{
		mat += weights[i] * gBoneTransforms[input.BoneIndices[i]];
	}
	posL = mul(input.Position, mat);
	normalL = mul(input.Normal, (float3x3)mat);
	
	posL.w = 1.0f;

	///world transform
	output.WorldPos = mul(posL, gWorld);
	output.Position = output.WorldPos;
	output.Position = mul(output.Position, gView);
	output.Position = mul(output.Position, gProjection);

	output.LightViewPos = mul(posL, gWorld);
	output.LightViewPos = mul(output.LightViewPos, gLightView);
	output.LightViewPos = mul(output.LightViewPos, gLightProj);

	output.Normal = mul(normalL, (float3x3)gWorld);

	output.Tex = input.Tex;
	output.Color = input.Color;

	return output;
}

float4 PS(PixelInput input) :SV_TARGET
{
	float4 textureColor = gDiffuseMap.Sample(samLinear, input.Tex);
	float4 color = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float3 normal = normalize(input.Normal);
	float3 v = normalize((gCameraPos - input.WorldPos).xyz);

	float bias;
	float2 projectTexCoord;
	float2 moments;
	float lightDepthValue;

	input.LightViewPos.xyz /= input.LightViewPos.w;

	projectTexCoord.x = input.LightViewPos.x / 2.0f + 0.5f;
	projectTexCoord.y = input.LightViewPos.y / -2.0f + 0.5f;

	moments = gShadowMap.Sample(samLinearClamp, projectTexCoord.xy).rg;
	lightDepthValue = input.LightViewPos.z;

	float shadow = ChebyshevUpperBound(moments, lightDepthValue);
	color += ComputeDirectional(normal, v, textureColor);
	color *= shadow;

	//if(lightDepthValue-0.0001 < moments.x)
	//{
	//	color += ComputeDirectional(normal, v, textureColor);
	//}

	//if((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
	//{
	//	depthValue = gShadowMap.Sample(samLinearClamp, projectTexCoord.xy).r;
	//	lightDepthValue = input.LightViewPos.z;
	//	lightDepthValue = lightDepthValue - bias;

	//	if(lightDepthValue < depthValue)
	//	{
	//		color += ComputeDirectional(normal, v, textureColor);
	//	}
	//}
	//else
	//{
	//	color += ComputeDirectional(normal, v, textureColor);
	//}


	return color;// float4(projectTexCoord.x, projectTexCoord.y, 0, 1);
}

technique11 Default
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
};

technique11 Skinned
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, SkinnedVS()));
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
};