struct Material
{
	float4 Diffuse;
	float Metallic;
	float Roughness;
	float4 Emissive;
};

Material gMaterial;

float4 gColor;
float4 gPosition;
float gRange;

matrix gLightView;
matrix gLightProj;

float4 gLightPos;

float4 gCameraPos;

//Schlick approximation
float FresnelTerm(float3 l, float3 h)
{
	return gMaterial.Metallic + (1.0f - gMaterial.Metallic) * pow(1.0f - dot(l, h), 5);
}

//Smith's shadow function
float GeometryTerm(float3 n, float3 l, float3 v)
{
	float NdotL_clamped = max(dot(n, l), 0.0f);
	float NdotV_clamped = max(dot(n, v), 0.0f);
	float k = gMaterial.Roughness * sqrt(2.0f / 3.14159265f);
	float one_minus_k = 1.0f - k;
	return (NdotL_clamped / ((NdotL_clamped * one_minus_k) + k)) * (NdotV_clamped / ((NdotV_clamped * one_minus_k) + k));
}

//Blinn-Phong
float DistributionTerm(float3 n, float3 h)
{
	float a = (2.0f / (gMaterial.Roughness * gMaterial.Roughness)) - 2.0f;
	return (a + 2.0f) * pow(max(dot(n, h), 0.0f), a) / (2.0f * 3.14159265f);
}

float4 ComputeDirectional(float3 n, float3 v, float4 diffuse)
{
	float3 l = normalize(-gPosition.xyz);
	float3 h = normalize(l + v);
	float NdotL = dot(n, l);
	float NdotV = dot(n, v);
	float NdotL_clamped = max(NdotL, 0.0f);
	float NdotV_clamped = max(NdotV, 0.0f);

	//Fresnel Term(Schlick approximation)
	float F = gMaterial.Metallic + (1.0f - gMaterial.Metallic) * pow(1.0f - dot(l, h), 5);

	//Geometry Term(Smith's shadow function)
	float k = gMaterial.Roughness * sqrt(2.0f / 3.14159265f);
	float one_minus_k = 1.0f - k;
	float G = (NdotL_clamped / ((NdotL_clamped * one_minus_k) + k))
		 * (NdotV_clamped / ((NdotV_clamped * one_minus_k) + k));

	//Distribution Term(Blinn-Phong)
	float a = (2.0f / (gMaterial.Roughness * gMaterial.Roughness)) - 2.0f;
	float D = (a + 2.0f) * pow(max(dot(n, h), 0.0f), a) / (2.0f * 3.14159265f);

	float brdf_spec = F * G * D / (4.0f * NdotL_clamped * NdotV_clamped);
	float4 spec = NdotL_clamped * brdf_spec * gColor;	
	float4 diff = NdotL_clamped * (1.0f - gMaterial.Metallic)
		* gMaterial.Diffuse * diffuse * gColor;	

	return spec + diff;
}


///Shadow
float ChebyshevUpperBound(float2 moments, float t)
{
	float p = (t <= moments.x);

	float variance = moments.y - (moments.x * moments.x);
	variance = max(variance, 0.000001);

	float d = t - moments.x;
	float pMax = variance / (variance + d*d);

	return max(p, pMax);
}