//#include "Base.fx"

float4x4 gWorld;
float4x4 gView;
float4x4 gProjection;

float4x4 gBoneTransforms[96];

struct VertexInput
{
	float4 Position : POSITION;
	float4 Color : COLOR;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
	float3 Weights : WEIGHTS;
	uint4 BoneIndices : BONEINDICES;
};

struct PixelInput
{
	float4 Position : SV_POSITION;
};

PixelInput VS(VertexInput input)
{
	PixelInput output = (PixelInput)0;

	input.Position.w = 1.0f;

	output.Position = mul(input.Position, gWorld);
	output.Position = mul(output.Position, gView);
	output.Position = mul(output.Position, gProjection);

	return output;
}

PixelInput SkinnedVS(VertexInput input)
{
	PixelInput output = (PixelInput)0;

	input.Position.w = 1.0f;

	///vertex blending
	float weights[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	weights[0] = input.Weights.x;
	weights[1] = input.Weights.y;
	weights[2] = input.Weights.z;
	weights[3] = 1.0f - weights[0] - weights[1] - weights[2];

	float4x4 mat = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
		float4 posL = float4(0.0f, 0.0f, 0.0f, 0.0f);
		float3 normalL = float3(0.0f, 0.0f, 0.0f);

		for(int i = 0; i<4; ++i)
		{
			posL += weights[i] * mul(input.Position, gBoneTransforms[input.BoneIndices[i]]);
			normalL += weights[i] * mul(input.Normal, (float3x3)gBoneTransforms[input.BoneIndices[i]]);
		}

	posL.w = 1.0f;

	///world transform
	output.Position = mul(posL, gWorld);
	output.Position = mul(output.Position, gView);
	output.Position = mul(output.Position, gProjection);

	return output;
}


float2 PS(PixelInput input) :SV_TARGET
{
	float depth = input.Position.z / input.Position.w;
	float2 moments;	

	float dx = ddx(depth);
	float dy = ddy(depth);

	moments.x = depth;
	moments.y = depth*depth + 0.25*(dx*dx + dy*dy);

	return  moments;
}

technique11 Default
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
};

technique11 Skinned
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, SkinnedVS()));
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
};