cbuffer cbViewProjectionMatrix : register(b0)
{
	matrix gmtxView;
	matrix gmtxProjection;
};

cbuffer cbWorldMatrix : register(b1)
{
	matrix gmtxWorld;
};

struct VS_INPUT
{
	float4 position : POSITION;
	float4 color : COLOR;
	float2 tex : TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR0;
	float2 tex : TEXCOORD0;
};

VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output;
	input.position.w = 1.0f;
	output.position = mul(input.position, gmtxWorld);
	output.position = mul(output.position, gmtxView);
	output.position = mul(output.position, gmtxProjection);
	output.color = input.color;

	output.tex = input.tex;
	return output;
}


Texture2D shaderTexture;
SamplerState SampleType;
float4 PS(VS_OUTPUT input) : SV_Target
{
	float4 textureColor;

	textureColor = shaderTexture.Sample(SmapleType, input.tex);
	return textureColor;
}
