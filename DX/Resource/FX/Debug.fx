cbuffer cbObject : register(b0)
{
	float4x4 gWorld;
};

cbuffer cbCamera : register(b1)
{
	float4x4 gView;
	float4x4 gProjection;
};

struct VertexInput
{
	float4 Position : POSITION;
	float4 Color : COLOR;
};

struct PixelInput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR;
};

PixelInput VS(VertexInput input)
{
	PixelInput output = (PixelInput)0;

	output.Position = mul(input.Position, gWorld);
	output.Position = mul(output.Position, gView);
	output.Position = mul(output.Position, gProjection);

	output.Color = input.Color;

	return output;
}

float4 PS(PixelInput input) :SV_TARGET
{
	return input.Color;
}

technique11 Default
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
};